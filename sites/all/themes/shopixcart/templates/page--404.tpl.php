<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Error 404 Page not found</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/interface-icons.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../css/theme-deepred.css" rel="stylesheet" type="text/css" media="all"/>
        <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700|Montserrat:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body class="scroll-assist">
		<div class="nav-container">
		</div>
		<div class="main-container">
		<section class="height-100 bg--primary page-error">
				<div class="container pos-vertical-center">
					<div class="row">
						<div class="col-sm-12 text-center">
							<?php
							echo "<i class=\"icon icon--lg icon-Shopping-Cart\"></i><h1>Error 404 - Page Not Found</h1>
							<p>The page you were looking for wasn't found, if you think this might be a mistake <a href=\"http://shopixcart.eu/contact\">drop us a line</a></p>";
							?>
						</div>
					</div>
				</div>
			</section>
		</div>
		<script src="../js/jquery-2.1.4.min.js"></script>
        <script src="../js/scrollreveal.min.js"></script>
        <script src="../js/scripts.js"></script>
    </body>
</html>