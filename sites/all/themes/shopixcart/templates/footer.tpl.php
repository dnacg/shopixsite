<footer class="bg--dark footer-1 text-center-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <?php print render($page['footer_first']); ?>
            </div>
            <div class="col-md-5 col-sm-8">
                <?php print render($page['footer_second']); ?>
            </div>
            <div class="col-md-4 col-sm-12">
                <?php print render($page['footer_third']); ?>
            </div>
        </div>
        <div class="row footer__lower text-center-xs">
            <div class="col-sm-12">
                <hr>
            </div>
            <div class="col-sm-6">
                <?php print render($page['copyright']); ?>
            </div>
            <div class="col-sm-6 text-right text-center-xs">
                <?php print render($page['social_footer']); ?>
            </div>
        </div>
    </div>
</footer>