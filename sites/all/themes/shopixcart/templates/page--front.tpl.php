<?php
	global $base_url;
	$themeUrl = $base_url.'/'.path_to_theme();
	$urlOptions = array('absolute' => TRUE);
	require_once(drupal_get_path('theme','shopixcart').'/templates/nav.tpl.php');
?>
		<div class="main-container">
					
		<section class="height-100 imagebg cover cover-6 parallax" data-overlay="3">
			<?php print render($page['slider_front']); ?>

				<div class="container pos-bottom pos-absolute">
					<div class="row">
						<form id="user-register-form" class="form--square form--labels text-center-xs user-info-from-cookie" accept-charset="UTF-8" method="post" enctype="multipart/form-data" action="/user/register">
                            <div class="row">
								<div class="col-sm-4">
									<label>Username:</label>
									<input type="text" id="edit-name" name="name" value="" class="username form-text required" placeholder="Username" >
								</div>
								<div class="col-sm-5">
									<label>Email Address:</label>
									<input type="email" class="form-text required" maxlength="254" size="60" value="" name="mail" id="edit-mail" placeholder="you@yoursite.com">
								</div>
                                <input type="hidden" name="timezone" value="Europe/Athens">
                                <input type="hidden" name="form_build_id" value="form-lHqORriy4sU_-Nq5bd8MrShGczVYJ0pSjevNw1DPxQ0">
                                <input type="hidden" name="form_id" value="user_register_form">
								<div class="col-sm-3">
									<input type="submit" id="edit-submit" class="btn btn--primary vpe form-submit" name="op" value="Get started" style="margin-top:39px;">
								</div>
							</div>
						</form>
                    </div>
				</div>
			</section>
			
			<a id="featureboxes" class="in-page-link"></a>
			
			<section class="stats-1 bg--secondary">
				<div class="container">
					<?php print render($page['features_area']); ?>
                </div>
				</div>
			</section>
			
			<a id="imgleft" class="in-page-link"></a>
			
			<section class="imageblock about-1">
				<div class="imageblock__content col-md-6 col-sm-4 pos-left">
					<div class="background-image-holder">
						<img alt="image" src="<?php print $themeUrl ?>/img/inner1.jpg">
					</div>
				</div>
				<div class="container">
					<?php print render($page['widget_first']); ?>
				</div>
			</section>
			
			<a id="imgright" class="in-page-link"></a>
			
			<section class="imageblock about-1">
				<div class="imageblock__content col-md-6 col-sm-4 pos-right">
					<div class="background-image-holder">
						<img alt="image" src="<?php print $themeUrl ?>/img/inner2.jpg">
					</div>
				</div>
				<div class="container">
                    <?php print render($page['widget_second']); ?>
				</div>
			</section>
			
			<a id="bgnoimgaction" class="in-page-link"></a>
			
			<section class="cta cta-6 bg--secondary">
				<div class="container">
					<div class="row">
                        <?php print render($page['widget_third']); ?>
					</div>
				</div>
			</section>
			<a id="bgbackground" class="in-page-link"></a>
			<section class="imagebg section--even" data-overlay="8">
				<div class="background-image-holder">
					<img alt="image" src="<?php print $themeUrl ?>/img/hero11.jpg">
				</div>
				<div class="container">
					<?php print render($page['widget_fourth']); ?>
				</div>
			</section>
			
			<a id="pricingplans" class="in-page-link"></a>
			
			<section>
				<div class="container">
					<?php print render($page['pricing_area']); ?>
				</div>
			</section>
			
			<a id="blog" class="in-page-link"></a>
			
			<section>
				<div class="container">
					<div class="row">
						<?php print render($page['blog_area']); ?>







					</div>
				</div>
			</section>
			<section class="subscribe subscribe-1 space--sm imagebg" data-overlay="3">
				<div class="background-image-holder">
					<img alt="image" src="<?php print $themeUrl ?>/img/hero10.jpg">
				</div>
				<div class="container"  style="padding-top: 20px;">
					<div class="row">
						<div class="col-sm-8">
							<div class="subscribe__title">
								<h4>Join our mailing list to receive exclusive deals and updates</h4>
							</div>
						</div>
						<div class="col-sm-4">
							<?php print render($page['newsletter']); ?>
							<data data-token="9681c192e51ca2682c24e16e36df3d3b" class="mj-w-data" data-apikey="2063" data-w-id="1Du" data-lang="en_US" data-base="https://app.mailjet.com" data-width="640" data-height="328" data-statics="statics"></data>
							<div data-token="9681c192e51ca2682c24e16e36df3d3b" class="mj-w-btn" style="font-family: Helvetica,Arial,sans-serif; color: white; padding: 0px 55px; background-color: rgb(187, 66, 66); text-align: center; vertical-align: middle; display: inline-block; border-radius: 0px;">
								<div style="display: table; height: 55px;">
									<div style="display: table-cell; vertical-align: middle;">
										<div style="font-family: Helvetica,Arial,sans-serif; display: inline-block; text-align: center; font-size: 15px; vertical-align: middle;"><b>SUBSCRIBE TO NEWSLETTER</b></div>
									</div>
								</div>
							</div>

							<script type="text/javascript" src="https://app.mailjet.com/statics/js/widget.modal.js"></script>
						</div>
					</div>
				</div>
			</section>
			<?php require_once(drupal_get_path('theme','shopixcart').'/templates/footer.tpl.php'); ?>
		</div>

				