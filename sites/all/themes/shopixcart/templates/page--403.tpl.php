<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Error 403 - Permission Denied</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="sites/all/themes/shopixcart/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="sites/all/themes/shopixcart/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="sites/all/themes/shopixcart/css/interface-icons.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="sites/all/themes/shopixcart/css/theme-deepred.css" rel="stylesheet" type="text/css" media="all"/>
        <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700|Montserrat:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body class="scroll-assist" data-reveal-selectors="section:not(.masonry):not(:first-of-type):not(.parallax)">
		<div class="nav-container">
		</div>
		<div class="main-container">
		<section class="height-100 bg--primary page-error">
				<div class="container pos-vertical-center">
					<div class="row">
						<div class="col-sm-12 text-center">
							<?php
								function getUserIP(){
									$client  = @$_SERVER['HTTP_CLIENT_IP'];
									$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
									$remote  = $_SERVER['REMOTE_ADDR'];
									if(filter_var($client, FILTER_VALIDATE_IP)) {
										$ip = $client;
									}elseif(filter_var($forward, FILTER_VALIDATE_IP)){
										$ip = $forward;
									}else{
										$ip = $remote;
									}
								return $ip;
								}
								$user_ip = getUserIP();
								echo "<i class=\"icon icon--lg icon-Danger-2\"></i><h1>Error 403 - Permission Denied</h1><h3>Your IP Address is ".$user_ip."</h3><p>The area you are trying to access is restricted. All your info have been logged!</p>";
								?>
						</div>
					</div>
				</div>
			</section>
		</div>
		<script src="sites/all/themes/shopixcart/js/jquery-2.1.4.min.js"></script>
        <script src="sites/all/themes/shopixcart/js/scrollreveal.min.js"></script>
        <script src="sites/all/themes/shopixcart/js/scripts.js"></script>
    </body>
</html>
				