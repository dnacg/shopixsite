<?php
    global $base_url;
    $themeUrl = $base_url.'/'.path_to_theme();
    $urlOptions = array('absolute' => TRUE);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ShopixCart Maintenance Mode</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php print $themeUrl; ?>/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php print $themeUrl; ?>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php print $themeUrl; ?>/css/interface-icons.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php print $themeUrl; ?>/css/theme-deepred.css" rel="stylesheet" type="text/css" media="all"/>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700|Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>
<body class="scroll-assist">
<div class="nav-container">
</div>
<div class="main-container">
    <section class="height-100 bg--primary page-error">
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <?php
                    echo "<i class=\"icon icon--lg icon-Shopping-Cart\"></i><h1>Maintenance Mode!</h1>
							<h3>ShopixCart is under heavy heavy maintenance. We are doing the best we can to be back soon! For whatever you want, <a href=\"http://shopixcart.eu/contact\">drop us a line</a></h3>";
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php print $themeUrl; ?>/js/jquery-2.1.4.min.js"></script>
<script src="<?php print $themeUrl; ?>/js/scrollreveal.min.js"></script>
<script src="<?php print $themeUrl; ?>/js/scripts.js"></script>
</body>
</html>