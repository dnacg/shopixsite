<?php
    global $base_url;
    $themeUrl = $base_url.'/'.path_to_theme();
    $urlOptions = array('absolute' => TRUE);
?>
<div class="nav-container">
    <nav>
        <div class="utility-bar bg--secondary">
                <?php print render($page['top_bar_left']); ?>
            <div class="nav-module right">
                <?php print render($page['top_bar_right']); ?>
            </div>
        </div>
        <div class="nav-bar" data-fixed-at="700">
            <div class="nav-module logo-module left">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img alt="logo" class="logo logo-dark" src="<?php print $themeUrl; ?>/img/logo-dark.png"> <img alt="ShopixCart" class="logo logo-light" src="<?php print $themeUrl; ?>/img/logo-light.png"></a>
            </div>
            <div class="nav-module menu-module left">
                <ul class="menu">
                    <li class="vpe">
                        <a href="http://shopixcart.eu/">Home</a>
                    </li>
                    <li class="vpe">
                        <a href="/how-it-works">How it works</a>
                    </li>
                    <li>
                        <a href="/features">Features</a>
                        <ul>
                            <li>
                                <a href="/design">Store Design</a>
                            </li>
                            <li>
                                <a href="/products">Product Management</a>
                            </li>
                            <li>
                                <a href="/payments">Payments</a>
                            </li>
                            <li>
                                <a href="/orders-shipping">Order & Shipping</a>
                            </li>
                            <li>
                                <a href="/social-commerce">Social Media Commerce</a>
                            </li>
                            <li>
                                <a href="/mobile-commerce">Mobile Commerce</a>
                            </li>
                            <li>
                                <a href="/loyalty-club">Loyalty Club</a>
                            </li>
                            <li>
                                <a href="/pos">Point of Sales</a>
                            </li>
                            <li>
                                <a href="/erp">ERP / CRM Bridges</a>
                            </li>
                            <li>
                                <a href="/seo">Search Engine Optimization</a>
                            </li>
                            <li>
                                <a href="/marketing">Marketing</a>
                            </li>
                            <li>
                                <a href="/reporting">Reporting & Analytics</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#header">Resources</a>
                        <ul class="multi-column">
                            <li>
                                <ul>
                                    <li><span class="multi-column__title">Resellers</span></li>
                                    <li>
                                        <a href="/user">Customer Login</a>
                                    </li>
                                    <li>
                                        <a href="/user">Resellers Login</a>
                                    </li>
                                    <li>
                                        <a href="/user">Partners Login</a>
                                    </li>
                                    <li>
                                        <a href="/library">Shopixcart Manuals</a>
                                    </li>
                                    <li>
                                        <a href="/marketing-material">Marketing Material</a>
                                    </li>
                                    <li>
                                        <a href="/marketing-tools">Marketing Tools</a>
                                    </li>
                                    <li>
                                        <a href="/knowledge-base">Knowledge Base</a>
                                    </li>
                                    <li>
                                        <a href="/custom">Custom Services</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><span class="multi-column__title">Resellers</span></li>
                                    <li>
                                        <a href="/user">Customer Login</a>
                                    </li>
                                    <li>
                                        <a href="/user">Resellers Login</a>
                                    </li>
                                    <li>
                                        <a href="/user">Partners Login</a>
                                    </li>
                                    <li>
                                        <a href="/library">Shopixcart Manuals</a>
                                    </li>
                                    <li>
                                        <a href="/marketing-material">Marketing Material</a>
                                    </li>
                                    <li>
                                        <a href="/marketing-tools">Marketing Tools</a>
                                    </li>
                                    <li>
                                        <a href="/knowledge-base">Knowledge Base</a>
                                    </li>
                                    <li>
                                        <a href="/custom">Custom Services</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="vpe">
                        <a href="/pricing">Pricing</a>
                    </li>
                    <li class="vpe">
                        <a href="/node/add/support-tickets">Support</a>
                    </li>
                    <li class="vpe">
                        <a href="/affiliates">Affiliates</a>
                    </li>
                    <li class="vpe">
                        <a href="/partners">Partners</a>
                    </li>
                    <li class="vpe">
                        <a href="/blog">Blog</a>
                    </li>
                    <li class="vpe">
                        <a href="/contact">Contact us</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="nav-mobile-toggle visible-sm visible-xs">
            <i class="icon-Align-JustifyAll icon icon--sm"></i>
        </div>
    </nav>
</div>