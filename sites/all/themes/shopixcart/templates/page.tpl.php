<?php
    global $base_url;
    $themeUrl = $base_url.'/'.path_to_theme();
    $urlOptions = array('absolute' => TRUE);
    require_once(drupal_get_path('theme','shopixcart').'/templates/nav.tpl.php');
?>
<div class="main-container">

    <section class="height-40 imagebg bg--primary" data-overlay="3">

        <?php print render($page['slider_inner']); ?>
        <div class="background-image-holder">
            <img alt="pic" src="<?php echo $themeUrl; ?>/img/hero26.jpg">
        </div>

    </section>
    <?php if ($messages): ?>
        <?php print $messages; ?>
    <?php endif; ?>
    <section class="bg--secondary">
        <div class="container">
            <div class="row">
                <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
                <?php if($page['sidebar']) { ?>
                <div class="col-sm-9">
                    <?php print render($title_prefix); ?>
                    <?php if ($title): ?><h4 class="title" id="page-title"><?php print $title; ?></h4><?php endif; ?>
                    <?php print render($title_suffix); ?>
                    <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
                    <?php print render($page['help']); ?>
                    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
                    <?php print render($page['content']); ?>
                    <?php print $feed_icons; ?>
                </div>
                <div class="col-sm-3">
                    <?php print render($page['sidebar']); ?>
                </div>
                <?php }else{ ?>
                <div class="col-sm-12">
                    <?php print render($title_prefix); ?>
                    <?php if ($title): ?><h4 class="title" id="page-title"><?php print $title; ?></h4><?php endif; ?>
                    <?php print render($title_suffix); ?>
                    <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
                    <?php print render($page['help']); ?>
                    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
                    <?php print render($page['content']); ?>
                    <?php print $feed_icons; ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="subscribe subscribe-1 space--sm imagebg" data-overlay="3">
        <div class="background-image-holder">
            <img alt="image" src="<?php print $themeUrl ?>/img/hero10.jpg">
        </div>
        <div class="container"  style="padding-top: 20px;">
            <div class="row">
                <div class="col-sm-8">
                    <div class="subscribe__title">
                        <h4>Join our mailing list to receive exclusive deals and updates</h4>
                    </div>
                </div>
                <div class="col-sm-4">
                    <?php print render($page['newsletter']); ?>
                    <data data-token="9681c192e51ca2682c24e16e36df3d3b" class="mj-w-data" data-apikey="2063" data-w-id="1Du" data-lang="en_US" data-base="https://app.mailjet.com" data-width="640" data-height="328" data-statics="statics"></data>
                    <div data-token="9681c192e51ca2682c24e16e36df3d3b" class="mj-w-btn" style="font-family: Helvetica,Arial,sans-serif; color: white; padding: 0px 55px; background-color: rgb(187, 66, 66); text-align: center; vertical-align: middle; display: inline-block; border-radius: 0px;">
                        <div style="display: table; height: 55px;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div style="font-family: Helvetica,Arial,sans-serif; display: inline-block; text-align: center; font-size: 15px; vertical-align: middle;"><b>SUBSCRIBE TO NEWSLETTER</b></div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript" src="https://app.mailjet.com/statics/js/widget.modal.js"></script>
                </div>
            </div>
        </div>
    </section>
   <?php require_once(drupal_get_path('theme','shopixcart').'/templates/footer.tpl.php'); ?>
</div>