<?php
//Override default drupal jquery 1.4.4 with 2.1.4

 function shopixcart_js_alter(&$js){
     $jqKey = "jquery-2.1.4.min.js"; // a key for new jquery file entry
     $js[$jqKey] = $js['misc/jquery.js']; // copy the default jquery settings, so you don't have to re-type them.
    $js[$jqKey]['data'] = 'sites/all/themes/shopixcart/js/jquery-2.1.4.min.js'; // the path for new jquery file.
    $js[$jqKey]['version'] = '2.1.4'; // your jquery version.
    unset($js['misc/jquery.js']); // delete drupal's default jquery file.
 }

function shopixcart_preprocess_page(&$vars) {
    $header = drupal_get_http_header("status");
    if ($header == "404 Not Found") {
        $vars['theme_hook_suggestions'][] = 'page__404';
    }
    elseif ($header == "403 Forbidden") {
        $vars['theme_hook_suggestions'][] = 'page__403';
    }
}
function shopixcart_feed_icon($url) {
    return null;
}
