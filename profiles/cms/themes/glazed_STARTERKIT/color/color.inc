<?php
  //color schemes
  $info = array(

  // Images to copy over
  'copy' => array(
    'logo.png',
    'glazed-favicon.png',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/glazed.css',
  ),

  // Color areas to fill (x, y, width, height)
  'fill' => array(
    'base' => array(0, 0, 300, 300),
  ),

  // Gradient definitions.
  'gradients' => array(
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'glazed-favicon.png' => array(0, 0, 300, 300),
  ),

  // color preview css
  'preview_css' => '',

  // Base file for image generation
  'base_image' => 'glazed-favicon-transparent.png',

  //  blend target
  'blend_target' => '#ffffff',

  // Available colors and color labels used in theme.
  'fields' => array(
    'base' => t('Base color'),
    'link' => t('Link color'),
    'accent1' => t('Accent color 1'),
    'accent2' => t('Accent color 2'),
    'text' => t('Text color'),
    'headings' => t('Headings color'),
    'well' => t('Well color'),
    'welltext' => t('Well text'),
    'headertop' => t('Top Header background'),
    'headertoptext' => t('Top Header text'),
    'pagetitle' => t('Page Title background'),
    'pagetitletext' => t('Page Title text'),
    'headersticky' => t('Sticky Header background'),
    'headerstickytext' => t('Sticky Header text'),
    'headermobile' => t('Mobile Header background'),
    'headermobiletext' => t('Mobile Header text'),
    'headerside' => t('Side Header background'),
    'headersidetext' => t('Side Header text'),
    'graylight' => t('Gray Light'),
    'graylighter' => t('Gray Lighter'),
    'silver' => t('Silver'),
  ),

  'schemes' => array(
    'default' => array(
      'title' => t('Sky Blue (Default)'),
      'colors' => array(
        'base' => '#1488cb',
        'link' => '#1488cb',
        'accent1' => '#06537e',
        'accent2' => '#033747',
        'text' => '#666666',
        'headings' => '#333333',
        'well' => '#eeeeee',
        'welltext' => '#444444',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'pagetitle' => '#e5e6e6',
        'pagetitletext' => '#feffff',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#ededed',
        'silver' => '#f1f1f1',
      ),
    ),
    'green' => array(
      'title' => t('Green'),
      'colors' => array(
        'base' => '#42bd41',
        'link' => '#42bd41',
        'accent1' => '#0a8f08',
        'accent2' => '#0d5302',
        'text' => '#555555',
        'headings' => '#333333',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#1f1f1f',
        'headertoptext' => '#f2f2f2',
        'headersticky' => '#1f1f1e',
        'headerstickytext' => '#f2f2f1',
        'headermobile' => '#1f1e1f',
        'headermobiletext' => '#f2f1f2',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'red' => array(
      'title' => t('Material Red'),
      'colors' => array(
        'base' => '#f36c60',
        'link' => '#f36c60',
        'accent1' => '#dd191d',
        'accent2' => '#b0120a',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'pink' => array(
      'title' => t('Material Pink'),
      'colors' => array(
        'base' => '#f06292',
        'link' => '#f06292',
        'accent1' => '#d81b60',
        'accent2' => '#880e4f',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'purple' => array(
      'title' => t('Material Purple'),
      'colors' => array(
        'base' => '#ba68c8',
        'link' => '#ba68c8',
        'accent1' => '#8e24aa',
        'accent2' => '#4a148c',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'deeppurple' => array(
      'title' => t('Material Deep Purple'),
      'colors' => array(
        'base' => '#9575cd',
        'link' => '#9575cd',
        'accent1' => '#5e35b1',
        'accent2' => '#311b92',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'indigo' => array(
      'title' => t('Material Indigo'),
      'colors' => array(
        'base' => '#7986cb',
        'link' => '#7986cb',
        'accent1' => '#3949ab',
        'accent2' => '#1a237e',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'blue' => array(
      'title' => t('Material Blue'),
      'colors' => array(
        'base' => '#91a7ff',
        'link' => '#91a7ff',
        'accent1' => '#4e6cef',
        'accent2' => '#2a36b1',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'lightblue' => array(
      'title' => t('Material Light Blue'),
      'colors' => array(
        'base' => '#4fc3f7',
        'link' => '#4fc3f7',
        'accent1' => '#039be5',
        'accent2' => '#01579b',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'cyan' => array(
      'title' => t('Material Cyan'),
      'colors' => array(
        'base' => '#4db6ac',
        'link' => '#4db6ac',
        'accent1' => '#00897b',
        'accent2' => '#004d40',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'teal' => array(
      'title' => t('Material Teal'),
      'colors' => array(
        'base' => '#4db6ac',
        'link' => '#4db6ac',
        'accent1' => '#00897b',
        'accent2' => '#004d40',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'lightgreen' => array(
      'title' => t('Material Light Green'),
      'colors' => array(
        'base' => '#aed581',
        'link' => '#aed581',
        'accent1' => '#7cb342',
        'accent2' => '#33691e',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'lime' => array(
      'title' => t('Material Lime'),
      'colors' => array(
        'base' => '#f06292',
        'link' => '#dce775',
        'accent1' => '#c0ca33',
        'accent2' => '#827717',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'yellow' => array(
      'title' => t('Material Yellow'),
      'colors' => array(
        'base' => '#fff176',
        'link' => '#fff176',
        'accent1' => '#fdd835',
        'accent2' => '#f57f17',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'amber' => array(
      'title' => t('Material Amber'),
      'colors' => array(
        'base' => '#ffd54f',
        'link' => '#ffd54f',
        'accent1' => '#ffb300',
        'accent2' => '#ff6f00',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'orange' => array(
      'title' => t('Material Orange'),
      'colors' => array(
        'base' => '#ffb74d',
        'link' => '#ffb74d',
        'accent1' => '#fb8c00',
        'accent2' => '#e65100',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'deeporange' => array(
      'title' => t('Material Deep Orange'),
      'colors' => array(
        'base' => '#ff8a65',
        'link' => '#ff8a65',
        'accent1' => '#f4511e',
        'accent2' => '#bf360c',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'brown' => array(
      'title' => t('Material Brown'),
      'colors' => array(
        'base' => '#a1887f',
        'link' => '#a1887f',
        'accent1' => '#6d4c41',
        'accent2' => '#3e2723',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'grey' => array(
      'title' => t('Material Grey'),
      'colors' => array(
        'base' => '#e0e0e0',
        'link' => '#e0e0e0',
        'accent1' => '#757575',
        'accent2' => '#212121',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
    'bluegrey' => array(
      'title' => t('Material Blue Grey'),
      'colors' => array(
        'base' => '#90a4ae',
        'link' => '#90a4ae',
        'accent1' => '#546e7a',
        'accent2' => '#263238',
        'well' => '#222222',
        'welltext' => '#ededed',
        'headertop' => '#ffffff',
        'headertoptext' => '#696969',
        'text' => '#555555',
        'headings' => '#333333',
        'headersticky' => '#fffffe',
        'headerstickytext' => '#696968',
        'headermobile' => '#fffeff',
        'headermobiletext' => '#696869',
        'headerside' => '#fafafa',
        'headersidetext' => '#323232',
        'graylight' => '#999999',
        'graylighter' => '#eeeeee',
        'silver' => '#f1f1f1',
      ),
    ),
  ),
);
?>