<?php

  $page_title_image = theme_get_setting('page_title_image', $theme);
  if ($page_title_image && is_numeric($page_title_image) && ($page_title_image > 0)) {
    $file = file_load($page_title_image);
    $uri = $file->uri;
    $url = file_create_url($uri);
    $CSS .= "  .page-title-full-width-container:after { background-image: url('" . $url . "'); }\n\n";
  }

  $CSS .= "  .page-title-full-width-container:after { opacity:  " . theme_get_setting('page_title_image_opacity') . " } \n\n";

  $CSS .= "  #page-title { text-align:  " . theme_get_setting('page_title_align') . " } \n\n";

  $CSS .= "  #page-title .page-title { height:  " . theme_get_setting('page_title_height') . "px } \n\n";
