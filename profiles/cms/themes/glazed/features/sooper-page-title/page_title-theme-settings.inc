<?php

$form['glazed_settings']['page_title'] = array(
  '#title' => t('Page Title'),
  '#type' => 'fieldset',
);

$form['glazed_settings']['page_title']['page_title_breadcrumbs'] = array(
  '#type' => 'checkbox',
  '#title' => t('Show Breadcrumbs'),
  '#default_value' => ((theme_get_setting('page_title_breadcrumbs') !== null)) ? theme_get_setting('page_title_breadcrumbs') : 1,
);

$form['glazed_settings']['page_title']['page_title_home_hide'] = array(
  '#type' => 'checkbox',
  '#title' => t('Hide on Homepage'),
  '#default_value' => ((theme_get_setting('page_title_home_hide') !== null)) ? theme_get_setting('page_title_home_hide') : 1,
);

$form['glazed_settings']['page_title']['page_title_align'] = array(
    '#type' => 'radios',
    '#title' => t('Align Title'),
    '#default_value' => (theme_get_setting('page_title_align')) ? theme_get_setting('page_title_align') : 'left',
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right')
    ),
    '#description' => t('Choose position of Page Title within page title header.'),
);

$form['glazed_settings']['page_title']['page_title_height'] = array(
  '#type' => 'textfield',
  '#title' => t('Height'),
  '#default_value' => (theme_get_setting('page_title_height')) ? theme_get_setting('page_title_height') : '120',
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('height of the header. 50px - 500px. Default is 120px.'),
);

$form['glazed_settings']['page_title']['page_title_image_opacity'] = array(
  '#type' => 'textfield',
  '#title' => t('Backound Image Opacity'),
  '#default_value' => (theme_get_setting('page_title_image_opacity')) ? theme_get_setting('page_title_image_opacity') : 1,
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Use this to blend the background image with a color. 0 is fully transparent and 1 is fully opaque.'),
);

// Does not work in subthemes: http://drupal.stackexchange.com/questions/157128/problem-with-function-from-theme-settings-php-in-sub-theme
if ($subject_theme == 'glazed') {

  $form['glazed_settings']['page_title']['page_title_image'] = array(
    '#type' => 'media',
    '#tree' => TRUE, // Required
    '#title' => t('Default Background'),
    '#description' => t('The default page title image can also be changed per node, using the Header image field in the node form.'),
    '#default_value' => theme_get_setting('page_title_image'),
    '#media_options' => array(
      'global' => array(
        'file_extensions' => 'gif png jpg jpeg', // File extensions
        'max_filesize' => '100 MB',
        'file_directory' => 'glazed-assets', // Will be a subdirectory of the files directory
        'types' => array('image'), // Refers to a file_entity bundle (such as audio, video, image, etc.)
      ),
    ),
  );
}

$form['glazed_settings']['page_title']['page_title_animate'] = array(
    '#type' => 'radios',
    '#title' => t('Animation'),
    '#default_value' => ((theme_get_setting('page_title_animate') !== null)) ? theme_get_setting('page_title_animate') : 0,
    '#options' => array(
      0 => t('No Animation'),
      'bounce' => t('Bounce'),
      'flash' => t('Flash'),
      'pulse' => t('Pulse'),
      'fadeIn' => t('Fade in'),
      'bounceInDown' => t('Bounce in down'),
      'bounceInUp' => t('Bounce in up'),
      'fadeInDown' => t('Fade in down'),
      'fadeInUp' => t('Fade in up'),
    ),
    '#description' => t('Choose <a target="_blank" href="@animatecss">animate.css</a> appear animation', array('@drupal-handbook' => 'https://daneden.github.io/animate.css/')),
);
