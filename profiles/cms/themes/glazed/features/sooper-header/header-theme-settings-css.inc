<?php

if (module_exists('color')) {
  module_load_include('module', 'color');
  $theme_colors = color_get_palette($theme);
}

 /**
 * Convert hexdec color string to rgb(a) string
 * http://mekshq.com/how-to-convert-hexadecimal-color-code-to-rgb-or-rgba-using-php/
 */
function _glazed_hex2rgba($color, $opacity = null) {
  $default = 'rgb(0,0,0)';
  //Return default if no color provided
  if(empty($color)) {
    return $default;
  }
  //Sanitize $color if "#" is provided
  if ($color[0] == '#' ) {
    $color = substr( $color, 1 );
  }

  //Check if color has 6 or 3 characters and get values
  if (strlen($color) == 6) {
    $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
  } elseif ( strlen( $color ) == 3 ) {
    $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
  } else {
    return $default;
  }

  //Convert hexadec to rgb
  $rgb =  array_map('hexdec', $hex);

  //Check if opacity is set(rgba or rgb)
  if($opacity !== null){
    if(abs($opacity) > 1)
    $opacity = 1.0;
    $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
  } else {
    $output = 'rgb('.implode(",",$rgb).')';
  }

  //Return rgb(a) color string
  return $output;
}

  $header_mobile_height = theme_get_setting('header_mobile_height', $theme);
  $CSS .= <<<EOT
@media screen and (max-width: 1200px) {
   .glazed-header .navbar-header .wrap-branding,
   .glazed-header .navbar-toggle {
      height: {$header_mobile_height}px
   }
}

EOT;

if (theme_get_setting('header_position', $theme)) {
  // Side Header
  $header_side_width = $padding_left = theme_get_setting('header_side_width', $theme);
  $header_side_rgba = $theme_colors['headerside'];
  $align = theme_get_setting('header_side_align', $theme);
  if (theme_get_setting('header_side_overlay', $theme)) {
    if (module_exists('color')) {
      $color = $theme_colors['headerside'];
      $opacity = theme_get_setting('header_side_bg_opacity', $theme);
      $header_side_rgba = _glazed_hex2rgba($color, $opacity);
    }
    $padding_left = 0;
  }

  $CSS .= <<<EOT
@media screen and (min-width: 1200px) {
   .body--glazed-header-side #navbar.glazed-header {
      width: {$header_side_width}px;
      background-color: $header_side_rgba;
      text-align: $align;
    }
    .body--glazed-header-side {
      padding-left: {$header_side_width}px;
    }
}

EOT;
}
else {
  // Top Header
  $header_top_height = theme_get_setting('header_top_height', $theme);
  $header_top_height_scroll = theme_get_setting('header_top_height_scroll', $theme);
  $header_top_opacity = $header_top_opacity_scroll = 1;
  $header_top_rgba = $header_top_scroll_rgba = $header_top_scroll_rgba = $theme_colors['header'];
  if (theme_get_setting('header_top_behavior', $theme) == 'sticky' && theme_get_setting('header_top_overlay', $theme)) {
    if (theme_get_setting('header_top_sticky_show', $theme) == 'after_scroll') {
      $header_top_opacity = 0;
    }
  }
  if (theme_get_setting('header_top_behavior', $theme) == 'sticky' OR theme_get_setting('header_top_overlay', $theme)) {
    if (module_exists('color')) {
      $color = $theme_colors['header'];
      $opacity = theme_get_setting('header_top_bg_opacity', $theme);
      $header_top_rgba = _glazed_hex2rgba($color, $opacity);
      $color = $theme_colors['header'];
      $opacity = theme_get_setting('header_top_bg_opacity_scroll', $theme);
      $header_top_scroll_rgba = _glazed_hex2rgba($color, $opacity);
    }
  }
  $CSS .= <<<EOT

@media screen and (min-width: 1200px) {
  #navbar.glazed-header--top {
    background-color: $header_top_rgba;
    opacity: $header_top_opacity;
  }
  #navbar.glazed-header--top.affix {
    background-color: $header_top_scroll_rgba;
    opacity: $header_top_opacity_scroll;
  }

  .glazed-header--top .menu > li > a,
  .glazed-header--top .wrap-branding {
    height: {$header_top_height}px;
  }
  .glazed-header--top.affix .menu > li > a,
  .glazed-header--top.affix .navbar-header .wrap-branding {
    height: {$header_top_height_scroll}px;
  }
  .nav-child-fixed-width {
    top: {$header_top_height}px;
  }
  .glazed-header--top.affix .nav-child-fixed-width {
    top: $header_top_height_scroll;
  }
}

EOT;

  if (theme_get_setting('header_top_behavior', $theme) == 'fixed' && !theme_get_setting('header_top_overlay', $theme)) {
    $CSS .= "@media screen and (min-width: 1200px) {\n\n";
    $CSS .= "  body.body--glazed-header-fixed.body--glazed-header-not-overlay { padding-top: " . theme_get_setting('header_top_height', $theme) . "px !important }\n\n";
    $CSS .= "  html.js body.admin-menu.body--glazed-header-fixed.body--glazed-header-not-overlay { padding-top: " . (theme_get_setting('header_top_height', $theme) + 28) . "px !important }\n\n";
    $CSS .= "  html.js body.toolbar.body--glazed-header-fixed.body--glazed-header-not-overlay { padding-top: " . (theme_get_setting('header_top_height', $theme) + 30) . "px !important }\n\n";
    $CSS .= "  html.js body.toolbar-drawer.body--lazed-header-fixed.body--glazed-header-not-overlay { padding-top: " . (theme_get_setting('header_top_height', $theme) + 64) . "px !important }\n\n";
    $CSS .= "}\n\n";
  }

  if (theme_get_setting('header_top_overlay', $theme)) {
    $CSS .= "  @media screen and (min-width: 1200px) { .body--glazed-header-overlay .page-title-full-width-container  { padding-top: " . (theme_get_setting('header_top_height', $theme) + 30) . "px } }\n\n";
  }
}
