<?php


$form['glazed_settings']['header'] = array(
  '#title' => t('Header'),
  '#type' => 'fieldset',
);

$form['glazed_settings']['header']['header_position'] = array(
  '#type' => 'checkbox',
  '#description' => t('Default is top header.'),
  '#title' => t('Enable Side-Header'),
  '#default_value' => ((theme_get_setting('header_position') !== null)) ? theme_get_setting('header_position') : 0,
);

$form['glazed_settings']['header']['header_top'] = array(
  '#title' => t('Top Header Options'),
  '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_layout'] = array(
    '#type' => 'radios',
    '#title' => t('Layout'),
    '#description' => t('Different header layouts give a different view at your menu and logo.'),
    '#default_value' => ((theme_get_setting('header_top_layout') !== null)) ? theme_get_setting('header_top_layout') : 0,
    '#options' => array(
      0 => t('Normal (Logo left, Menu Right)'),
      'centered' => t('Centered (stacked)'),
      'logo_left_menu_left' => t('Logo Left, Menu Left'),
      'logo_center_menu_left' => t('Logo Center, Menu Left'),
      'logo_center_menu_right' => t('Logo Center, Menu Right'),
      'logo_right_menu_left' => t('Logo Right, Menu Left'),
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_overlay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overlay mode'),
    '#description' => t('Overlay mode makes the header hover over the underlying content.'),
    '#default_value' => ((theme_get_setting('header_top_overlay') !== null)) ? theme_get_setting('header_top_overlay') : TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_height'] = array(
  '#type' => 'textfield',
  '#title' => t('Height'),
  '#default_value' => ((theme_get_setting('header_top_height') !== null)) ? theme_get_setting('header_top_height') : '100',
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Initial height of the header. 10px - 200px. Default is 100.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_behavior'] = array(
    '#type' => 'radios',
    '#title' => t('Behavior'),
    '#default_value' => ((theme_get_setting('header_top_behavior') !== null)) ? theme_get_setting('header_top_behavior') : 'sticky',
    '#options' => array(
      'normal' => t('Normal'),
      'fixed' => t('Fixed'),
      'sticky' => t('Sticky'),
    ),
    '#description' => t('Choose what happens when a user scrolls. A Normal header stays in place, fixed header scrolls along, sticky header catches up later.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_sticky_show'] = array(
    '#type' => 'radios',
    '#title' => t('Hide before Scrolling'),
    '#default_value' => ((theme_get_setting('header_top_sticky_show') !== null)) ? theme_get_setting('header_top_sticky_show') : 'always',
    '#options' => array(
      'always' => t('Always Show'),
      'after_scroll' => t('Hide initially. Show after scroll offset'),
    ),
    '#description' => t('Hide menu before or after scrolling.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
        ':input[name="header_top_overlay"]' => array('checked' => TRUE),
        ':input[name="header_top_behavior"]' => array('value' => 'sticky'),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_bg_opacity'] = array(
  '#type' => 'textfield',
  '#title' => t('Background Color Opacity'),
  '#default_value' => ((theme_get_setting('header_top_bg_opacity') !== null)) ? theme_get_setting('header_top_bg_opacity') : 1,
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Creates RGBa translucent background color. 0 is fully transparent and 1 is fully opaque.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
        ':input[name="header_top_behavior"]' => array('!value' => 'normal'),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_height_sticky_offset'] = array(
  '#type' => 'textfield',
  '#title' => t('Scroll offset'),
  '#default_value' => ((theme_get_setting('header_top_height_sticky_offset') !== null)) ? theme_get_setting('header_top_height_sticky_offset') : '60',
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Scroll distance before header jumps to its fixed position at the top of page. 0 - 2096px. Default is 60.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
        ':input[name="header_top_behavior"]' => array('value' => 'sticky'),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_height_scroll'] = array(
  '#type' => 'textfield',
  '#title' => t('Height after scroll offset'),
  '#default_value' => ((theme_get_setting('header_top_height_scroll') !== null)) ? theme_get_setting('header_top_height_scroll') : '50',
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Header height after scrolling past scroll offset. Default is 50.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
        ':input[name="header_top_behavior"]' => array('value' => 'sticky'),
      ),
    ),
);

$form['glazed_settings']['header']['header_top']['header_top_bg_opacity_scroll'] = array(
  '#type' => 'textfield',
  '#title' => t('Background Opacity after scroll offset'),
  '#default_value' => ((theme_get_setting('header_top_bg_opacity_scroll') !== null)) ? theme_get_setting('header_top_bg_opacity_scroll') : 1,
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Creates RGBa translucent background color. 0 is fully transparent and 1 is fully opaque.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => FALSE),
        ':input[name="header_top_behavior"]' => array('value' => 'sticky'),
      ),
    ),
);

$form['glazed_settings']['header']['header_side'] = array(
  '#title' => t('Side Header Options'),
  '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => TRUE),
      ),
    ),
);

$form['glazed_settings']['header']['header_side']['header_side_overlay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overlay mode'),
    '#description' => t('Overlay mode makes the header hover over the page background.'),
    '#default_value' => ((theme_get_setting('header_side_overlay') !== null)) ? theme_get_setting('header_side_overlay') : 'normal',
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => TRUE),
      ),
    ),
);

$form['glazed_settings']['header']['header_side']['header_side_bg_opacity'] = array(
  '#type' => 'textfield',
  '#title' => t('Background Opacity'),
  '#default_value' => ((theme_get_setting('header_side_bg_opacity') !== null)) ? theme_get_setting('header_side_bg_opacity') : 1,
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Creates RGBa translucent background color. 0 is fully transparent and 1 is fully opaque.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => TRUE),
        ':input[name="header_side_overlay"]' => array('checked' => TRUE),
      ),
    ),
);

$form['glazed_settings']['header']['header_side']['header_side_align'] = array(
    '#type' => 'radios',
    '#title' => t('Align Content'),
    '#description' => t('Different header aligns give a different view at your menu and logo.'),
    '#default_value' => ((theme_get_setting('header_side_align') !== null)) ? theme_get_setting('header_side_align') : 'left',
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => TRUE),
      ),
    ),
);

$form['glazed_settings']['header']['header_side']['header_side_width'] = array(
  '#type' => 'textfield',
  '#title' => t('Width'),
  '#default_value' => ((theme_get_setting('header_side_width') !== null)) ? theme_get_setting('header_side_width') : '200',
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Width of the side-header. Default is 200.'),
    '#states' => array(
      'visible' => array(
        ':input[name="header_position"]' => array('checked' => TRUE),
      ),
    ),
);

// Header in mobile mode
$form['glazed_settings']['header']['header_mobile'] = array(
  '#title' => t('Mobile View Header'),
  '#type' => 'fieldset',
);

$form['glazed_settings']['header']['header_mobile']['header_mobile_height'] = array(
  '#type' => 'textfield',
  '#title' => t('Height'),
  '#default_value' => ((theme_get_setting('header_mobile_height') !== null)) ? theme_get_setting('header_mobile_height') : '60',
  '#size' => 9,
  '#maxlength' => 9,
  '#description' => t('Height of header in mobile view. 10px - 200px. Default is 60.'),
);

// Secondary Header
$form['glazed_settings']['header']['secondary_header'] = array(
  '#title' => t('Secondary Header'),
  '#type' => 'fieldset',
);

$form['glazed_settings']['header']['secondary_header']['secondary_header_hide'] = array(
    '#type' => 'radios',
    '#title' => t('Hide in small devices'),
    '#default_value' => ((theme_get_setting('secondary_header_hide') !== null)) ? theme_get_setting('secondary_header_hide') : 'hidden_none',
    '#options' => array(
      'hidden_none' => t('Never hide'),
      'hidden_xs' => t('Extra small devices (<768px)'),
      'hidden_sm' => t('Small devices (768px)'),
      'hidden_md' => t('Medium devices (992px)'),
    ),
);
