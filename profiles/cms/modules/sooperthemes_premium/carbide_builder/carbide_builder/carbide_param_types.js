(function($) {
  var p = '';
  var fp = '';
  if ('carbide_prefix' in window) {
    p = window.carbide_prefix;
    fp = window.carbide_prefix.replace('-', '_');
  }
  function toAbsoluteURL(url) {
    if (url.search(/^\/\//) != -1) {
      return window.location.protocol + url
    }
    if (url.search(/:\/\//) != -1) {
      return url
    }
    if (url.search(/^\//) != -1) {
      return window.location.origin + url
    }
    var base = window.location.href.match(/(.*\/)/)[0]
    return base + url
  }
  function t(text) {
    if ('carbide_t' in window) {
      return window.carbide_t(text);
    } else {
      return text;
    }
  }
  function lang() {
    if ('carbide_lang' in window) {
      return window.carbide_lang;
    } else {
      return 'en';
    }
  }

  window.carbide_open_popup = function(url) {
    window.open(url, '', 'location,width=800,height=600,top=0');
  }
  function chosen_select(options, input) {
    var single_select = '<select>';
    for (var key in options) {
      single_select = single_select + '<option value="' + key + '">"' + options[key] + '"</option>';
    }
    single_select = single_select + '</select>';
    $(input).css('display', 'none');
    var select = $(single_select).insertAfter(input);
    if ($(input).val().length) {
      $(select).append('<option value=""></option>');
      var value = $(input).val();
      if (!$(select).find('option[value="' + value + '"]').length) {
        $(select).append('<option value="' + value + '">"' + value + '"</option>');
      }
      $(select).find('option[value="' + value + '"]').attr("selected", "selected");
    } else {
      $(select).append('<option value="" selected></option>');
    }
    $(select).chosen({
      search_contains: true,
      allow_single_deselect: true,
    });
    $(select).change(function() {
      $(this).find('option:selected').each(function() {
        $(input).val($(this).val());
      });
    });
    $(select).parent().find('.chosen-container').width('100%');
    $('<div><a class="direct-input" href="#">' + t("Direct input") + '</a></div>').insertBefore(select).click(function() {
      $(input).css('display', 'block');
      $(select).parent().find('.chosen-container').remove();
      $(select).remove();
      $(this).remove();
    });
    return select;
  }
  function multiple_chosen_select(options, input, delimiter) {
    var multiple_select = '<select multiple="multiple">';
    var optgroup = '';
    for (var key in options) {
      if (key.indexOf("optgroup") >= 0) {
        if (optgroup == '') {
          multiple_select = multiple_select + '</optgroup>';
        }
        multiple_select = multiple_select + '<optgroup label="' + options[key] + '">';
        optgroup = options[key];
        continue;
      }
      multiple_select = multiple_select + '<option value="' + key + '">"' + options[key] + '"</option>';
    }
    if (optgroup != '') {
      multiple_select = multiple_select + '</optgroup>';
    }
    multiple_select = multiple_select + '</select>';
    $(input).css('display', 'none');
    var select = $(multiple_select).insertAfter(input);
    if ($(input).val().length) {
      var values = $(input).val().split(delimiter);
      for (var i = 0; i < values.length; i++) {
        if (!$(select).find('option[value="' + values[i] + '"]').length) {
          $(select).append('<option value="' + values[i] + '">"' + values[i] + '"</option>');
        }
        $(select).find('option[value="' + values[i] + '"]').attr("selected", "selected");
      }
    }
    $(select).chosen({
      search_contains: true,
    });
    $(select).change(function() {
      var selected = [];
      $(this).find('option:selected').each(function() {
        selected.push($(this).val());
      });
      $(input).val(selected.join(delimiter));
    });
    $(select).parent().find('.chosen-container').width('100%');
    $('<div><a class="direct-input" href="#">' + t("Direct input") + '</a></div>').insertBefore(select).click(function() {
      $(input).css('display', 'block');
      $(select).parent().find('.chosen-container').remove();
      $(select).remove();
      $(this).remove();
    });
    return select;
  }
  function image_select(input) {
    images_select(input, '');
  }
  function images_select(input, delimiter) {
    if ('images_select' in window) {
      window.images_select(input, delimiter);
    } else {
      function refresh_value(preview, input) {
        var value = [];
        _.each($(preview).find('> div'), function(img) {
          value.push(toAbsoluteURL(window.carbide_baseurl + 'filemanager' + $(img).attr('data-src').split('filemanager')[1]));
        });
        value = value.join(delimiter);
        $(input).val(value);
      }
      $(input).css('display', 'none');
      var preview = $('<div id="images-preview"></div>').insertAfter(input);
      var images = [];
      if (delimiter == '')
        images = [$(input).val()];
      else
        images = $(input).val().split(delimiter);
      for (var i = 0; i < images.length; i++) {
        if (images[i] != '') {
          var img = render_image(images[i], 100, 100);
          $(img).appendTo(preview).append('<div class="delete"></div>').click(function() {
            $(this).remove();
            refresh_value(preview, input);
          });
        }
      }
      var filemanager_input_id = _.uniqueId();
      var filemanager_input = $('<input id="' + filemanager_input_id + '" hidden type="text" name="filemanager">').insertAfter(input);
      var popup = $('<a href="javascript:carbide_open_popup(\'' + window.carbide_baseurl + 'filemanager/filemanager/dialog.php?type=1&popup=1&field_id=' + filemanager_input_id + '\')" class="' + p + 'btn ' + p + 'btn-default ' + p + 'glyphicon ' + p + 'glyphicon-picture" type="button"></a>').insertAfter(input);
      $('<div><a class="direct-input" href="#">' + t("Direct input") + '</a></div>').insertAfter(input).click(function() {
        $(input).css('display', 'block');
        $(preview).remove();
        $(filemanager_input).remove();
        $(popup).remove();
        $(this).remove();
      });
      var intervalID = setInterval(function() {
        if ($(filemanager_input).val() != '') {
          var srcs = [];
          _.each($(preview).find('> div'), function(img) {
            srcs.push(toAbsoluteURL(window.carbide_baseurl + 'filemanager' + $(img).attr('data-src').split('filemanager')[1]));
          });
          if (_.indexOf(srcs, $(filemanager_input).val()) < 0) {
            if (delimiter == '')
              srcs = [];
            srcs.push(toAbsoluteURL(window.carbide_baseurl + 'filemanager' + $(filemanager_input).val().split('filemanager')[1]));
            var img = render_image(toAbsoluteURL(window.carbide_baseurl + 'filemanager' + $(filemanager_input).val().split('filemanager')[1]), 100, 100);
            if (delimiter == '')
              $(preview).empty();
            $(img).appendTo(preview).append('<div class="delete"></div>').click(function() {
              $(this).remove();
              refresh_value(preview, input);
            });
            $(filemanager_input).val('');
            $(input).val(srcs.join(delimiter));
          }
        }
      }, 200);
      $(input).on("remove", function() {
        clearInterval(intervalID);
      });
      $(input).parent().find('#images-preview').sortable({
        items: '> div',
        placeholder: 'az-sortable-placeholder',
        forcePlaceholderSize: true,
        over: function(event, ui) {
          ui.placeholder.attr('class', ui.helper.attr('class'));
          ui.placeholder.attr('width', ui.helper.attr('width'));
          ui.placeholder.attr('height', ui.helper.attr('height'));
          ui.placeholder.removeClass('ui-sortable-helper');
          ui.placeholder.addClass('az-sortable-placeholder');
        },
        update: function() {
          refresh_value(preview, input);
        },
      });
    }
  }
  function colorpicker(input) {
    if ('wpColorPicker' in $.fn) {
      $(input).wpColorPicker();
      _.defer(function() {
        $(input).wpColorPicker({
          change: _.throttle(function() {
            $(input).trigger('change');
          }, 1000)
        });
      });
    } else {
      window.wpColorPickerL10n = {
        "clear": t("Clear"),
        "defaultString": t("Default"),
        "pick": t("Select Color"),
        "current": t("Current Color")
      }
      carbide_add_js({
        path: 'vendor/jquery.iris/dist/iris.min.js',
        callback: function() {
          carbide_add_js({
            path: 'js/carbide.iris.js',
            callback: function() {
              carbide_add_css('css/color-picker.min.css', function() {
                $(input).wpColorPicker();
                _.defer(function() {
                  $(input).wpColorPicker({
                    change: _.throttle(function() {
                      $(input).trigger('change');
                    }, 1000)
                  });
                });
              });
            }
          });
        }});
    }
  }
  function nouislider(slider, min, max, value, step, target) {
    carbide_add_css('vendor/noUiSlider/jquery.nouislider.min.css', function() {
    });
    carbide_add_js({
      path: 'vendor/noUiSlider/jquery.nouislider.min.js',
      callback: function() {
        $(slider).noUiSlider({
          start: [(value == '' || isNaN(parseFloat(value)) || value == 'NaN') ? min : parseFloat(value)],
          step: parseFloat(step),
          range: {
            min: [parseFloat(min)],
            max: [parseFloat(max)]
          },
        }).on('change', function() {
          $(target).val($(slider).val());
        });
      }
    });
  }
  function render_image(value, width, height) {
    if ($.isNumeric(width))
      width = width + 'px';
    if ($.isNumeric(height))
      height = height + 'px';
    var img = $('<div style="background-image: url(' + encodeURI(value) + ');" data-src="' + value + '" ></div>');
    if (width.length > 0)
      $(img).css('width', width);
    if (height.length > 0)
      $(img).css('height', height);
    return img;
  }

  var icons = [];
  if ('carbide_icons' in window)
    icons = window.carbide_icons;

  var carbide_param_types = [
    {
      type: 'dropdown',
      get_value: function() {
        return $(this.dom_element).find('select[name="' + this.param_name + '"] > option:selected').val();
      },
      render: function(value) {
        var select = '<select name="' + this.param_name + '" class="' + p + 'form-control">';
        for (var name in this.value) {
          var option = '';
          if (name == value) {
            option = '<option selected value="' + name + '">' + this.value[name] + '</option>';
          } else {
            option = '<option value="' + name + '">' + this.value[name] + '</option>';
          }
          select += option;
        }
        select += '/<select>';

        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div>' + select + '</div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      }
    },
    {
      type: 'checkbox',
      get_value: function() {
        var values = [];
        _.each($(this.dom_element).find('input[name="' + this.param_name + '"]:checked'), function(obj) {
          values.push($(obj).val());
        });
        return values.join(',');
      },
      render: function(value) {
        if (value == null)
          value = '';
        var values = value.split(',');
        var inputs = '';
        for (var name in this.value) {
          if (_.indexOf(values, name) >= 0) {
            inputs += '<div class="' + p + 'checkbox"><label><input name="' + this.param_name + '" type="checkbox" checked value="' + name + '">' + this.value[name] + '</label></div>';
          } else {
            inputs += '<div class="' + p + 'checkbox"><label><input name="' + this.param_name + '" type="checkbox" value="' + name + '">' + this.value[name] + '</label></div>';
          }
        }
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div>' + inputs + '</div><div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      }
    },
    {
      type: 'textfield',
      get_value: function() {
        return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      },
      render: function(value) {
        var required = this.required ? 'required' : '';
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><input class="' + p + 'form-control" name="' + this.param_name + '" type="text" value="' + value + '" ' + required + '></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      }
    },
    {
      type: 'textarea',
      safe: false,
      get_value: function() {
        // Return data.
        return CKEDITOR.instances[this.id].getData();
      },
      render: function(value) {
        this.id = _.uniqueId();
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><textarea id="' + this.id + '" rows="10" cols="45" name="' + this.param_name + '" ">' + value + '</textarea></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
      opened: function() {
        var param = this;
        if ('carbide_ckeditor' in window) {
          window.carbide_ckeditor($(this.dom_element).find('#' + param.id));
        } else {
          function ckeditor_add_editor() {

            // Don't add spaces to empty blocks
            CKEDITOR.config.fillEmptyBlocks = false;
            // Disabling content filtering.
            CKEDITOR.config.allowedContent = true;
            // Prevent wrapping inline content in paragraphs
            CKEDITOR.config.autoParagraph = false;

            // Theme integration
            CKEDITOR.config.contentsCss = ['//cdn.jsdelivr.net/bootstrap/3.3.5/css/bootstrap.css'];
            if (typeof window.Drupal.settings.glazed.glazedPath.length != "undefined") {
              CKEDITOR.config.contentsCss.push(Drupal.settings.basePath + window.Drupal.settings.glazed.glazedPath + 'css/glazed.css');
            }

            // Styles dropdown
            CKEDITOR.config.stylesSet = [
              { name: 'Lead', element: 'p', attributes: { 'class': 'lead' } },
              { name: 'Muted', element: 'p', attributes: { 'class': 'text-muted' } },
              { name: 'Highlighted', element: 'mark' },
              { name: 'Small', element: 'small' },
              { name: 'Button Primary', element: 'div', attributes: { 'class': 'btn btn-primary' } },
              { name: 'Button Default', element: 'div', attributes: { 'class': 'btn btn-default' } },
            ];

            var palette = [];
            for (var name in window.sooperthemes_theme_palette) {
              palette.push(window.sooperthemes_theme_palette[name].substring(1));
            }
            CKEDITOR.config.colorButton_colors = palette.join(',') + ',' + CKEDITOR.config.colorButton_colors;

            // Added config toolbar
            CKEDITOR.config.toolbar = [
              { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Superscript', 'Subscript', 'RemoveFormat']},
              { name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BulletedList', 'NumberedList', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv']},
              { name: 'clipboard', items: ['Undo', 'Redo', 'PasteText', 'PasteFromWord']},
              { name: 'links', items: ['Link', 'Unlink']},
              { name: 'insert', items: ['Image', 'HorizontalRule', 'SpecialChar', 'Table', 'Templates']},
              { name: 'colors', items: ['TextColor']},
              { name: 'document', items: ['Source']},
              { name: 'tools', items: ['ShowBlocks', 'Maximize']},
              { name: 'styles', items: ['Format', 'Styles']},
              { name: 'editing', items: ['Scayt']},
            ];

            // Don't move about our Carbide Builder stylesheet link tags
            CKEDITOR.config.protectedSource.push(/<link.*?>/gi);

            CKEDITOR.replace(param.id);
          }
          if ('CKEDITOR' in window) {
            ckeditor_add_editor();
          } else {
            carbide_add_js({
              path: 'vendor/ckeditor/ckeditor.js',
              callback: function() {
                if (_.isObject(CKEDITOR)) {
                  ckeditor_add_editor();
                }
              }
            });
          }
        }
      },
      closed: function() {
        // Destroy ckeditor.
        CKEDITOR.instances[this.id].destroy();
      }
    },
    {
      type: 'html',
      safe: false,
      get_value: function() {
        return $(this.dom_element).find('#' + this.id).val();
      },
      opened: function() {
        var param = this;
        carbide_add_js({
          path: 'vendor/ace/ace.js',
          callback: function() {
            var aceeditor = ace.edit(param.id);
            aceeditor.setTheme("ace/theme/chrome");
            aceeditor.getSession().setMode("ace/mode/html");
            aceeditor.setOptions({
              minLines: 10,
              maxLines: 30,
            });
            $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
            aceeditor.on(
                'change', function(e) {
                  $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
                  aceeditor.resize();
                }
            );
          }
        });
      },
      render: function(value) {
        this.id = _.uniqueId();
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div id="' + this.id + '"><textarea class="' + p + 'form-control" rows="10" cols="45" name="' + this.param_name + '" ">' + value + '</textarea></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
    },
    {
      type: 'css',
      safe: false,
      get_value: function() {
        return $(this.dom_element).find('#' + this.id).val();
      },
      opened: function() {
        var param = this;
        carbide_add_js({
          path: 'vendor/ace/ace.js',
          callback: function() {
            var aceeditor = ace.edit(param.id);
            aceeditor.setTheme("ace/theme/chrome");
            aceeditor.getSession().setMode("ace/mode/css");
            aceeditor.setOptions({
              minLines: 10,
              maxLines: 30,
            });
            $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
            aceeditor.on(
                'change', function(e) {
                  $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
                  aceeditor.resize();
                }
            );
          }
        });
      },
      render: function(value) {
        this.id = _.uniqueId();
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div id="' + this.id + '"><textarea class="' + p + 'form-control" rows="10" cols="45" name="' + this.param_name + '" ">' + value + '</textarea></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
    },
    {
      type: 'javascript',
      safe: false,
      get_value: function() {
        return $(this.dom_element).find('#' + this.id).val();
      },
      opened: function() {
        var param = this;
        carbide_add_js({
          path: 'vendor/ace/ace.js',
          callback: function() {
            var aceeditor = ace.edit(param.id);
            aceeditor.setTheme("ace/theme/chrome");
            aceeditor.getSession().setMode("ace/mode/javascript");
            aceeditor.setOptions({
              minLines: 10,
              maxLines: 30,
            });
            $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
            aceeditor.on(
                'change', function(e) {
                  $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
                  aceeditor.resize();
                }
            );
          }
        });
      },
      render: function(value) {
        this.id = _.uniqueId();
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div id="' + this.id + '"><textarea class="' + p + 'form-control" rows="10" cols="45" name="' + this.param_name + '" ">' + value + '</textarea></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
    },
    {
      type: 'rawtext',
      safe: false,
      get_value: function() {
        return $(this.dom_element).find('#' + this.id).val();
      },
      render: function(value) {
        this.id = _.uniqueId();
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><textarea id="' + this.id + '" class="' + p + 'form-control" rows="10" cols="45" name="' + this.param_name + '" ">' + value + '</textarea></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
    },
    {
      type: 'colorpicker',
      get_value: function() {
        return $(this.dom_element).find('#' + this.id).val();
      },
      render: function(value) {
        this.id = _.uniqueId();
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><input id="' + this.id + '" name="' + this.param_name + '" type="text" value="' + value + '"></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
      opened: function() {
        colorpicker('#' + this.id);
      },
    },
    {
      type: 'style',
      create: function() {
        this.important = false;
      },
      get_value: function() {
        var important_str = '';
        if (this.important) {
          important_str = ' !important';
        }
        var style = '';
        var margin_top = $(this.dom_element).find('[name="margin_top"]').val();
        if (margin_top != '') {
          if ($.isNumeric(margin_top))
            margin_top = margin_top + 'px';
          style += 'margin-top:' + margin_top + important_str + ';';
        }
        var margin_bottom = $(this.dom_element).find('[name="margin_bottom"]').val();
        if (margin_bottom != '') {
          if ($.isNumeric(margin_bottom))
            margin_bottom = margin_bottom + 'px';
          style += 'margin-bottom:' + margin_bottom + important_str + ';';
        }
        var margin_left = $(this.dom_element).find('[name="margin_left"]').val();
        if (margin_left != '') {
          if ($.isNumeric(margin_left))
            margin_left = margin_left + 'px';
          style += 'margin-left:' + margin_left + important_str + ';';
        }
        var margin_right = $(this.dom_element).find('[name="margin_right"]').val();
        if (margin_right != '') {
          if ($.isNumeric(margin_right))
            margin_right = margin_right + 'px';
          style += 'margin-right:' + margin_right + important_str + ';';
        }


        var border_top_width = $(this.dom_element).find('[name="border_top_width"]').val();
        if (border_top_width != '') {
          if ($.isNumeric(border_top_width))
            border_top_width = border_top_width + 'px';
          style += 'border-top-width:' + border_top_width + important_str + ';';
        }
        var border_bottom_width = $(this.dom_element).find('[name="border_bottom_width"]').val();
        if (border_bottom_width != '') {
          if ($.isNumeric(border_bottom_width))
            border_bottom_width = border_bottom_width + 'px';
          style += 'border-bottom-width:' + border_bottom_width + important_str + ';';
        }
        var border_left_width = $(this.dom_element).find('[name="border_left_width"]').val();
        if (border_left_width != '') {
          if ($.isNumeric(border_left_width))
            border_left_width = border_left_width + 'px';
          style += 'border-left-width:' + border_left_width + important_str + ';';
        }
        var border_right_width = $(this.dom_element).find('[name="border_right_width"]').val();
        if (border_right_width != '') {
          if ($.isNumeric(border_right_width))
            border_right_width = border_right_width + 'px';
          style += 'border-right-width:' + border_right_width + important_str + ';';
        }


        var padding_top = $(this.dom_element).find('[name="padding_top"]').val();
        if (padding_top != '') {
          if ($.isNumeric(padding_top))
            padding_top = padding_top + 'px';
          style += 'padding-top:' + padding_top + important_str + ';';
        }
        var padding_bottom = $(this.dom_element).find('[name="padding_bottom"]').val();
        if (padding_bottom != '') {
          if ($.isNumeric(padding_bottom))
            padding_bottom = padding_bottom + 'px';
          style += 'padding-bottom:' + padding_bottom + important_str + ';';
        }
        var padding_left = $(this.dom_element).find('[name="padding_left"]').val();
        if (padding_left != '') {
          if ($.isNumeric(padding_left))
            padding_left = padding_left + 'px';
          style += 'padding-left:' + padding_left + important_str + ';';
        }
        var padding_right = $(this.dom_element).find('[name="padding_right"]').val();
        if (padding_right != '') {
          if ($.isNumeric(padding_right))
            padding_right = padding_right + 'px';
          style += 'padding-right:' + padding_right + important_str + ';';
        }

        var color = $(this.dom_element).find('#' + this.color_id).val();
        if (color != '') {
          style += 'color:' + color + important_str + ';';
        }
        var fontsize = $(this.dom_element).find('[name="fontsize"]').val();
        if (fontsize != '') {
          if ($.isNumeric(fontsize))
            fontsize = Math.round(fontsize) + 'px';
          style += 'font-size:' + fontsize + important_str + ';';
        }

        var border_color = $(this.dom_element).find('#' + this.border_color_id).val();
        if (border_color != '') {
          style += 'border-color:' + border_color + important_str + ';';
        }
        var border_radius = $(this.dom_element).find('[name="border_radius"]').val();
        if (border_radius != '') {
          if ($.isNumeric(border_radius))
            border_radius = Math.round(border_radius) + '%';
          style += 'border-radius:' + border_radius + important_str + ';';
        }
        var border_style = $(this.dom_element).find('select[name="border_style"] > option:selected').val();
        if (border_style != '') {
          style += 'border-style:' + border_style + important_str + ';';
        }

        var bg_color = $(this.dom_element).find('#' + this.bg_color_id).val();
        if (bg_color) {
          style += 'background-color:' + bg_color + important_str + ';';
        }
        var bg_image = $(this.dom_element).find('[name="bg_image"]').val();
        if (bg_image) {
          style += 'background-image: url(' + encodeURI(bg_image) + ');';
        }
        var background_style = $(this.dom_element).find('select[name="background_style"] > option:selected').val();
        if (background_style.match(/repeat/)) {
          if (background_style.match(/repeat-x/)) {
            style += 'background-position: center bottom;';
            style += 'background-repeat: repeat-x' + important_str + ';';
          } else {
            if (background_style.match(/no-repeat/)) {
              style += 'background-position: center;';
            } else {
              style += 'background-position: 0 0;';
            }
            style += 'background-repeat: ' + background_style + important_str + ';';
          }
        } else if (background_style.match(/cover|contain/)) {
          style += 'background-position: center;';
          style += 'background-repeat: no-repeat;';
          style += 'background-size: ' + background_style + important_str + ';';
        }
        var opacity = $(this.dom_element).find('[name="opacity"]').val();
        if (opacity != '') {
          style += 'opacity:' + opacity + important_str + ';';
        }
        return style;
      },
      render: function(value) {
        value = value.replace(/!important/g, '');
        var output = '<div class="style ' + p + 'row">';
        var match = null;
        var v = '';

        output += '<div class="layout ' + p + 'col-sm-6">';

        output += '<div class="margin"><label>' + t('Margin') + '</label>';
        match = value.match(/margin-top[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="margin_top" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/margin-bottom[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="margin_bottom" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/margin-left[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="margin_left" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/margin-right[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="margin_right" type="text" placeholder="-" value="' + v + '">';



        output += '<div class="border"><label>' + t('Border') + '</label>';
        match = value.match(/border-top-width[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="border_top_width" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/border-bottom-width[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="border_bottom_width" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/border-left-width[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="border_left_width" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/border-right-width[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="border_right_width" type="text" placeholder="-" value="' + v + '">';



        output += '<div class="padding"><label>' + t('Padding') + '</label>';
        match = value.match(/padding-top[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="padding_top" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/padding-bottom[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="padding_bottom" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/padding-left[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="padding_left" type="text" placeholder="-" value="' + v + '">';
        match = value.match(/padding-right[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="padding_right" type="text" placeholder="-" value="' + v + '">';



        output += '<div class="content">';
        output += '</div></div></div></div>';

        output += '</div>';


        output += '<div class="settings ' + p + 'col-sm-6">';


        output += '<div class="font ' + p + 'form-group"><label>' + t('Font') + '</label>';
        this.color_id = _.uniqueId();
        match = value.match(/(^| |;)color[: ]*([#\dabcdef]*) *;/);
        if (match == null)
          v = '';
        else
          v = match[2];
        output += '<div><input id="' + this.color_id + '" name="color" type="text" value="' + v + '"></div>';
        match = value.match(/font-size[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="fontsize" class="' + p + 'form-control" type="text" placeholder="' + t('Font size') + '" value="' + v + '">';
        output += '<div class="fontsize-slider"></div>';

        output += '</div>';



        output += '<div class="border ' + p + 'form-group"><label>' + t('Border') + '</label>';
        this.border_color_id = _.uniqueId();
        match = value.match(/border-color[: ]*([#\dabcdef]*) *;/);
        if (match == null)
          v = '';
        else
          v = match[1];
        output += '<div><input id="' + this.border_color_id + '" name="border_color" type="text" value="' + v + '"></div>';
        match = value.match(/border-radius[: ]*([\-\d\.]*)(px|%|em) *;/);
        if (match == null)
          v = '';
        else
          v = match[1] + match[2];
        output += '<input name="border_radius" class="' + p + 'form-control" type="text" placeholder="' + t('Border radius') + '" value="' + v + '">';
        output += '<div class="radius-slider"></div>';
        match = value.match(/border-style[: ]*(\w*) *;/);
        if (match == null)
          v = '';
        else
          v = match[1];
        output += '<select name="border_style" class="' + p + 'form-control">';
        var border_styles = {
          '': t("Theme defaults"),
          'solid': t("Solid"),
          'dotted': t("Dotted"),
          'dashed': t("Dashed"),
          'none': t("None"),
          'hidden': t("Hidden"),
          'double': t("Double"),
          'groove': t("Groove"),
          'ridge': t("Ridge"),
          'inset': t("Inset"),
          'outset': t("Outset"),
          'initial': t("Initial"),
          'inherit': t("Inherit"),
        };
        for (var key in border_styles) {
          if (key == v)
            output += '<option selected value="' + key + '">' + border_styles[key] + '</option>';
          else
            output += '<option value="' + key + '">' + border_styles[key] + '</option>';
        }
        output += '</select>';
        output += '</div>';

        output += '<div class="background ' + p + 'form-group"><label>' + t('Background') + '</label>';
        this.bg_color_id = _.uniqueId();
        match = value.match(/background-color[: ]*([#\dabcdef]*) *;/);
        if (match == null)
          v = '';
        else
          v = match[1];
        output += '<div><input id="' + this.bg_color_id + '" name="bg_color" type="text" value="' + v + '"></div>';

        this.bg_image_id = _.uniqueId();
        match = value.match(/background-image[: ]*url\(([^\)]+)\) *;/);
        if (match == null)
          v = '';
        else
          v = decodeURI(match[1]);
        output += '<input id="' + this.bg_image_id + '" name="bg_image" class="' + p + 'form-control" type="text" value="' + v + '">';

        match = value.match(/background-repeat[: ]*([-\w]*) *;/);
        if (match == null) {
          v = '';
        } else {
          if (match[1] == 'repeat') {
            v = match[1];
          } else {
            if (match[1] == 'repeat-x') {
              v = 'repeat-x';
            } else {
              match = value.match(/background-size[: ]*([-\w]*) *;/);
              if (match == null) {
                v = 'no-repeat';
              } else {
                v = match[1];
              }
            }
          }
        }
        output += '<select name="background_style" class="' + p + 'form-control">';
        var background_styles = {
          '': t("Theme defaults"),
          'cover': t("Cover"),
          'contain': t("Contain"),
          'no-repeat': t("No Repeat"),
          'repeat': t("Repeat"),
          'repeat-x': t("Horizontal repeat bottom"),
        };
        for (var key in background_styles) {
          if (key == v)
            output += '<option selected value="' + key + '">' + background_styles[key] + '</option>';
          else
            output += '<option value="' + key + '">' + background_styles[key] + '</option>';
        }
        output += '</select>';

        match = value.match(/opacity[: ]*([\d\.]*) *;/);
        if (match == null)
          v = '';
        else
          v = match[1];
        output += '<input name="opacity" class="' + p + 'form-control" type="text" placeholder="' + t('Opacity') + '" value="' + v + '">';
        output += '<div class="opacity-slider"></div>';


        output += '</div>';

        output += '</div>';

        output += '</div>';
        this.dom_element = $(output);
      },
      opened: function() {
        image_select($(this.dom_element).find('input[name="bg_image"]'));
        colorpicker('#' + this.color_id);
        colorpicker('#' + this.border_color_id);
        colorpicker('#' + this.bg_color_id);
        nouislider($(this.dom_element).find('.opacity-slider'), 0, 1, $(this.dom_element).find('input[name="opacity"]').val(), 0.01, $(this.dom_element).find('input[name="opacity"]'));
        nouislider($(this.dom_element).find('.fontsize-slider'), 0, 100, $(this.dom_element).find('input[name="fontsize"]').val(), 1, $(this.dom_element).find('input[name="fontsize"]'));
        nouislider($(this.dom_element).find('.radius-slider'), 0, 100, $(this.dom_element).find('input[name="border_radius"]').val(), 1, $(this.dom_element).find('input[name="border_radius"]'));
      },
    },
    {
      type: 'google_font',
      hidden: !'carbide_google_fonts' in window,
      get_value: function() {
        var font = $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
        var subset = $(this.dom_element).find('input[name="' + this.param_name + '_subset"]').val();
        var variant = $(this.dom_element).find('input[name="' + this.param_name + '_variant"]').val();
        return font + '|' + subset + '|' + variant;
      },
      render: function(value) {
        var font = '';
        var subset = '';
        var variant = '';
        if (_.isString(value) && value != '' && value.split('|').length == 3) {
          font = value.split('|')[0];
          subset = value.split('|')[1];
          variant = value.split('|')[2];
        }
        var font_input = '<div class="' + p + 'col-sm-4"><label>' + this.heading + '</label><input class="' + p + 'form-control" name="' + this.param_name + '" type="text" value="' + font + '"></div>';
        var subset_input = '<div class="' + p + 'col-sm-4"><label>' + t('Subset') + '</label><input class="' + p + 'form-control" name="' + this.param_name + '_subset" type="text" value="' + subset + '"></div>';
        var variant_input = '<div class="' + p + 'col-sm-4"><label>' + t('Variant') + '</label><input class="' + p + 'form-control" name="' + this.param_name + '_variant" type="text" value="' + variant + '"></div>';
        this.dom_element = $('<div class="' + p + 'form-group"><div class="' + p + 'row">' + font_input + subset_input + variant_input + '</div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
      opened: function() {
        var element = this;
        var fonts = Object.keys(window.carbide_google_fonts);
        fonts = _.object(fonts, fonts);
        var font_select = null;
        var subset_select = null;
        var variant_select = null;
        font_select = chosen_select(fonts, $(this.dom_element).find('input[name="' + this.param_name + '"]'));
        $(font_select).chosen().change(function() {
          var f = Object.keys(window.carbide_google_fonts)[0];
          if ($(this).val() in window.carbide_google_fonts)
            f = window.carbide_google_fonts[$(this).val()];
          var subsets = {};
          for (var i = 0; i < f.subsets.length; i++) {
            subsets[f.subsets[i].id] = f.subsets[i].name;
          }
          var variants = {};
          for (var i = 0; i < f.variants.length; i++) {
            variants[f.variants[i].id] = f.variants[i].name;
          }

          $(subset_select).parent().find('.direct-input').click();
          subset_select = chosen_select(subsets, $(element.dom_element).find('input[name="' + element.param_name + '_subset"]'));

          $(variant_select).parent().find('.direct-input').click();
          variant_select = chosen_select(variants, $(element.dom_element).find('input[name="' + element.param_name + '_variant"]'));
        });
        $(font_select).chosen().trigger('change');
      },
    },
    {
      type: 'image',
      get_value: function() {
        return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      },
      render: function(value) {
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><input class="' + p + 'form-control" name="' + this.param_name + '" type="text" value="' + value + '"></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
      opened: function() {
        image_select($(this.dom_element).find('input[name="' + this.param_name + '"]'));
      },
    },
    {
      type: 'images',
      get_value: function() {
        return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      },
      render: function(value) {
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><input class="' + p + 'form-control" name="' + this.param_name + '" type="text" value="' + value + '"></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
      opened: function() {
        images_select($(this.dom_element).find('input[name="' + this.param_name + '"]'), ',');
      },
    },
    {
      type: 'icon',
      icons: [
        // Glyphicons
        'glyphicon glyphicon-asterisk', 'glyphicon glyphicon-plus', 'glyphicon glyphicon-euro', 'glyphicon glyphicon-minus', 'glyphicon glyphicon-cloud', 'glyphicon glyphicon-envelope', 'glyphicon glyphicon-pencil', 'glyphicon glyphicon-glass', 'glyphicon glyphicon-music', 'glyphicon glyphicon-search', 'glyphicon glyphicon-heart', 'glyphicon glyphicon-star', 'glyphicon glyphicon-star-empty', 'glyphicon glyphicon-user', 'glyphicon glyphicon-film', 'glyphicon glyphicon-th-large', 'glyphicon glyphicon-th', 'glyphicon glyphicon-th-list', 'glyphicon glyphicon-ok', 'glyphicon glyphicon-remove', 'glyphicon glyphicon-zoom-in', 'glyphicon glyphicon-zoom-out', 'glyphicon glyphicon-off', 'glyphicon glyphicon-signal', 'glyphicon glyphicon-cog', 'glyphicon glyphicon-trash', 'glyphicon glyphicon-home', 'glyphicon glyphicon-file', 'glyphicon glyphicon-time', 'glyphicon glyphicon-road', 'glyphicon glyphicon-download-alt', 'glyphicon glyphicon-download', 'glyphicon glyphicon-upload', 'glyphicon glyphicon-inbox', 'glyphicon glyphicon-play-circle', 'glyphicon glyphicon-repeat', 'glyphicon glyphicon-refresh', 'glyphicon glyphicon-list-alt', 'glyphicon glyphicon-lock', 'glyphicon glyphicon-flag', 'glyphicon glyphicon-headphones', 'glyphicon glyphicon-volume-off', 'glyphicon glyphicon-volume-down', 'glyphicon glyphicon-volume-up', 'glyphicon glyphicon-qrcode', 'glyphicon glyphicon-barcode', 'glyphicon glyphicon-tag', 'glyphicon glyphicon-tags', 'glyphicon glyphicon-book', 'glyphicon glyphicon-bookmark', 'glyphicon glyphicon-print', 'glyphicon glyphicon-camera', 'glyphicon glyphicon-font', 'glyphicon glyphicon-bold', 'glyphicon glyphicon-italic', 'glyphicon glyphicon-text-height', 'glyphicon glyphicon-text-width', 'glyphicon glyphicon-align-left', 'glyphicon glyphicon-align-center', 'glyphicon glyphicon-align-right', 'glyphicon glyphicon-align-justify', 'glyphicon glyphicon-list', 'glyphicon glyphicon-indent-left', 'glyphicon glyphicon-indent-right', 'glyphicon glyphicon-facetime-video', 'glyphicon glyphicon-picture', 'glyphicon glyphicon-map-marker', 'glyphicon glyphicon-adjust', 'glyphicon glyphicon-tint', 'glyphicon glyphicon-edit', 'glyphicon glyphicon-share', 'glyphicon glyphicon-check', 'glyphicon glyphicon-move', 'glyphicon glyphicon-step-backward', 'glyphicon glyphicon-fast-backward', 'glyphicon glyphicon-backward', 'glyphicon glyphicon-play', 'glyphicon glyphicon-pause', 'glyphicon glyphicon-stop', 'glyphicon glyphicon-forward', 'glyphicon glyphicon-fast-forward', 'glyphicon glyphicon-step-forward', 'glyphicon glyphicon-eject', 'glyphicon glyphicon-chevron-left', 'glyphicon glyphicon-chevron-right', 'glyphicon glyphicon-plus-sign', 'glyphicon glyphicon-minus-sign', 'glyphicon glyphicon-remove-sign', 'glyphicon glyphicon-ok-sign', 'glyphicon glyphicon-question-sign', 'glyphicon glyphicon-info-sign', 'glyphicon glyphicon-screenshot', 'glyphicon glyphicon-remove-circle', 'glyphicon glyphicon-ok-circle', 'glyphicon glyphicon-ban-circle', 'glyphicon glyphicon-arrow-left', 'glyphicon glyphicon-arrow-right', 'glyphicon glyphicon-arrow-up', 'glyphicon glyphicon-arrow-down', 'glyphicon glyphicon-share-alt', 'glyphicon glyphicon-resize-full', 'glyphicon glyphicon-resize-small', 'glyphicon glyphicon-exclamation-sign', 'glyphicon glyphicon-gift', 'glyphicon glyphicon-leaf', 'glyphicon glyphicon-fire', 'glyphicon glyphicon-eye-open', 'glyphicon glyphicon-eye-close', 'glyphicon glyphicon-warning-sign', 'glyphicon glyphicon-plane', 'glyphicon glyphicon-calendar', 'glyphicon glyphicon-random', 'glyphicon glyphicon-comment', 'glyphicon glyphicon-magnet', 'glyphicon glyphicon-chevron-up', 'glyphicon glyphicon-chevron-down', 'glyphicon glyphicon-retweet', 'glyphicon glyphicon-shopping-cart', 'glyphicon glyphicon-folder-close', 'glyphicon glyphicon-folder-open', 'glyphicon glyphicon-resize-vertical', 'glyphicon glyphicon-resize-horizontal', 'glyphicon glyphicon-hdd', 'glyphicon glyphicon-bullhorn', 'glyphicon glyphicon-bell', 'glyphicon glyphicon-certificate', 'glyphicon glyphicon-thumbs-up', 'glyphicon glyphicon-thumbs-down', 'glyphicon glyphicon-hand-right', 'glyphicon glyphicon-hand-left', 'glyphicon glyphicon-hand-up', 'glyphicon glyphicon-hand-down', 'glyphicon glyphicon-circle-arrow-right', 'glyphicon glyphicon-circle-arrow-left', 'glyphicon glyphicon-circle-arrow-up', 'glyphicon glyphicon-circle-arrow-down', 'glyphicon glyphicon-globe', 'glyphicon glyphicon-wrench', 'glyphicon glyphicon-tasks', 'glyphicon glyphicon-filter', 'glyphicon glyphicon-briefcase', 'glyphicon glyphicon-fullscreen', 'glyphicon glyphicon-dashboard', 'glyphicon glyphicon-paperclip', 'glyphicon glyphicon-heart-empty', 'glyphicon glyphicon-link', 'glyphicon glyphicon-phone', 'glyphicon glyphicon-pushpin', 'glyphicon glyphicon-usd', 'glyphicon glyphicon-gbp', 'glyphicon glyphicon-sort', 'glyphicon glyphicon-sort-by-alphabet', 'glyphicon glyphicon-sort-by-alphabet-alt', 'glyphicon glyphicon-sort-by-order', 'glyphicon glyphicon-sort-by-order-alt', 'glyphicon glyphicon-sort-by-attributes', 'glyphicon glyphicon-sort-by-attributes-alt', 'glyphicon glyphicon-unchecked', 'glyphicon glyphicon-expand', 'glyphicon glyphicon-collapse-down', 'glyphicon glyphicon-collapse-up', 'glyphicon glyphicon-log-in', 'glyphicon glyphicon-flash', 'glyphicon glyphicon-log-out', 'glyphicon glyphicon-new-window', 'glyphicon glyphicon-record', 'glyphicon glyphicon-save', 'glyphicon glyphicon-open', 'glyphicon glyphicon-saved', 'glyphicon glyphicon-import', 'glyphicon glyphicon-export', 'glyphicon glyphicon-send', 'glyphicon glyphicon-floppy-disk', 'glyphicon glyphicon-floppy-saved', 'glyphicon glyphicon-floppy-remove', 'glyphicon glyphicon-floppy-save', 'glyphicon glyphicon-floppy-open', 'glyphicon glyphicon-credit-card', 'glyphicon glyphicon-transfer', 'glyphicon glyphicon-cutlery', 'glyphicon glyphicon-header', 'glyphicon glyphicon-compressed', 'glyphicon glyphicon-earphone', 'glyphicon glyphicon-phone-alt', 'glyphicon glyphicon-tower', 'glyphicon glyphicon-stats', 'glyphicon glyphicon-sd-video', 'glyphicon glyphicon-hd-video', 'glyphicon glyphicon-subtitles', 'glyphicon glyphicon-sound-stereo', 'glyphicon glyphicon-sound-dolby', 'glyphicon glyphicon-sound-5-1', 'glyphicon glyphicon-sound-6-1', 'glyphicon glyphicon-sound-7-1', 'glyphicon glyphicon-copyright-mark', 'glyphicon glyphicon-registration-mark', 'glyphicon glyphicon-cloud-download', 'glyphicon glyphicon-cloud-upload', 'glyphicon glyphicon-tree-conifer', 'glyphicon glyphicon-tree-deciduous',
        // ET Line icons
        'et et-icon-mobile', 'et et-icon-laptop', 'et et-icon-desktop', 'et et-icon-tablet', 'et et-icon-phone', 'et et-icon-document', 'et et-icon-documents', 'et et-icon-search', 'et et-icon-clipboard', 'et et-icon-newspaper', 'et et-icon-notebook', 'et et-icon-book-open', 'et et-icon-browser', 'et et-icon-calendar', 'et et-icon-presentation', 'et et-icon-picture', 'et et-icon-pictures', 'et et-icon-video', 'et et-icon-camera', 'et et-icon-printer', 'et et-icon-toolbox', 'et et-icon-briefcase', 'et et-icon-wallet', 'et et-icon-gift', 'et et-icon-bargraph', 'et et-icon-grid', 'et et-icon-expand', 'et et-icon-focus', 'et et-icon-edit', 'et et-icon-adjustments', 'et et-icon-ribbon', 'et et-icon-hourglass', 'et et-icon-lock', 'et et-icon-megaphone', 'et et-icon-shield', 'et et-icon-trophy', 'et et-icon-flag', 'et et-icon-map', 'et et-icon-puzzle', 'et et-icon-basket', 'et et-icon-envelope', 'et et-icon-streetsign', 'et et-icon-telescope', 'et et-icon-gears', 'et et-icon-key', 'et et-icon-paperclip', 'et et-icon-attachment', 'et et-icon-pricetags', 'et et-icon-lightbulb', 'et et-icon-layers', 'et et-icon-pencil', 'et et-icon-tools', 'et et-icon-tools-2', 'et et-icon-scissors', 'et et-icon-paintbrush', 'et et-icon-magnifying-glass', 'et et-icon-circle-compass', 'et et-icon-linegraph', 'et et-icon-mic', 'et et-icon-strategy', 'et et-icon-beaker', 'et et-icon-caution', 'et et-icon-recycle', 'et et-icon-anchor', 'et et-icon-profile-male', 'et et-icon-profile-female', 'et et-icon-bike', 'et et-icon-wine', 'et et-icon-hotairballoon', 'et et-icon-globe', 'et et-icon-genius', 'et et-icon-map-pin', 'et et-icon-dial', 'et et-icon-chat', 'et et-icon-heart', 'et et-icon-cloud', 'et et-icon-upload', 'et et-icon-download', 'et et-icon-target', 'et et-icon-hazardous', 'et et-icon-piechart', 'et et-icon-speedometer', 'et et-icon-global', 'et et-icon-compass', 'et et-icon-lifesaver', 'et et-icon-clock', 'et et-icon-aperture', 'et et-icon-quote', 'et et-icon-scope', 'et et-icon-alarmclock', 'et et-icon-refresh', 'et et-icon-happy', 'et et-icon-sad', 'et et-icon-facebook', 'et et-icon-twitter', 'et et-icon-googleplus', 'et et-icon-rss', 'et et-icon-tumblr', 'et et-icon-linkedin', 'et et-icon-dribbble',
        // Font Awesome
        'fa fa-glass ', 'fa fa-music ', 'fa fa-search ', 'fa fa-envelope-o ', 'fa fa-heart ', 'fa fa-star ', 'fa fa-star-o ', 'fa fa-user ', 'fa fa-film ', 'fa fa-th-large ', 'fa fa-th ', 'fa fa-th-list ', 'fa fa-check ', 'fa fa-remove', 'fa fa-times ', 'fa fa-search-plus ', 'fa fa-search-minus ', 'fa fa-power-off ', 'fa fa-signal ', 'fa fa-gear', 'fa fa-trash-o ', 'fa fa-home ', 'fa fa-file-o ', 'fa fa-clock-o ', 'fa fa-road ', 'fa fa-download ', 'fa fa-arrow-circle-o-down ', 'fa fa-arrow-circle-o-up ', 'fa fa-inbox ', 'fa fa-play-circle-o ', 'fa fa-rotate-right', 'fa fa-refresh ', 'fa fa-list-alt ', 'fa fa-lock ', 'fa fa-flag ', 'fa fa-headphones ', 'fa fa-volume-off ', 'fa fa-volume-down ', 'fa fa-volume-up ', 'fa fa-qrcode ', 'fa fa-barcode ', 'fa fa-tag ', 'fa fa-tags ', 'fa fa-book ', 'fa fa-bookmark ', 'fa fa-print ', 'fa fa-camera ', 'fa fa-font ', 'fa fa-bold ', 'fa fa-italic ', 'fa fa-text-height ', 'fa fa-text-width ', 'fa fa-align-left ', 'fa fa-align-center ', 'fa fa-align-right ', 'fa fa-align-justify ', 'fa fa-list ', 'fa fa-dedent', 'fa fa-indent ', 'fa fa-video-camera ', 'fa fa-photo', 'fa fa-picture-o ', 'fa fa-pencil ', 'fa fa-map-marker ', 'fa fa-adjust ', 'fa fa-tint ', 'fa fa-edit', 'fa fa-share-square-o ', 'fa fa-check-square-o ', 'fa fa-arrows ', 'fa fa-step-backward ', 'fa fa-fast-backward ', 'fa fa-backward ', 'fa fa-play ', 'fa fa-pause ', 'fa fa-stop ', 'fa fa-forward ', 'fa fa-fast-forward ', 'fa fa-step-forward ', 'fa fa-eject ', 'fa fa-chevron-left ', 'fa fa-chevron-right ', 'fa fa-plus-circle ', 'fa fa-minus-circle ', 'fa fa-times-circle ', 'fa fa-check-circle ', 'fa fa-question-circle ', 'fa fa-info-circle ', 'fa fa-crosshairs ', 'fa fa-times-circle-o ', 'fa fa-check-circle-o ', 'fa fa-ban ', 'fa fa-arrow-left ', 'fa fa-arrow-right ', 'fa fa-arrow-up ', 'fa fa-arrow-down ', 'fa fa-mail-forward', 'fa fa-expand ', 'fa fa-compress ', 'fa fa-plus ', 'fa fa-minus ', 'fa fa-asterisk ', 'fa fa-exclamation-circle ', 'fa fa-gift ', 'fa fa-leaf ', 'fa fa-fire ', 'fa fa-eye ', 'fa fa-eye-slash ', 'fa fa-warning', 'fa fa-plane ', 'fa fa-calendar ', 'fa fa-random ', 'fa fa-comment ', 'fa fa-magnet ', 'fa fa-chevron-up ', 'fa fa-chevron-down ', 'fa fa-retweet ', 'fa fa-shopping-cart ', 'fa fa-folder ', 'fa fa-folder-open ', 'fa fa-arrows-v ', 'fa fa-arrows-h ', 'fa fa-bar-chart-o', 'fa fa-twitter-square ', 'fa fa-facebook-square ', 'fa fa-camera-retro ', 'fa fa-key ', 'fa fa-gears', 'fa fa-comments ', 'fa fa-thumbs-o-up ', 'fa fa-thumbs-o-down ', 'fa fa-star-half ', 'fa fa-heart-o ', 'fa fa-sign-out ', 'fa fa-linkedin-square ', 'fa fa-thumb-tack ', 'fa fa-external-link ', 'fa fa-sign-in ', 'fa fa-trophy ', 'fa fa-github-square ', 'fa fa-upload ', 'fa fa-lemon-o ', 'fa fa-phone ', 'fa fa-square-o ', 'fa fa-bookmark-o ', 'fa fa-phone-square ', 'fa fa-twitter ', 'fa fa-facebook-f', 'fa fa-github ', 'fa fa-unlock ', 'fa fa-credit-card ', 'fa fa-feed', 'fa fa-hdd-o ', 'fa fa-bullhorn ', 'fa fa-bell ', 'fa fa-certificate ', 'fa fa-hand-o-right ', 'fa fa-hand-o-left ', 'fa fa-hand-o-up ', 'fa fa-hand-o-down ', 'fa fa-arrow-circle-left ', 'fa fa-arrow-circle-right ', 'fa fa-arrow-circle-up ', 'fa fa-arrow-circle-down ', 'fa fa-globe ', 'fa fa-wrench ', 'fa fa-tasks ', 'fa fa-filter ', 'fa fa-briefcase ', 'fa fa-arrows-alt ', 'fa fa-group', 'fa fa-chain', 'fa fa-cloud ', 'fa fa-flask ', 'fa fa-cut', 'fa fa-copy', 'fa fa-paperclip ', 'fa fa-save', 'fa fa-square ', 'fa fa-navicon', 'fa fa-bars ', 'fa fa-list-ul ', 'fa fa-list-ol ', 'fa fa-strikethrough ', 'fa fa-underline ', 'fa fa-table ', 'fa fa-magic ', 'fa fa-truck ', 'fa fa-pinterest ', 'fa fa-pinterest-square ', 'fa fa-google-plus-square ', 'fa fa-google-plus ', 'fa fa-money ', 'fa fa-caret-down ', 'fa fa-caret-up ', 'fa fa-caret-left ', 'fa fa-caret-right ', 'fa fa-columns ', 'fa fa-unsorted', 'fa fa-sort-down', 'fa fa-sort-up', 'fa fa-envelope ', 'fa fa-linkedin ', 'fa fa-rotate-left', 'fa fa-legal', 'fa fa-dashboard', 'fa fa-comment-o ', 'fa fa-comments-o ', 'fa fa-flash', 'fa fa-sitemap ', 'fa fa-umbrella ', 'fa fa-paste', 'fa fa-lightbulb-o ', 'fa fa-exchange ', 'fa fa-cloud-download ', 'fa fa-cloud-upload ', 'fa fa-user-md ', 'fa fa-stethoscope ', 'fa fa-suitcase ', 'fa fa-bell-o ', 'fa fa-coffee ', 'fa fa-cutlery ', 'fa fa-file-text-o ', 'fa fa-building-o ', 'fa fa-hospital-o ', 'fa fa-ambulance ', 'fa fa-medkit ', 'fa fa-fighter-jet ', 'fa fa-beer ', 'fa fa-h-square ', 'fa fa-plus-square ', 'fa fa-angle-double-left ', 'fa fa-angle-double-right ', 'fa fa-angle-double-up ', 'fa fa-angle-double-down ', 'fa fa-angle-left ', 'fa fa-angle-right ', 'fa fa-angle-up ', 'fa fa-angle-down ', 'fa fa-desktop ', 'fa fa-laptop ', 'fa fa-tablet ', 'fa fa-mobile-phone', 'fa fa-circle-o ', 'fa fa-quote-left ', 'fa fa-quote-right ', 'fa fa-spinner ', 'fa fa-circle ', 'fa fa-mail-reply', 'fa fa-github-alt ', 'fa fa-folder-o ', 'fa fa-folder-open-o ', 'fa fa-smile-o ', 'fa fa-frown-o ', 'fa fa-meh-o ', 'fa fa-gamepad ', 'fa fa-keyboard-o ', 'fa fa-flag-o ', 'fa fa-flag-checkered ', 'fa fa-terminal ', 'fa fa-code ', 'fa fa-mail-reply-all', 'fa fa-star-half-empty', 'fa fa-star-half-o ', 'fa fa-location-arrow ', 'fa fa-crop ', 'fa fa-code-fork ', 'fa fa-unlink', 'fa fa-question ', 'fa fa-info ', 'fa fa-exclamation ', 'fa fa-superscript ', 'fa fa-subscript ', 'fa fa-eraser ', 'fa fa-puzzle-piece ', 'fa fa-microphone ', 'fa fa-microphone-slash ', 'fa fa-shield ', 'fa fa-calendar-o ', 'fa fa-fire-extinguisher ', 'fa fa-rocket ', 'fa fa-maxcdn ', 'fa fa-chevron-circle-left ', 'fa fa-chevron-circle-right ', 'fa fa-chevron-circle-up ', 'fa fa-chevron-circle-down ', 'fa fa-html5 ', 'fa fa-css3 ', 'fa fa-anchor ', 'fa fa-unlock-alt ', 'fa fa-bullseye ', 'fa fa-ellipsis-h ', 'fa fa-ellipsis-v ', 'fa fa-rss-square ', 'fa fa-play-circle ', 'fa fa-ticket ', 'fa fa-minus-square ', 'fa fa-minus-square-o ', 'fa fa-level-up ', 'fa fa-level-down ', 'fa fa-check-square ', 'fa fa-pencil-square ', 'fa fa-external-link-square ', 'fa fa-share-square ', 'fa fa-compass ', 'fa fa-toggle-down', 'fa fa-toggle-up', 'fa fa-toggle-right', 'fa fa-euro', 'fa fa-gbp ', 'fa fa-dollar', 'fa fa-rupee', 'fa fa-cny', 'fa fa-yen', 'fa fa-ruble', 'fa fa-rub ', 'fa fa-won', 'fa fa-bitcoin', 'fa fa-file ', 'fa fa-file-text ', 'fa fa-sort-alpha-asc ', 'fa fa-sort-alpha-desc ', 'fa fa-sort-amount-asc ', 'fa fa-sort-amount-desc ', 'fa fa-sort-numeric-asc ', 'fa fa-sort-numeric-desc ', 'fa fa-thumbs-up ', 'fa fa-thumbs-down ', 'fa fa-youtube-square ', 'fa fa-youtube ', 'fa fa-xing ', 'fa fa-xing-square ', 'fa fa-youtube-play ', 'fa fa-dropbox ', 'fa fa-stack-overflow ', 'fa fa-instagram ', 'fa fa-flickr ', 'fa fa-adn ', 'fa fa-bitbucket ', 'fa fa-bitbucket-square ', 'fa fa-tumblr ', 'fa fa-tumblr-square ', 'fa fa-long-arrow-down ', 'fa fa-long-arrow-up ', 'fa fa-long-arrow-left ', 'fa fa-long-arrow-right ', 'fa fa-apple ', 'fa fa-windows ', 'fa fa-android ', 'fa fa-linux ', 'fa fa-dribbble ', 'fa fa-skype ', 'fa fa-foursquare ', 'fa fa-trello ', 'fa fa-female ', 'fa fa-male ', 'fa fa-gittip', 'fa fa-sun-o ', 'fa fa-moon-o ', 'fa fa-archive ', 'fa fa-bug ', 'fa fa-vk ', 'fa fa-weibo ', 'fa fa-renren ', 'fa fa-pagelines ', 'fa fa-stack-exchange ', 'fa fa-arrow-circle-o-right ', 'fa fa-arrow-circle-o-left ', 'fa fa-toggle-left', 'fa fa-dot-circle-o ', 'fa fa-wheelchair ', 'fa fa-vimeo-square ', 'fa fa-turkish-lira', 'fa fa-plus-square-o ', 'fa fa-space-shuttle ', 'fa fa-slack ', 'fa fa-envelope-square ', 'fa fa-wordpress ', 'fa fa-openid ', 'fa fa-institution', 'fa fa-university ', 'fa fa-mortar-board', 'fa fa-yahoo ', 'fa fa-google ', 'fa fa-reddit ', 'fa fa-reddit-square ', 'fa fa-stumbleupon-circle ', 'fa fa-stumbleupon ', 'fa fa-delicious ', 'fa fa-digg ', 'fa fa-pied-piper ', 'fa fa-pied-piper-alt ', 'fa fa-drupal ', 'fa fa-joomla ', 'fa fa-language ', 'fa fa-fax ', 'fa fa-building ', 'fa fa-child ', 'fa fa-paw ', 'fa fa-spoon ', 'fa fa-cube ', 'fa fa-cubes ', 'fa fa-behance ', 'fa fa-behance-square ', 'fa fa-steam ', 'fa fa-steam-square ', 'fa fa-recycle ', 'fa fa-automobile', 'fa fa-cab', 'fa fa-tree ', 'fa fa-spotify ', 'fa fa-deviantart ', 'fa fa-soundcloud ', 'fa fa-database ', 'fa fa-file-pdf-o ', 'fa fa-file-word-o ', 'fa fa-file-excel-o ', 'fa fa-file-powerpoint-o ', 'fa fa-file-photo-o', 'fa fa-file-image-o ', 'fa fa-file-zip-o', 'fa fa-file-sound-o', 'fa fa-file-movie-o', 'fa fa-file-code-o ', 'fa fa-vine ', 'fa fa-codepen ', 'fa fa-jsfiddle ', 'fa fa-life-bouy', 'fa fa-life-saver', 'fa fa-life-ring ', 'fa fa-circle-o-notch ', 'fa fa-ra', 'fa fa-ge', 'fa fa-git-square ', 'fa fa-git ', 'fa fa-y-combinator-square', 'fa fa-hacker-news ', 'fa fa-tencent-weibo ', 'fa fa-qq ', 'fa fa-wechat', 'fa fa-send', 'fa fa-send-o', 'fa fa-history ', 'fa fa-circle-thin ', 'fa fa-header ', 'fa fa-paragraph ', 'fa fa-sliders ', 'fa fa-share-alt ', 'fa fa-share-alt-square ', 'fa fa-bomb ', 'fa fa-soccer-ball-o', 'fa fa-tty ', 'fa fa-binoculars ', 'fa fa-plug ', 'fa fa-slideshare ', 'fa fa-twitch ', 'fa fa-yelp ', 'fa fa-newspaper-o ', 'fa fa-wifi ', 'fa fa-calculator ', 'fa fa-paypal ', 'fa fa-google-wallet ', 'fa fa-cc-visa ', 'fa fa-cc-mastercard ', 'fa fa-cc-discover ', 'fa fa-cc-amex ', 'fa fa-cc-paypal ', 'fa fa-cc-stripe ', 'fa fa-bell-slash ', 'fa fa-bell-slash-o ', 'fa fa-trash ', 'fa fa-copyright ', 'fa fa-at ', 'fa fa-eyedropper ', 'fa fa-paint-brush ', 'fa fa-birthday-cake ', 'fa fa-area-chart ', 'fa fa-pie-chart ', 'fa fa-line-chart ', 'fa fa-lastfm ', 'fa fa-lastfm-square ', 'fa fa-toggle-off ', 'fa fa-toggle-on ', 'fa fa-bicycle ', 'fa fa-bus ', 'fa fa-ioxhost ', 'fa fa-angellist ', 'fa fa-cc ', 'fa fa-shekel', 'fa fa-ils ', 'fa fa-meanpath ', 'fa fa-buysellads ', 'fa fa-connectdevelop ', 'fa fa-dashcube ', 'fa fa-forumbee ', 'fa fa-leanpub ', 'fa fa-sellsy ', 'fa fa-shirtsinbulk ', 'fa fa-simplybuilt ', 'fa fa-skyatlas ', 'fa fa-cart-plus ', 'fa fa-cart-arrow-down ', 'fa fa-diamond ', 'fa fa-ship ', 'fa fa-user-secret ', 'fa fa-motorcycle ', 'fa fa-street-view ', 'fa fa-heartbeat ', 'fa fa-venus ', 'fa fa-mars ', 'fa fa-mercury ', 'fa fa-intersex', 'fa fa-transgender-alt ', 'fa fa-venus-double ', 'fa fa-mars-double ', 'fa fa-venus-mars ', 'fa fa-mars-stroke ', 'fa fa-mars-stroke-v ', 'fa fa-mars-stroke-h ', 'fa fa-neuter ', 'fa fa-genderless ', 'fa fa-facebook-official ', 'fa fa-pinterest-p ', 'fa fa-whatsapp ', 'fa fa-server ', 'fa fa-user-plus ', 'fa fa-user-times ', 'fa fa-hotel', 'fa fa-viacoin ', 'fa fa-train ', 'fa fa-subway ', 'fa fa-medium ', 'fa fa-yc', 'fa fa-optin-monster ', 'fa fa-opencart ', 'fa fa-expeditedssl ', 'fa fa-battery-4', 'fa fa-battery-3', 'fa fa-battery-2', 'fa fa-battery-1', 'fa fa-battery-0', 'fa fa-mouse-pointer ', 'fa fa-i-cursor ', 'fa fa-object-group ', 'fa fa-object-ungroup ', 'fa fa-sticky-note ', 'fa fa-sticky-note-o ', 'fa fa-cc-jcb ', 'fa fa-cc-diners-club ', 'fa fa-clone ', 'fa fa-balance-scale ', 'fa fa-hourglass-o ', 'fa fa-hourglass-1', 'fa fa-hourglass-2', 'fa fa-hourglass-3', 'fa fa-hourglass ', 'fa fa-hand-grab-o', 'fa fa-hand-stop-o', 'fa fa-hand-scissors-o ', 'fa fa-hand-lizard-o ', 'fa fa-hand-spock-o ', 'fa fa-hand-pointer-o ', 'fa fa-hand-peace-o ', 'fa fa-trademark ', 'fa fa-registered ', 'fa fa-creative-commons ', 'fa fa-gg ', 'fa fa-gg-circle ', 'fa fa-tripadvisor ', 'fa fa-odnoklassniki ', 'fa fa-odnoklassniki-square ', 'fa fa-get-pocket ', 'fa fa-wikipedia-w ', 'fa fa-safari ', 'fa fa-chrome ', 'fa fa-firefox ', 'fa fa-opera ', 'fa fa-internet-explorer ', 'fa fa-tv', 'fa fa-contao ', 'fa fa-500px ', 'fa fa-amazon ', 'fa fa-calendar-plus-o ', 'fa fa-calendar-minus-o ', 'fa fa-calendar-times-o ', 'fa fa-calendar-check-o ', 'fa fa-industry ', 'fa fa-map-pin ', 'fa fa-map-signs ', 'fa fa-map-o ', 'fa fa-map ', 'fa fa-commenting ', 'fa fa-commenting-o ', 'fa fa-houzz ', 'fa fa-vimeo ', 'fa fa-black-tie ', 'fa fa-fonticons ', 'fa fa-reddit-alien ', 'fa fa-edge ', 'fa fa-credit-card-alt ', 'fa fa-codiepie ', 'fa fa-modx ', 'fa fa-fort-awesome ', 'fa fa-usb ', 'fa fa-product-hunt ', 'fa fa-mixcloud ', 'fa fa-scribd ', 'fa fa-pause-circle ', 'fa fa-pause-circle-o ', 'fa fa-stop-circle ', 'fa fa-stop-circle-o ', 'fa fa-shopping-bag ', 'fa fa-shopping-basket ', 'fa fa-hashtag ', 'fa fa-bluetooth ', 'fa fa-bluetooth-b ', 'fa fa-percent',
        // PE Line Icons
        'pe pe-7s-album', 'pe pe-7s-arc', 'pe pe-7s-back-2', 'pe pe-7s-bandaid', 'pe pe-7s-car', 'pe pe-7s-diamond', 'pe pe-7s-door-lock', 'pe pe-7s-eyedropper', 'pe pe-7s-female', 'pe pe-7s-gym', 'pe pe-7s-hammer', 'pe pe-7s-headphones', 'pe pe-7s-helm', 'pe pe-7s-hourglass', 'pe pe-7s-leaf', 'pe pe-7s-magic-wand', 'pe pe-7s-male', 'pe pe-7s-map-2', 'pe pe-7s-next-2', 'pe pe-7s-paint-bucket', 'pe pe-7s-pendrive', 'pe pe-7s-photo', 'pe pe-7s-piggy', 'pe pe-7s-plugin', 'pe pe-7s-refresh-2', 'pe pe-7s-rocket', 'pe pe-7s-settings', 'pe pe-7s-shield', 'pe pe-7s-smile', 'pe pe-7s-usb', 'pe pe-7s-vector', 'pe pe-7s-wine', 'pe pe-7s-cloud-upload', 'pe pe-7s-cash', 'pe pe-7s-close', 'pe pe-7s-bluetooth', 'pe pe-7s-cloud-download', 'pe pe-7s-way', 'pe pe-7s-close-circle', 'pe pe-7s-id', 'pe pe-7s-angle-up', 'pe pe-7s-wristwatch', 'pe pe-7s-angle-up-circle', 'pe pe-7s-world', 'pe pe-7s-angle-right', 'pe pe-7s-volume', 'pe pe-7s-angle-right-circle', 'pe pe-7s-users', 'pe pe-7s-angle-left', 'pe pe-7s-user-female', 'pe pe-7s-angle-left-circle', 'pe pe-7s-up-arrow', 'pe pe-7s-angle-down', 'pe pe-7s-switch', 'pe pe-7s-angle-down-circle', 'pe pe-7s-scissors', 'pe pe-7s-wallet', 'pe pe-7s-safe', 'pe pe-7s-volume2', 'pe pe-7s-volume1', 'pe pe-7s-voicemail', 'pe pe-7s-video', 'pe pe-7s-user', 'pe pe-7s-upload', 'pe pe-7s-unlock', 'pe pe-7s-umbrella', 'pe pe-7s-trash', 'pe pe-7s-tools', 'pe pe-7s-timer', 'pe pe-7s-ticket', 'pe pe-7s-target', 'pe pe-7s-sun', 'pe pe-7s-study', 'pe pe-7s-stopwatch', 'pe pe-7s-star', 'pe pe-7s-speaker', 'pe pe-7s-signal', 'pe pe-7s-shuffle', 'pe pe-7s-shopbag', 'pe pe-7s-share', 'pe pe-7s-server', 'pe pe-7s-search', 'pe pe-7s-film', 'pe pe-7s-science', 'pe pe-7s-disk', 'pe pe-7s-ribbon', 'pe pe-7s-repeat', 'pe pe-7s-refresh', 'pe pe-7s-add-user', 'pe pe-7s-refresh-cloud', 'pe pe-7s-paperclip', 'pe pe-7s-radio', 'pe pe-7s-note2', 'pe pe-7s-print', 'pe pe-7s-network', 'pe pe-7s-prev', 'pe pe-7s-mute', 'pe pe-7s-power', 'pe pe-7s-medal', 'pe pe-7s-portfolio', 'pe pe-7s-like2', 'pe pe-7s-plus', 'pe pe-7s-left-arrow', 'pe pe-7s-play', 'pe pe-7s-key', 'pe pe-7s-plane', 'pe pe-7s-joy', 'pe pe-7s-photo-gallery', 'pe pe-7s-pin', 'pe pe-7s-phone', 'pe pe-7s-plug', 'pe pe-7s-pen', 'pe pe-7s-right-arrow', 'pe pe-7s-paper-plane', 'pe pe-7s-delete-user', 'pe pe-7s-paint', 'pe pe-7s-bottom-arrow', 'pe pe-7s-notebook', 'pe pe-7s-note', 'pe pe-7s-next', 'pe pe-7s-news-paper', 'pe pe-7s-musiclist', 'pe pe-7s-music', 'pe pe-7s-mouse', 'pe pe-7s-more', 'pe pe-7s-moon', 'pe pe-7s-monitor', 'pe pe-7s-micro', 'pe pe-7s-menu', 'pe pe-7s-map', 'pe pe-7s-map-marker', 'pe pe-7s-mail', 'pe pe-7s-mail-open', 'pe pe-7s-mail-open-file', 'pe pe-7s-magnet', 'pe pe-7s-loop', 'pe pe-7s-look', 'pe pe-7s-lock', 'pe pe-7s-lintern', 'pe pe-7s-link', 'pe pe-7s-like', 'pe pe-7s-light', 'pe pe-7s-less', 'pe pe-7s-keypad', 'pe pe-7s-junk', 'pe pe-7s-info', 'pe pe-7s-home', 'pe pe-7s-help2', 'pe pe-7s-help1', 'pe pe-7s-graph3', 'pe pe-7s-graph2', 'pe pe-7s-graph1', 'pe pe-7s-graph', 'pe pe-7s-global', 'pe pe-7s-gleam', 'pe pe-7s-glasses', 'pe pe-7s-gift', 'pe pe-7s-folder', 'pe pe-7s-flag', 'pe pe-7s-filter', 'pe pe-7s-file', 'pe pe-7s-expand1', 'pe pe-7s-exapnd2', 'pe pe-7s-edit', 'pe pe-7s-drop', 'pe pe-7s-drawer', 'pe pe-7s-download', 'pe pe-7s-display2', 'pe pe-7s-display1', 'pe pe-7s-diskette', 'pe pe-7s-date', 'pe pe-7s-cup', 'pe pe-7s-culture', 'pe pe-7s-crop', 'pe pe-7s-credit', 'pe pe-7s-copy-file', 'pe pe-7s-config', 'pe pe-7s-compass', 'pe pe-7s-comment', 'pe pe-7s-coffee', 'pe pe-7s-cloud', 'pe pe-7s-clock', 'pe pe-7s-check', 'pe pe-7s-chat', 'pe pe-7s-cart', 'pe pe-7s-camera', 'pe pe-7s-call', 'pe pe-7s-calculator', 'pe pe-7s-browser', 'pe pe-7s-box2', 'pe pe-7s-box1', 'pe pe-7s-bookmarks', 'pe pe-7s-bicycle', 'pe pe-7s-bell', 'pe pe-7s-battery', 'pe pe-7s-ball', 'pe pe-7s-back', 'pe pe-7s-attention', 'pe pe-7s-anchor', 'pe pe-7s-albums', 'pe pe-7s-alarm', 'pe pe-7s-airplay',

      ].concat(icons),
      get_value: function() {
        return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      },
      render: function(value) {
        this.dom_element = $('<div class="' + p + 'form-group"><label>'
          + this.heading + '</label><div><input class="'
          + p + 'form-control" name="'
          + this.param_name + '" type="text" value="'
          + value + '"></div><p class="'
          + p + 'help-block">'
          + this.description + '</p></div>');
      },
      opened: function() {
        carbide_add_css('vendor/font-awesome/css/font-awesome.css', function() {});
        carbide_add_css('vendor/et-line-font/et-line-font.css', function() {});
        carbide_add_css('vendor/pe-icon-7-stroke/css/pe-icon-7-stroke.css', function() {});
        var icons = $('<div class="icons"></div>').appendTo(this.dom_element);
        for (var i = 0; i < this.icons.length; i++) {
          $(icons).append('<span class="' + this.icons[i] + '"></span>');
        }
        var param = this;
        $(icons).selectable({
          stop: function() {
            var icon = '';
            var c = $(param.dom_element).find('.ui-selected').attr('class');
            if (c)
              icon = c.replace('ui-selectee', '').replace('ui-selected', '');
            $(param.dom_element).find('input[name="' + param.param_name + '"]').val($.trim(icon));
          }
        });
        if (this.get_value() != '')
          $(icons).find('.' + $.trim(this.get_value()).replace(/ /g, '.')).addClass("ui-selected");
      },
    },
    {
      type: 'link',
      get_value: function() {
        return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      },
      render: function(value) {
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><input class="' + p + 'form-control" name="' + this.param_name + '" type="text" value="' + value + '"></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
    },
    {
      type: 'links',
      get_value: function() {
        return $(this.dom_element).find('#' + this.id).val();
      },
      render: function(value) {
        this.id = _.uniqueId();
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><textarea id="' + this.id + '" class="' + p + 'form-control" rows="10" cols="45" name="' + this.param_name + '" ">' + value + '</textarea></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
    },
    {
      type: 'integer_slider',
      create: function() {
        this.min = 0;
        this.max = 100;
        this.step = 1;
      },
      get_value: function() {
        var v = $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
        return (v == '') ? NaN : parseFloat(v).toString();
      },
      render: function(value) {
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><input class="' + p + 'form-control" name="' + this.param_name + '" type="text" value="' + value + '"></div><div class="slider"></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
      opened: function() {
        nouislider($(this.dom_element).find('.slider'), this.min, this.max, this.get_value(), this.step, $(this.dom_element).find('input[name="' + this.param_name + '"]'));
      },
    },
    {
      type: 'datetime',
      create: function() {
        this.formatDate = '';
        this.formatTime = '';
        this.timepicker = false;
        this.datepicker = false;
      },
      get_value: function() {
        return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      },
      render: function(value) {
        this.dom_element = $('<div class="' + p + 'form-group"><label>' + this.heading + '</label><div><input class="' + p + 'form-control" name="' + this.param_name + '" type="text" value="' + value + '"></div><p class="' + p + 'help-block">' + this.description + '</p></div>');
      },
      opened: function() {
        var param = this;
        carbide_add_css('vendor/datetimepicker/jquery.datetimepicker.css', function() {});
        carbide_add_js({
          path: 'vendor/datetimepicker/jquery.datetimepicker.js',
          callback: function() {
            if (param.datepicker && param.timepicker)
              param.format = param.formatDate + ' ' + param.formatTime;
            if (param.datepicker && !param.timepicker)
              param.format = param.formatDate;
            if (!param.datepicker && param.timepicker)
              param.format = param.formatTime;
            $(param.dom_element).find('input[name="' + param.param_name + '"]').datetimepicker({
              format: param.format,
              timepicker: param.timepicker,
              datepicker: param.datepicker,
              inline: true,
              lang: lang()
            });
          }});
      },
    },
    {
      type: 'saved_datetime',
      get_value: function() {
        return (new Date).toUTCString();
      },
    },
  ];

  if ('carbide_param_types' in window) {
    window.carbide_param_types = window.carbide_param_types.concat(carbide_param_types);
  } else {
    window.carbide_param_types = carbide_param_types;
  }

})(window.jQuery);
