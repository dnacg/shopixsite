(function($) {
  var p = '';
  var fp = '';
  if ('carbide_prefix' in window) {
    p = window.carbide_prefix;
    fp = window.carbide_prefix.replace('-', '_');
  }

  function t(text) {
    if ('carbide_t' in window) {
      return window.carbide_t(text);
    } else {
      return text;
    }
  }

  var target_options = {
    '_self': t('Same window'),
    '_blank': t('New window'),
  };

 var colors = [];
 colors['brand'] = t('Brand color');
 colors[''] = t('Custom');
 colors['inherit'] = t('Inherit');
  // Helped function for add button styles.
  function getButtonsStyle() {
    var keys = [p + 'btn-default', p + 'btn-primary', p + 'btn-success', p + 'btn-info', p + 'btn-warning', p + 'btn-danger'];
    var value = [p + 'btn-default', p + 'btn-primary', p + 'btn-success', p + 'btn-info', p + 'btn-warning', p + 'btn-danger'];
    for (var name in window.button_styles) {
      value.push(p + name);
      keys.push(window.button_styles[p + name]);
    }
    return _.object(keys, value);
  }
  var carbide_elements = [
    {
      base: 'az_text',
      name: t('Text'),
      icon: 'et et-icon-document',
      // description: t('Text with editor'),
      params: [
        {
          type: 'textarea',
          heading: t('Text'),
          param_name: 'content',
          value: '<h2>Lorem ipsum dolor sit amet.</h2> Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
      ],
      show_settings_on_create: true,
      is_container: true,
      has_content: true,
      render: function($, p, fp) {

        this.dom_element = $('<div class="az-element az-text ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '">' + this.attrs['content'] + '</div>');
        this.dom_content_element = this.dom_element;
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_link',
      name: t('Link'),
      icon: 'et et-icon-attachment fa-flip-horizontal',
      // description: t('Link wrapper'),
      params: [
        {
          type: 'link',
          heading: t('Link'),
          param_name: 'link',
          description: t('Content link (url).'),
        },
        {
          type: 'dropdown',
          heading: t('Link target'),
          param_name: 'link_target',
          description: t('Select where to open link.'),
          value: target_options,
          dependency: {'element': 'link', 'not_empty': {}},
        },
      ],
      is_container: true,
      show_settings_on_create: true,
      show_controls: function() {
        if (window.carbide_editor) {
          this.baseclass.prototype.show_controls.apply(this, arguments);
          $(this.dom_content_element).click(function(){return false});
        }
      },
      render: function($, p, fp) {
        this.dom_element = $('<div class="az-element az-link ' + this.attrs['el_class'] + '"></div>');
        this.dom_content_element = $('<a href="' + this.attrs['link'] + '" class="az-ctnr" target="' + this.attrs['link_target'] + '"></a>').appendTo(this.dom_element);
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_icon',
      name: t('Icon'),
      icon: 'et et-icon-strategy',
      // description: t('Vector icon'),
      params: [
        {
          type: 'icon',
          heading: t('Icon'),
          param_name: 'icon',
        },
        {
            type: 'dropdown',
            heading: t('Icon Size'),
            param_name: 'size',
            value: _.object(['fa-lg', '', 'fa-2x', 'fa-3x', 'fa-4x', 'fa-5x'], [t('Normal'), t('Large'), t('2x'), t('3x'), t('4x'), t('5x')]),
        },
        {
            type: 'dropdown',
            heading: t('Style'),
            param_name: 'st_style',
            value: {
                '': t('None'),
                'stbe-util-icon-rounded': t('Rounded'),
                'stbe-util-icon-circle': t('Circle'),
                'stbe-util-icon-square': t('Square'),
            },
            tab: t('Icon Utilities')
        },
        {
            type: 'dropdown',
            heading: t('Icon Animation'),
            param_name: 'animation',
            value: _.object(['', 'fa-spin', 'fa-pulse'], [t('No'), t('Spin'), t('Pulse')]),
            tab: t('Icon Utilities')
        },
        {
            type: 'dropdown',
            heading: t('Icon Orientation'),
            param_name: 'orientation',
            value: _.object(['', 'fa-rotate-90', 'fa-rotate-180', 'fa-rotate-270', 'fa-flip-horizontal', 'fa-flip-vertical'], [t('Normal'), t('Rotate 90'), t('Rotate 180'), t('Rotate 270'), t('Flip Horizontal'), t('Flip Vertical')]),
            tab: t('Icon Utilities')
        },
        {
          type: 'link',
          heading: t('Link'),
          param_name: 'link',
          description: t('Icon link (url).'),
        },
        {
          type: 'dropdown',
          heading: t('Link target'),
          param_name: 'link_target',
          description: t('Select where to open link.'),
          value: target_options,
          dependency: {'element': 'link', 'not_empty': {}},
        },
      ],
      show_settings_on_create: true,
      showed: function($, p, fp) {
        var icon_set = this.attrs['icon'].charAt(0);
        switch (icon_set) {
          case 'e':
            // ET Icons
            this.add_css('vendor/et-line-font/et-line-font.css', 'ETLineFont' in $.fn, function() {});
            this.add_css('css/icon-helpers.css', 'IconHelpers' in $.fn, function() {});
            break;
          case 'f':
            // Font Awesome Icons
            this.add_css('vendor/font-awesome/css/font-awesome.min.css', 'fontAwesome' in $.fn, function() {});
            break;
          case 'g':
            // Glyphicons
            this.add_css('css/icon-helpers.css', 'IconHelpers' in $.fn, function() {});
            break;
          case 'p':
            // Pixeden Icons
            this.add_css('vendor/pe-icon-7-stroke/css/pe-icon-7-stroke', 'PELineFont' in $.fn, function() {});
            this.add_css('css/icon-helpers.css', 'IconHelpers' in $.fn, function() {});
            break;
          default:
            break;
        }
        this.baseclass.prototype.showed.apply(this, arguments);
        var element = this;
      },
      render: function($, p, fp) {
        var icon_style = '';
        // Foreground color
        if (this.attrs['st_theme_color'] == '') {
            icon_style = icon_style + 'color: ' + this.attrs['st_color'] + ';';
        } else {
            if('sooperthemes_theme_palette' in window && window.sooperthemes_theme_palette != null && this.attrs['st_theme_color'] in window.sooperthemes_theme_palette)
                icon_style = icon_style + 'color: ' + window.sooperthemes_theme_palette[this.attrs['st_theme_color']] + ';';
            else
                icon_style = icon_style + 'color: ' + this.attrs['st_theme_color'] + ';';
        }
        // Background color
        if (this.attrs['st_theme_bgcolor'] == '') {
            icon_style = icon_style + 'background-color: ' + this.attrs['st_bgcolor'] + ';';
        } else {
            if('sooperthemes_theme_palette' in window && window.sooperthemes_theme_palette != null && this.attrs['st_theme_bgcolor'] in window.sooperthemes_theme_palette)
                icon_style = icon_style + 'background-color: ' + window.sooperthemes_theme_palette[this.attrs['st_theme_bgcolor']] + ';';
            else
                icon_style = icon_style + 'background-color: ' + this.attrs['st_theme_bgcolor'] + ';';
        }
        var icon_html =  '<div class="az-element az-icon ' + this.attrs['el_class'] + '"><i class="' + this.attrs['icon'] + ' ' + this.attrs['size'] + ' ' + this.attrs['st_style'] + ' ' + this.attrs['fw'] + ' ' + this.attrs['pull'] + ' ' + this.attrs['animation'] + ' ' + this.attrs['orientation'] + '" style="' + this.attrs['style'] + icon_style + '"></i></div>';
        if (this.attrs['link'] == '') {
            this.dom_element = $(icon_html);
        } else {
            this.dom_element = $('<a href="' + this.attrs['link'] + '" class="az-element az-icon ' + this.attrs['el_class'] + '" target="' + this.attrs['link_target'] + '">' + icon_html + '</a>');
        }
        $(this.dom_element).css('font-size', this.attrs['size'] + 'px');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_image',
      name: t('Image'),
      icon: 'et et-icon-picture',
      // description: t('Single image'),
      params: [
        {
          type: 'image',
          heading: t('Image'),
          param_name: 'image',
          description: t('Select image from media library.'),
        },
        {
          type: 'textfield',
          heading: t('Image width'),
          param_name: 'width',
          description: t('You can use px, em, %, etc. or enter just a number and it will use pixels.'),
          can_be_empty: true,
          value: '100%',
        },
        {
          type: 'textfield',
          heading: t('Image height'),
          description: t('You can use px, em, %, etc. or enter just a number and it will use pixels.'),
          can_be_empty: true,
          param_name: 'height',
        },
        {
          type: 'link',
          heading: t('Image link'),
          param_name: 'link',
          description: t('Enter URL if you want this image to have a link.'),
        },
        {
          type: 'dropdown',
          heading: t('Image link target'),
          param_name: 'link_target',
          description: t('Select where to open link.'),
          value: target_options,
          dependency: {'element': 'link', 'not_empty': {}},
        },
      ],
      frontend_render: true,
      show_settings_on_create: true,
      showed: function($, p, fp) {
        this.baseclass.prototype.showed.apply(this, arguments);
        var element = this;
      },
      render: function ($, p, fp) {

        var id = this.id;
        var element = this;
        this.dom_element = $('<div class="az-element az-image ' + this.attrs['el_class'] + '"></div>');
        function render_image(value, style, width, height) {
        if ($.isNumeric(width))
          width = width + 'px';
        if ($.isNumeric(height))
          height = height + 'px';
        var img = $('<img src="' + value + '" alt="">');
        $(img).attr('style', style);
        if (width.length > 0)
          $(img).css('width', width);
        if (height.length > 0)
          $(img).css('height', height);
        return img;
        }

        var img = render_image(this.attrs['image'], this.attrs['style'], this.attrs['width'], this.attrs['height']);
        $(img).appendTo(this.dom_element);
        if (this.attrs['link'] != '') {
          $(this.dom_element).find('img').each(function () {
          $(this).wrap('<a href="' + element.attrs['link'] + '" target="' + element.attrs['link_target'] + '"></a>');
          });
        }
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_button',
      name: t('Button'),
      icon: 'pe pe-7s-mouse',
      // description: t('Button'),
      params: [
        {
          type: 'textfield',
          heading: t('Title'),
          param_name: 'title',
          description: t('Text on the button'),
        },
        {
          type: 'link',
          heading: t('Link'),
          param_name: 'link',
          description: t('Button link (url).'),
        },
        {
          type: 'dropdown',
          heading: t('Link target'),
          param_name: 'link_target',
          description: t('Select where to open link.'),
          value: target_options,
          dependency: {'element': 'link', 'not_empty': {}},
        },
        {
          type: 'dropdown',
          heading: t('Type'),
          param_name: 'type',
          value: getButtonsStyle()
        },
        {
          type: 'dropdown',
          heading: t('Size'),
          param_name: 'size',
          value: _.object(['', p + 'btn-lg', p + 'btn-sm', p + 'btn-xs'], [t('Normal'), t('Large'), t('Small'), t('Extra small')]),
        },
      ],
      show_settings_on_create: true,
      style_selector: '> .' + p + 'btn',
      render: function($, p, fp) {


        if (this.attrs['link'] == '') {
          this.dom_element = $('<div class="az-element az-button ' + this.attrs['el_class'] + '"><button type="button" class="' + p + 'btn ' + this.attrs['type'] + ' ' + this.attrs['size'] + '" style="' + this.attrs['style'] + '">' + this.attrs['title'] + '</button></div>');
        } else {
          this.dom_element = $('<div class="az-element az-button ' + this.attrs['el_class'] + '"><a href="' + this.attrs['link'] + '" type="button" class="' + p + 'btn ' + this.attrs['type'] + ' ' + this.attrs['size'] + '" style="' + this.attrs['style'] + '" target="' + this.attrs['link_target'] + '">' + this.attrs['title'] + '</a></div>');
        }
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_jumbotron',
      name: t('Jumbotron'),
      icon: 'et et-icon-megaphone',
      // description: t('Big Box'),
      params: [
      ],
      is_container: true,
      get_empty: function() {
        return '<div class="az-empty"><div class="top-left ' + p + 'well"><h1>↖</h1>' + t('Settings for this jumbotron.') + '</div></div>';
      },
      render: function($, p, fp) {


        this.dom_element = $('<div class="az-element az-ctnr az-jumbotron ' + p + 'jumbotron ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"></div>');
        this.dom_content_element = this.dom_element;
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
    base: 'az_well',
    name: t('Well'),
    icon: 'et et-icon-focus',
    // description: t('Content box'),
    params: [
      {
      type: 'dropdown',
      heading: t('Type'),
      param_name: 'type',
      value: _.object(
        [p, p + 'well-lg', p + 'well-sm'],
        [t('Default'), t('Large'), t('Small')]
      )
      }
    ],
    is_container: true,
    show_settings_on_create: true,
    get_empty: function () {
      return '<div class="az-empty"><div class="top-left ' + p + 'well"><h1>↖</h1>' + t('Settings for this well: ') + '<span class="' + p + 'glyphicon ' + p + 'glyphicon-pencil"></span></div></div>';
    },
    render: function ($, p, fp) {
      this.dom_element = $('<div class="az-element az-well ' + p + 'well ' + this.attrs['el_class'] + ' ' + this.attrs['type'] + '" style="' + this.attrs['style'] + '"></div>');
      var body = $('<div class="' + p + 'well az-ctnr"></div>').appendTo(this.dom_element);
      this.dom_content_element = body;
      this.baseclass.prototype.render.apply(this, arguments);
    }
    },
    {
      base: 'az_map',
      name: t('Map'),
      icon: 'et et-icon-map',
      params: [
        {
          type: 'textfield',
          heading: t('Address'),
          param_name: 'address',
          description: t('1865 Winchester Blvd #202 Campbell, CA 95008'),
          can_be_empty: false
        },
        {
          type: 'textfield',
          heading: t('Map width'),
          param_name: 'width',
          description: t('You can use px, em, %, etc. or enter just a number and it will use pixels.'),
          can_be_empty: true,
          value: '100%'
        },
        {
          type: 'textfield',
          heading: t('Map Height'),
          description: t('You can use px, em, %, etc. or enter just a number and it will use pixels.'),
          can_be_empty: true,
          param_name: 'height',
          value: '400px'
        }
      ],
      show_settings_on_create: true,
      is_container: true,
      has_content: true,
      render: function($, p, fp) {

        this.dom_element = $('<div class="az-element az-map ' + this.attrs['el_class'] + '"></div>');
        function render_map(address, style, width, height) {
          address = address.replace(' ', '+');
          if ($.isNumeric(width))
            width = width + 'px';
          if ($.isNumeric(height))
            height = height + 'px';
          var map = $('<iframe src="https://maps.google.com/maps?q=' + address + '&iwloc=near&output=embed" frameborder="0"></<iframe>');
          $(map).attr('style', style);
          if (width.length > 0)
            $(map).css('width', width);
          if (height.length > 0)
            $(map).css('height', height);
          return map;
        }

        var map = render_map(this.attrs['address'], this.attrs['style'], this.attrs['width'], this.attrs['height']);
        $(map).appendTo(this.dom_element);
        this.baseclass.prototype.render.apply(this, arguments);
      }
    },
    {
      base: 'az_panel',
      name: t('Panel'),
      icon: 'et et-icon-focus',
      // description: t('Content Panel'),
      params: [
        {
          type: 'textfield',
          heading: t('Title'),
          param_name: 'title',
        },
        {
          type: 'dropdown',
          heading: t('Type'),
          param_name: 'type',
          value: _.object([p + 'panel-default', p + 'panel-primary', p + 'panel-success', p + 'panel-info', p + 'panel-warning', p + 'panel-danger'], [t('Default'), t('Primary'), t('Success'), t('Info'), t('Warning'), t('Danger'), ])
        },
      ],
      is_container: true,
      show_settings_on_create: true,
      get_empty: function() {
        return '<div class="az-empty"><div class="top-left ' + p + 'well"><h1>↖</h1>' + t('Settings for this panel: ') + '<span class="' + p + 'glyphicon ' + p + 'glyphicon-pencil"></span></div></div>';
      },
      render: function($, p, fp) {


        this.dom_element = $('<div class="az-element az-panel ' + p + 'panel ' + this.attrs['el_class'] + ' ' + this.attrs['type'] + '" style="' + this.attrs['style'] + '"></div>');
        if (this.attrs['title'] != '') {
          var heading = $('<div class="' + p + 'panel-heading"><h3 class="' + p + 'panel-title">' + this.attrs['title'] + '</div></div>');
          $(this.dom_element).append(heading);
        }
        var body = $('<div class="' + p + 'panel-body az-ctnr"></div>').appendTo(this.dom_element);
        this.dom_content_element = body;
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_alert',
      name: t('Alert'),
      icon: 'et et-icon-caution',
      // description: t('Alert box'),
      params: [
        {
          type: 'textfield',
          heading: t('Message'),
          param_name: 'message',
          value: t('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        },
        {
          type: 'dropdown',
          heading: t('Type'),
          param_name: 'type',
          description: t('Select message type.'),
          value: _.object([p + 'alert-success', p + 'alert-info', p + 'alert-warning', p + 'alert-danger'], [t('Success'), t('Info'), t('Warning'), t('Danger')]),
        },
      ],
      show_settings_on_create: true,
      render: function($, p, fp) {

        this.dom_element = $('<div class="az-element az-alert ' + p + 'alert ' + this.attrs['type'] + ' ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '">' + this.attrs['message'] + '</div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_separator',
      name: t('Separator'),
      icon: 'et et-icon-scissors',
      // description: t('Horizontal separator'),
      params: [
      ],
      render: function($, p, fp) {
        this.dom_element = $('<hr class="az-element az-separator ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"></hr>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_blockquote',
      name: t('Blockquote'),
      icon: 'et et-icon-quote',
      // description: t('Blockquote box'),
      params: [
        {
          type: 'textarea',
          heading: t('Text'),
          param_name: 'content',
          value: t('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
        },
        {
          type: 'textfield',
          heading: t('Cite'),
          param_name: 'cite',
        },
        {
          type: 'checkbox',
          heading: t('Reverse?'),
          param_name: 'reverse',
          value: {
            'blockquote-reverse': t("Yes"),
          },
        },
      ],
      show_settings_on_create: true,
      is_container: true,
      has_content: true,
      render: function($, p, fp) {
        var reverse = this.attrs['reverse'];
        if (reverse != '')
          reverse = p + reverse;
        this.dom_element = $('<blockquote class="az-element az-blockquote ' + this.attrs['el_class'] + ' ' + reverse + '" style="' + this.attrs['style'] + '">' + this.attrs['content'] + '</blockquote>');
        this.dom_content_element = this.dom_element;

        // Condition to check existing item.
        var str = '<footer><cite>' + this.attrs['cite'] + '</cite></footer>';
        var innerHtml = this.dom_element.html().indexOf(str);
        if (this.attrs['cite'] != '' && innerHtml < 0)
          $(this.dom_element).append(str);
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_progress_bar',
      name: t('Progress Bar'),
      icon: 'et et-icon-bargraph fa-rotate-90',
      // description: t('Animated progress bar'),
      params: [
        {
          type: 'textfield',
          heading: t('Label'),
          param_name: 'label',
        },
        {
          type: 'integer_slider',
          heading: t('Width'),
          param_name: 'width',
          value: '50',
        },
        {
          type: 'dropdown',
          heading: t('Type'),
          param_name: 'type',
          value: _.object(['', p + 'progress-bar-success', p + 'progress-bar-info', p + 'progress-bar-warning', p + 'progress-bar-danger'], [t('Default'), t('Success'), t('Info'), t('Warning'), t('Danger')]),
        },
        {
          type: 'checkbox',
          heading: t('Options'),
          param_name: 'options',
          value: {
            'progress-striped': t("Add Stripes?"),
            'active': t("Add animation? Will be visible with striped bars."),
          },
        },
      ],
      render: function($, p, fp) {
        var options = this.attrs['options'];
        if (options != '')
          options = _.map(options.split(','), function(value) {
            return p + value;
          }).join(' ');
        this.dom_element = $('<div class="az-element az-progress-bar ' + p + 'progress ' + this.attrs['el_class'] + ' ' + options + '" style="' + this.attrs['style'] + '"><div class="' + p + 'progress-bar ' + this.attrs['type'] + '" role="progressbar" aria-valuenow="' + this.attrs['width'] + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + this.attrs['width'] + '%">' + this.attrs['label'] + '</div></div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_images_carousel',
      name: t('Images carousel'),
      icon: 'et et-icon-pictures',
      // description: t('Image Slider'),
      params: [
        {
          type: 'images',
          heading: t('Images'),
          param_name: 'images',
        },
        {
          type: 'textfield',
          heading: t('Image width'),
          param_name: 'width',
          description: t('You can use px, em, %, etc. or enter just a number and it will use pixels.'),
          can_be_empty: true,
          value: '100%',
        },
        {
          type: 'integer_slider',
          heading: t('Image height'),
          param_name: 'height',
          can_be_empty: true,
          max: '1000',
          value: '100',
        },
        {
          type: 'integer_slider',
          heading: t('Interval'),
          param_name: 'interval',
          max: '10000',
          value: '5000',
        },
        {
          type: 'checkbox',
          heading: t('Hide'),
          param_name: 'hide',
          value: {
            'pagination_control': t("Hide pagination control"),
            'prev_next_buttons': t("Hide prev/next buttons"),
          },
        },
      ],
      show_settings_on_create: true,
      showed: function($, p, fp) {


        this.baseclass.prototype.showed.apply(this, arguments);
        var element = this;
        $(this.dom_element)[fp + 'carousel']({
          interval: this.attrs['interval'],
          pause: 'hover',
        });
      },
      render: function($, p, fp) {


        var id = this.id;
        var element = this;
        var images = this.attrs['images'].split(',');
        this.dom_element = $('<div id="' + this.id + '" class="az-element az-images-carousel ' + p + 'carousel ' + p + 'slide ' + this.attrs['el_class'] + '" data-ride="carousel" style="' + this.attrs['style'] + '"></div>');
        var hide = this.attrs['hide'].split(',');
        if ($.inArray('pagination_control', hide) < 0) {
          var indicators = $('<ol class="' + p + 'carousel-indicators"></ol>');
          for (var i = 0; i < images.length; i++) {
            $(indicators).append('<li data-target="#' + this.id + '" data-slide-to="' + i + '"></li>');
          }
          $(this.dom_element).append(indicators);
        }

        var inner = $('<div class="' + p + 'carousel-inner"></div>');
        for (var i = 0; i < images.length; i++) {
          var item = $('<div class="' + p + 'item"></div>').appendTo(inner);
          $(item).css('background-image', 'url(' + images[i] + ')');
          $(item).css('background-position', 'center');
          $(item).css('background-repeat', 'no-repeat');
          $(item).css('background-size', 'cover');
          $(item).width(this.attrs['width']);
          $(item).height(this.attrs['height'] + 'px');
        }
        $(this.dom_element).append(inner);
        if ($.inArray('prev_next_buttons', hide) < 0) {
          var controls = $('<a class="' + p + 'left ' + p + 'carousel-control" href="#' + this.id + '" data-slide="prev"><span class="' + p + 'glyphicon ' + p + 'glyphicon-chevron-left"></span></a><a class="' + p + 'right ' + p + 'carousel-control" href="#' + this.id + '" data-slide="next"><span class="' + p + 'glyphicon ' + p + 'glyphicon-chevron-right"></span></a>');
          $(this.dom_element).append(controls);
        }

        $(this.dom_element).find('.' + p + 'carousel-indicators li:first').addClass(p + 'active');
        $(this.dom_element).find('.' + p + 'carousel-inner .' + p + 'item:first').addClass(p + 'active');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_video',
      name: t('Video'),
      icon: 'et et-icon-video',
      // description: t('YouTube / Vimeo video'),
      params: [
        {
          type: 'textfield',
          heading: t('Video link'),
          param_name: 'link',
          description: t('Link Youtube or Vimeo video'),
        },
        {
          type: 'textfield',
          heading: t('Video width'),
          param_name: 'width',
          description: t('You can use px, em, %, etc. or enter just a number and it will use pixels.'),
          value: '100%',
        },
      ],
      show_settings_on_create: true,
      render: function($, p, fp) {

        function youtube_parser(url) {
          var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
          var match = url.match(regExp);
          if (match && match[7].length == 11) {
            return match[7];
          } else {
            return false;
          }
        }
        function vimeo_parser(url) {
          var m = url.match(/^.+vimeo.com\/(.*\/)?([^#\?]*)/);
          return m ? m[2] || m[1] : false;
        }
        var url = youtube_parser(this.attrs['link']);
        if (url) {
          url = 'http://www.youtube.com/embed/' + url;
        } else {
          url = vimeo_parser(this.attrs['link']);
          if (url) {
            url = 'http://player.vimeo.com/video/' + url;
          } else {
            url = '';
          }
        }
        this.dom_element = $('<div class="az-element az-video embed-responsive embed-responsive-16by9 ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"></div>');
        function renderVideo(url, style, width, height) {
          if ($.isNumeric(width))
            width = width + 'px';
          var iframe = $('<iframe src="' + url + '" type="text/html" webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder="0"></iframe>');
          $(iframe).attr('style', style);
          if (width.length > 0)
            $(iframe).css('width', width);
          return iframe;
        }

        var video = renderVideo(url, this.attrs['style'], this.attrs['width'], this.attrs['height']);
        $(video).appendTo(this.dom_element);
        this.baseclass.prototype.render.apply(this, arguments);
      }
    },
    {
      base: 'az_html',
      name: t('HTML'),
      icon: 'et et-icon-search',
      // description: t('HTML Editor'),
      params: [
        {
          type: 'html',
          heading: t('Raw html'),
          param_name: 'content',
          description: t('Enter your HTML content.'),
          value: t('<p>I am raw html block.<br/>Click edit button to change this HTML</p>'),
        },
      ],
      show_settings_on_create: true,
      is_container: true,
      has_content: true,
      render: function($, p, fp) {

        this.dom_element = $('<div class="az-element az-html ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '">' + this.attrs['content'] + '</div>');
        this.dom_content_element = this.dom_element;
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_circle_counter',
      name: t('Circle counter'),
      icon: 'et et-icon-speedometer',
      // description: t('Infographic Counter'),
      params: [
        {
          type: 'colorpicker',
          heading: t('Foreground color'),
          param_name: 'fgcolor',
          description: t('Is the foreground color of the circle.'),
          value: '#333333',
        },
        {
          type: 'colorpicker',
          heading: t('Background color'),
          param_name: 'bgcolor',
          description: t('Is the background color of the circle.'),
          value: '#999999',
        },
        {
          type: 'colorpicker',
          heading: t('Fill'),
          param_name: 'fill',
          description: t('Is the background color of the whole circle (can be empty if you dont want to set a background to the whole circle).'),
        },
        {
          type: 'checkbox',
          heading: t('Half?'),
          param_name: 'type',
          description: t('Full or half circle for example data-type="half" if not set the circle will be a full circle.'),
          value: {
            'half': t("Yes"),
          },
        },
        {
          type: 'integer_slider',
          heading: t('Dimension'),
          param_name: 'dimension',
          max: '500',
          description: t('Is the height and width of the element.'),
          value: '250',
        },
        {
          type: 'textfield',
          heading: t('Text'),
          param_name: 'text',
          description: t('Will be displayed inside of the circle over the info element.'),
          tab: t('Circle content'),
        },
        {
          type: 'integer_slider',
          heading: t('Font size'),
          param_name: 'fontsize',
          max: '100',
          description: t('Is the font size for the text element.'),
          value: '16',
          tab: t('Circle content'),
        },
        {
          type: 'textfield',
          heading: t('Info'),
          param_name: 'info',
          description: t('Will be displayed inside of the circle bellow the text element (can be empty if you dont want to show info text).'),
          tab: t('Circle content'),
        },
        {
          type: 'integer_slider',
          heading: t('Width'),
          param_name: 'width',
          max: '100',
          description: t('Is the thickness of circle.'),
          value: '5',
        },
        {
          type: 'integer_slider',
          heading: t('Percent'),
          param_name: 'percent',
          description: t('Can be 1 to 100.'),
          value: '50',
        },
        {
          type: 'dropdown',
          heading: t('Border'),
          param_name: 'border',
          description: t('Will change the styling of the circle. The line for showing the percentage value will be displayed inside or outside.'),
          value: {
            'default': t('Default'),
            'inline': t('Inline'),
            'outline': t('Outline'),
          },
        },
        {
          type: 'icon',
          heading: t('Icon'),
          param_name: 'icon',
          tab: t('Icon'),
        },
        {
          type: 'integer_slider',
          heading: t('Icon size'),
          param_name: 'icon_size',
          max: '100',
          description: t('Will set the font size of the icon.'),
          value: '16',
          tab: t('Icon'),
        },
        {
          type: 'colorpicker',
          heading: t('Icon color'),
          param_name: 'icon_color',
          description: t('Will set the font color of the icon.'),
          tab: t('Icon'),
        },
      ],
      show_settings_on_create: true,
      frontend_render: true,
      showed: function($, p, fp) {

        this.baseclass.prototype.showed.apply(this, arguments);
        var icon_set = this.attrs['icon'].charAt(0);
        switch (icon_set) {
          case 'e':
            this.add_css('vendor/et-line-font/et-line-font.css', 'ETLineFont' in $.fn, function() {});
            break;
          case 'f':
            this.add_css('vendor/font-awesome/css/font-awesome.min.css', 'fontAwesome' in $.fn, function() {});
            break;
          case 'p':
            this.add_css('vendor/pe-icon-7-stroke/css/pe-icon-7-stroke', 'PELineFont' in $.fn, function() {});
            break;
          default:
            break;
        }
        var element = this;
        this.add_css('vendor/jquery.circliful/css/jquery.circliful.css', 'circliful' in $.fn, function() {});
        this.add_js_list({
          paths: ['vendor/jquery.circliful/js/jquery.circliful.min.js', 'vendor/jquery.waypoints/lib/jquery.waypoints.min.js'],
          loaded: 'waypoint' in $.fn && 'circliful' in $.fn,
          callback: function() {
            $(element.dom_element).waypoint(function(direction) {
              $(element.dom_element).find('#' + element.id).once().circliful();
            }, {offset: '100%', triggerOnce: true});
            $(document).trigger('scroll');
          }});
      },
      render: function($, p, fp) {
        if (this.attrs['icon']) {
          var circliful_icon = '" data-icon=" ' + this.attrs['icon']
            + '" data-iconsize="' + this.attrs['icon_size']
            + '" data-iconcolor="' + this.attrs['icon_color'];
        }
        else {
          var circliful_icon = '';
        }

        this.dom_element = $('<div class="az-element az-circle-counter '
            + this.attrs['el_class'] + '" style="' + this.attrs['style']
            + '"><div id="' + this.id + '" data-dimension="'
            + this.attrs['dimension']
            + '" data-text="' + this.attrs['text']
            + '" data-info="' + this.attrs['info']
            + '" data-width="' + this.attrs['width']
            + '" data-fontsize="' + this.attrs['fontsize']
            + '" data-type="' + this.attrs['type']
            + '" data-percent="' + this.attrs['percent']
            + '" data-fgcolor="' + this.attrs['fgcolor']
            + '" data-bgcolor="' + this.attrs['bgcolor']
            + '" data-fill="' + this.attrs['fill']
            + '" data-border="' + this.attrs['border']
            + circliful_icon
            + '"></div></div>');

        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_counter',
      name: t('Number Counter'),
      icon: 'et et-icon-hourglass',
      // description: t('Count up or down'),
      params: [
        {
          type: 'textfield',
          heading: t('Start'),
          param_name: 'start',
          description: t('Enter the number to start counting from.'),
          value: '0',
        },
        {
          type: 'textfield',
          heading: t('End'),
          param_name: 'end',
          description: t('Enter the number to count up to.'),
          value: '100',
        },
        {
          type: 'integer_slider',
          heading: t('Font Size'),
          param_name: 'fontsize',
          max: '200',
          description: t('Select the font size for the counter number.'),
          value: '30',
        },
        {
          type: 'integer_slider',
          heading: t('Speed'),
          param_name: 'speed',
          max: '10000',
          description: t('Select the speed in ms for the counter to finish.'),
          value: '2000',
        },
        {
          type: 'dropdown',
          heading: t('Thousand Seperator'),
          param_name: 'seperator',
          description: t('Select a character to seperate thousands in the end number.'),
          value: {
            '': t('None'),
            ',': t('Comma'),
            '.': t('Dot'),
            ' ': t('Space'),
          },
        },
        {
          type: 'textfield',
          heading: t('Prefix'),
          param_name: 'prefix',
          description: t('Enter any character to be shown before the number (i.e. $).'),
        },
        {
          type: 'textfield',
          heading: t('Postfix'),
          param_name: 'postfix',
          description: t('Enter any character to be shown after the number (i.e. %).'),
        },
      ],
      show_settings_on_create: true,
      showed: function($, p, fp) {

        this.baseclass.prototype.showed.apply(this, arguments);
        var element = this;
        this.add_js_list({
          paths: ['vendor/jquery-countTo/jquery.countTo.min.js', 'vendor/jquery.waypoints/lib/jquery.waypoints.min.js'],
          loaded: 'waypoint' in $.fn && 'countTo' in $.fn,
          callback: function() {
            $(element.dom_element).waypoint(function(direction) {
              $(element.dom_element).find('#' + element.id).countTo({
                from: Math.round(element.attrs['start']),
                to: Math.round(element.attrs['end']),
                speed: Math.round(element.attrs['speed']),
                refreshInterval: 50,
                decimals: Math.round(element.attrs['seperator']),
                formatter: function(t, e) {
                  return element.attrs['prefix'] + t.toFixed(e.decimals) + element.attrs['postfix'];
                }});
            }, {offset: '100%', triggerOnce: true});
            $(document).trigger('scroll');
//        $(element.dom_element).waypoint({
//          handler: function() {
//          }
//        });
          }});
      },
      render: function($, p, fp) {

        this.dom_element = $('<div class="az-element az-counter"><div id="' + this.id + '" class="' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '">' + this.attrs['start'] + '</div></div>');
        $(this.dom_element).find('#' + this.id).css('font-size', this.attrs['fontsize'] + 'px');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_countdown',
      name: t('Countdown Timer'),
      icon: 'et et-icon-clock',
      // description: t('Count to date/time'),
      params: [
        {
          type: 'dropdown',
          heading: t('Countdown style'),
          param_name: 'countdown_style',
          description: t('Select the style for the countdown element.'),
          value: {
            'plain': t('Plain'),
            'style1': t('Grid'),
            'style6': t('3D Flip'),
            'style9': t('Disc'),
            'style10': t('Airport Style'),
            'style12': t('Transparent'),
          },
        },
        {
          type: 'dropdown',
          heading: t('Date / Time Limitations'),
          param_name: 'counter_scope',
          description: t('Select the countdown scope in terms of date and time.'),
          value: {
            'date': t('Specify Date Only'),
            'date_time': t('Specify Date and Time'),
            'repeating': t('Specifiy Time Only (repeating on every day)'),
            'resetting': t('Resetting Counter (set interval up to 24 hours)'),
          },
        },
        {
          type: 'datetime',
          heading: t('Date'),
          param_name: 'date',
          datepicker: true,
          description: t('Select the date to which you want to count down to.'),
          formatDate: 'd.m.Y',
          dependency: {'element': 'counter_scope', 'value': ['date']},
        },
        {
          type: 'datetime',
          heading: t('Date / Time'),
          param_name: 'date_time',
          timepicker: true,
          datepicker: true,
          description: t('Select the date and time to which you want to count down to.'),
          formatDate: 'd.m.Y',
          formatTime: 'H',
          dependency: {'element': 'counter_scope', 'value': ['date_time']},
        },
        {
          type: 'datetime',
          heading: t('Time'),
          param_name: 'time',
          timepicker: true,
          description: t('Select the time on the day above to which you want to count down to.'),
          formatTime: 'H',
          dependency: {'element': 'counter_scope', 'value': ['repeating']},
        },
        {
          type: 'integer_slider',
          heading: t('Reset in Hours'),
          param_name: 'reset_hours',
          max: 24,
          description: t('Define the number of hours until countdown reset.'),
          dependency: {'element': 'counter_scope', 'value': ['resetting']},
        },
        {
          type: 'integer_slider',
          heading: t('Reset in Minutes'),
          param_name: 'reset_minutes',
          max: 60,
          description: t('Define the number of minutes until countdown reset.'),
          dependency: {'element': 'counter_scope', 'value': ['resetting']},
        },
        {
          type: 'integer_slider',
          heading: t('Reset in Seconds'),
          param_name: 'reset_seconds',
          max: 60,
          description: t('Define the number of seconds until countdown reset.'),
          dependency: {'element': 'counter_scope', 'value': ['resetting']},
        },
        {
          type: 'link',
          heading: t('Page Referrer'),
          param_name: 'referrer',
          description: t('Provide an optional link to another site/page to be opened after countdown expires.'),
          dependency: {'element': 'counter_scope', 'value': ['repeating', 'resetting']},
        },
        {
          type: 'checkbox',
          heading: t('Automatic Restart'),
          param_name: 'restart',
          description: t('Switch the toggle if you want to restart the countdown after each expiration.'),
          value: {
            'yes': t("Yes"),
          },
          dependency: {'element': 'counter_scope', 'value': ['resetting']},
        },
        {
          type: 'saved_datetime',
          param_name: 'saved',
        },
        {
          type: 'checkbox',
          heading: t('Display Options'),
          param_name: 'display',
          value: {
            'days': t("Show Remaining Days"),
            'hours': t("Show Remaining Hours"),
            'minutes': t("Show Remaining Minutes"),
            'seconds': t("Show Remaining Seconds"),
          },
        },
      ],
      show_settings_on_create: true,
      frontend_render: true,
      showed: function($, p, fp) {

        this.baseclass.prototype.showed.apply(this, arguments);
        var element = this;
        this.add_css('vendor/counteverest/css/counteverest.carbide.css', 'countEverest' in $.fn, function() {});
        this.add_js_list({
          paths: ['vendor/counteverest/js/vendor/jquery.counteverest.min.js', 'vendor/datetimepicker/jquery.datetimepicker.js'],
          loaded: 'countEverest' in $.fn && 'datetimepicker' in $.fn,
          callback: function() {
            var options = {};
            switch (element.attrs['countdown_style']) {
              case 'style6':
                function countEverestFlipAnimate($el, data) {
                  $el.each(function(index) {
                    var $this = $(this),
                        $flipFront = $this.find('.ce-flip-front'),
                        $flipBack = $this.find('.ce-flip-back'),
                        field = $flipBack.text(),
                        fieldOld = $this.attr('data-old');
                    if (typeof fieldOld === 'undefined') {
                      $this.attr('data-old', field);
                    }
                    if (field != fieldOld) {
                      $this.addClass('ce-animate');
                      window.setTimeout(function() {
                        $flipFront.text(field);
                        $this
                            .removeClass('ce-animate')
                            .attr('data-old', field);
                      }, 800);
                    }
                  });
                }
                if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
                  $('html').addClass('internet-explorer');
                }

                options = {
                  daysWrapper: '.ce-days .ce-flip-back',
                  hoursWrapper: '.ce-hours .ce-flip-back',
                  minutesWrapper: '.ce-minutes .ce-flip-back',
                  secondsWrapper: '.ce-seconds .ce-flip-back',
                  wrapDigits: false,
                  onChange: function() {
                    countEverestFlipAnimate($(element.dom_element).find('.ce-countdown .ce-col>div'), this);
                  }
                }
                break;
              case 'style9':
                function deg(v) {
                  return (Math.PI / 180) * v - (Math.PI / 2);
                }

                function drawCircle(canvas, value, max) {
                  var circle = canvas.getContext('2d');

                  circle.clearRect(0, 0, canvas.width, canvas.height);
                  circle.lineWidth = 6;

                  circle.beginPath();
                  circle.arc(
                      canvas.width / 2,
                      canvas.height / 2,
                      canvas.width / 2 - circle.lineWidth,
                      deg(0),
                      deg(360 / max * (max - value)),
                      false);
                  circle.strokeStyle = '#282828';
                  circle.stroke();

                  circle.beginPath();
                  circle.arc(
                      canvas.width / 2,
                      canvas.height / 2,
                      canvas.width / 2 - circle.lineWidth,
                      deg(0),
                      deg(360 / max * (max - value)),
                      true);
                  circle.strokeStyle = '#1488cb';
                  circle.stroke();
                }
                options = {
                  leftHandZeros: false,
                  onChange: function() {
                    drawCircle($(element.dom_element).find('#ce-days').get(0), this.days, 365);
                    drawCircle($(element.dom_element).find('#ce-hours').get(0), this.hours, 24);
                    drawCircle($(element.dom_element).find('#ce-minutes').get(0), this.minutes, 60);
                    drawCircle($(element.dom_element).find('#ce-seconds').get(0), this.seconds, 60);
                  }
                }
                break;
              case 'style10':
                var $countdown = $(element.dom_element).find('.ce-countdown');
                var firstCalculation = true;
                options = {
                  leftHandZeros: true,
                  dayLabel: null,
                  hourLabel: null,
                  minuteLabel: null,
                  secondLabel: null,
                  afterCalculation: function() {
                    var plugin = this,
                        units = {
                          days: this.days,
                          hours: this.hours,
                          minutes: this.minutes,
                          seconds: this.seconds
                        },
                    //max values per unit
                    maxValues = {
                      hours: '23',
                      minutes: '59',
                      seconds: '59'
                    },
                    actClass = 'active',
                        befClass = 'before';

                    //build necessary elements
                    if (firstCalculation == true) {
                      firstCalculation = false;

                      //build necessary markup
                      $countdown.find('.ce-unit-wrap div').each(function() {
                        var $this = $(this),
                            className = $this.attr('class'),
                            unit = className.substring(3);
                            value = units[unit],
                            sub = '',
                            dig = '';

                        //build markup per unit digit
                        for (var x = 0; x < 10; x++) {
                          sub += [
                            '<div class="ce-digits-inner">',
                            '<div class="ce-flip-wrap">',
                            '<div class="ce-up">',
                            '<div class="ce-shadow"></div>',
                            '<div class="ce-inn">' + x + '</div>',
                            '</div>',
                            '<div class="ce-down">',
                            '<div class="ce-shadow"></div>',
                            '<div class="ce-inn">' + x + '</div>',
                            '</div>',
                            '</div>',
                            '</div>'
                          ].join('');
                        }

                        //build markup for number
                        for (var i = 0; i < value.length; i++) {
                          dig += '<div class="ce-digits">' + sub + '</div>';
                        }
                        $this.append(dig);
                      });
                    }

                    //iterate through units
                    $.each(units, function(unit) {
                      var digitCount = $countdown.find('.ce-' + unit + ' .ce-digits').length,
                          maxValueUnit = maxValues[unit],
                          maxValueDigit,
                          value = plugin.strPad(this, digitCount, '0');

                      //iterate through digits of an unit
                      for (var i = value.length - 1; i >= 0; i--) {
                        var $digitsWrap = $countdown.find('.ce-' + unit + ' .ce-digits:eq(' + (i) + ')'),
                            $digits = $digitsWrap.find('div.ce-digits-inner');

                        //use defined max value for digit or simply 9
                        if (maxValueUnit) {
                          maxValueDigit = (maxValueUnit[i] == 0) ? 9 : maxValueUnit[i];
                        } else {
                          maxValueDigit = 9;
                        }

                        //which numbers get the active and before class
                        var activeIndex = parseInt(value[i]),
                            beforeIndex = (activeIndex == maxValueDigit) ? 0 : activeIndex + 1;

                        //check if value change is needed
                        if ($digits.eq(beforeIndex).hasClass(actClass)) {
                          $digits.parent().addClass('play');
                        }

                        //remove all classes
                        $digits
                            .removeClass(actClass)
                            .removeClass(befClass);

                        //set classes
                        $digits.eq(activeIndex).addClass(actClass);
                        $digits.eq(beforeIndex).addClass(befClass);
                      }
                    });
                  }
                }
                break;
            }
            switch (element.attrs['counter_scope']) {
              case 'date':
                var d = Date.parseDate(element.attrs['date'], 'd.m.Y');
                if (d != null)
                  $(element.dom_element).countEverest($.extend(options, {
                    day: d.getDate(),
                    month: d.getMonth() + 1,
                    year: d.getFullYear(),
                  }));
                break;
              case 'date_time':
                var d = Date.parseDate(element.attrs['date_time'], 'd.m.Y H');
                if (d != null)
                  $(element.dom_element).countEverest($.extend(options, {
                    day: d.getDate(),
                    month: d.getMonth() + 1,
                    year: d.getFullYear(),
                    hour: d.getHours()
                  }));
                break;
              case 'repeating':
                var d = new Date();
                d.setHours(element.attrs['time']);
                if (d != null)
                  $(element.dom_element).countEverest($.extend(options, {
                    day: d.getDate(),
                    month: d.getMonth() + 1,
                    year: d.getFullYear(),
                    hour: d.getHours(),
                    onComplete: function() {
                      if (element.attrs['referrer'] != '') {
                        window.location.replace(element.attrs['referrer']);
                      }
                    }
                  }));
                break;
              case 'resetting':
                if (element.attrs['saved'] != '') {
                  var saved = new Date(element.attrs['saved']);
                  var interval = (Math.round(element.attrs['reset_hours']) * 60 * 60 + Math.round(element.attrs['reset_minutes']) * 60 + Math.round(element.attrs['reset_seconds'])) * 1000;
                  if (element.attrs['restart'] == 'yes') {
                    var current = new Date();
                    var elapsed = current.getTime() - saved.getTime();
                    var k = elapsed / interval;
                    elapsed = elapsed - Math.floor(k) * interval;
                    var delta = interval - elapsed;
                    var d = new Date(current.getTime() + delta);
                    $(element.dom_element).countEverest($.extend(options, {
                      day: d.getDate(),
                      month: d.getMonth() + 1,
                      year: d.getFullYear(),
                      hour: d.getHours(),
                      minute: d.getMinutes(),
                      second: d.getSeconds(),
                      onComplete: function() {
                        if (element.attrs['referrer'] != '') {
                          window.location.replace(element.attrs['referrer']);
                        }
                      }
                    }));
                  } else {
                    var d = new Date(saved.getTime() + interval);
                    $(element.dom_element).countEverest($.extend(options, {
                      day: d.getDate(),
                      month: d.getMonth() + 1,
                      year: d.getFullYear(),
                      hour: d.getHours(),
                      minute: d.getMinutes(),
                      second: d.getSeconds(),
                      onComplete: function() {
                        if (element.attrs['referrer'] != '') {
                          window.location.replace(element.attrs['referrer']);
                        }
                      }
                    }));
                  }
                }
                break;
              default:
                break;
            }
          }});
      },
      render: function($, p, fp) {

        this.dom_element = $('<div class="az-element az-countdown ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"></div>');
        var countdown = $('<div class="ce-countdown"></div>').appendTo(this.dom_element);
        switch (this.attrs['countdown_style']) {
          case 'style1':
            $(this.dom_element).addClass('ce-countdown--theme-1');
            if (_.indexOf(this.attrs['display'].split(','), 'days') >= 0)
              $(countdown).append('<div class="ce-col"><span class="ce-days"></span> <span class="ce-days-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'hours') >= 0)
              $(countdown).append('<div class="ce-col"><span class="ce-hours"></span> <span class="ce-hours-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'minutes') >= 0)
              $(countdown).append('<div class="ce-col"><span class="ce-minutes"></span> <span class="ce-minutes-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'seconds') >= 0)
              $(countdown).append('<div class="ce-col"><span class="ce-seconds"></span> <span class="ce-seconds-label"></span></div>');
            break;
          case 'style6':
            $(this.dom_element).addClass('ce-countdown--theme-6 clearfix');
            if (_.indexOf(this.attrs['display'].split(','), 'days') >= 0)
              $(countdown).append('<div class="ce-col col-md-3"><div class="ce-days"><div class="ce-flip-wrap"><div class="ce-flip-front bg-primary"></div><div class="ce-flip-back bg-primary"></div></div></div><span class="ce-days-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'hours') >= 0)
              $(countdown).append('<div class="ce-col col-md-3"><div class="ce-hours"><div class="ce-flip-wrap"><div class="ce-flip-front bg-primary"></div><div class="ce-flip-back bg-primary"></div></div></div><span class="ce-hours-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'minutes') >= 0)
              $(countdown).append('<div class="ce-col col-md-3"><div class="ce-minutes"><div class="ce-flip-wrap"><div class="ce-flip-front bg-primary"></div><div class="ce-flip-back bg-primary"></div></div></div><span class="ce-minutes-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'seconds') >= 0)
              $(countdown).append('<div class="ce-col col-md-3"><div class="ce-seconds"><div class="ce-flip-wrap"><div class="ce-flip-front bg-primary"></div><div class="ce-flip-back bg-primary"></div></div></div><span class="ce-seconds-label"></span></div>');
            break;
          case 'style9':
            $(this.dom_element).addClass('ce-countdown--theme-9');
            if (_.indexOf(this.attrs['display'].split(','), 'days') >= 0)
              $(countdown).append('<div class="ce-circle"><canvas id="ce-days" width="408" height="408"></canvas><div class="ce-circle__values"><span class="ce-digit ce-days"></span><span class="ce-label ce-days-label"></span></div></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'hours') >= 0)
              $(countdown).append('<div class="ce-circle"><canvas id="ce-hours" width="408" height="408"></canvas><div class="ce-circle__values"><span class="ce-digit ce-hours"></span><span class="ce-label ce-hours-label"></span></div></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'minutes') >= 0)
              $(countdown).append('<div class="ce-circle"><canvas id="ce-minutes" width="408" height="408"></canvas><div class="ce-circle__values"><span class="ce-digit ce-minutes"></span><span class="ce-label ce-minutes-label"></span></div></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'seconds') >= 0)
              $(countdown).append('<div class="ce-circle"><canvas id="ce-seconds" width="408" height="408"></canvas><div class="ce-circle__values"><span class="ce-digit ce-seconds"></span><span class="ce-label ce-seconds-label"></span></div></div>');
            break;
          case 'style10':
            $(this.dom_element).addClass('ce-countdown--theme-10');
            if (_.indexOf(this.attrs['display'].split(','), 'days') >= 0)
              $(countdown).append('<div class="ce-unit-wrap"><div class="ce-days"></div><span class="ce-days-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'hours') >= 0)
              $(countdown).append('<div class="ce-unit-wrap"><div class="ce-hours"></div><span class="ce-hours-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'minutes') >= 0)
              $(countdown).append('<div class="ce-unit-wrap"><div class="ce-minutes"></div><span class="ce-minutes-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'seconds') >= 0)
              $(countdown).append('<div class="ce-unit-wrap"><div class="ce-seconds"></div><span class="ce-seconds-label"></span></div>');
            break;
          case 'style12':
            $(this.dom_element).addClass('ce-countdown--theme-12');
            if (_.indexOf(this.attrs['display'].split(','), 'days') >= 0)
              $(countdown).append('<div class="ce-col"><div class="ce-days ce-digits"></div> <span class="ce-days-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'hours') >= 0)
              $(countdown).append('<div class="ce-col"><div class="ce-hours ce-digits"></div> <span class="ce-hours-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'minutes') >= 0)
              $(countdown).append('<div class="ce-col"><div class="ce-minutes ce-digits"></div> <span class="ce-minutes-label"></span></div>');
            if (_.indexOf(this.attrs['display'].split(','), 'seconds') >= 0)
              $(countdown).append('<div class="ce-col"><div class="ce-seconds ce-digits"></div> <span class="ce-seconds-label"></span></div>');
            break;
          default:
            if (_.indexOf(this.attrs['display'].split(','), 'days') >= 0)
              $(countdown).append('<span class="lead ce-days"></span> <span class="lead ce-days-label"></span> ');
            if (_.indexOf(this.attrs['display'].split(','), 'hours') >= 0)
              $(countdown).append('<span class="lead ce-hours"></span> <span class="lead ce-hours-label"></span> ');
            if (_.indexOf(this.attrs['display'].split(','), 'minutes') >= 0)
              $(countdown).append('<span class="lead ce-minutes"></span> <span class="lead ce-minutes-label"></span> ');
            if (_.indexOf(this.attrs['display'].split(','), 'seconds') >= 0)
              $(countdown).append('<span class="lead ce-seconds"></span> <span class="lead ce-seconds-label"></span> ');
            break;
        }
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'st_social',
      name: t('Social links'),
      icon: 'et et-icon-twitter',
      // description: t('Branded Social Links'),
      params: [
       {
         type: 'html',
         heading: t('Social links'),
         param_name: 'st_social_links',
         description: t('Enter a social brand and URL per line. Example: Facebook="https://www.facebook.com/sooperthemes"'),
         value: "Facebook='https://www.facebook.com/sooperthemes'\nYouTube='https://www.facebook.com/sooperthemes'",
       },
       {
         type: 'dropdown',
         heading: t('Layout'),
         param_name: 'st_type',
         value: {
           'inline': t('Inline'),
           'stacked': t('Stacked'),
         },
       },
       {
         type: 'dropdown',
         heading: t('Style'),
         param_name: 'st_style',
         value: {
           '': t('None'),
           'rounded': t('Rounded'),
           'circle': t('Circle'),
           'square': t('Square'),
         },
       },
       {
         type: 'dropdown',
         heading: t('Size'),
         param_name: 'st_size',
         value: {
           'lg': t('Large'),
           '': t('Small'),
           '2x': t('2x'),
           '3x': t('3x'),
           '4x': t('4x'),
           '5x': t('5x'),
         },
       },
       {
         type: 'dropdown',
         heading: t('Color'),
         param_name: 'st_theme_color',
         value: colors,
       },
       {
         type: 'colorpicker',
         heading: t('Color'),
         param_name: 'st_color',
         dependency: {'element': 'st_theme_color', 'is_empty': {}},
       },
       {
         type: 'dropdown',
         heading: t('Background Color'),
         param_name: 'st_theme_bgcolor',
         value: colors,
       },
       {
         type: 'colorpicker',
         heading: t('Background Color'),
         param_name: 'st_bgcolor',
         dependency: {'element': 'st_theme_bgcolor', 'is_empty': {}},
       },
       {
         type: 'dropdown',
         heading: t('Hover color'),
         param_name: 'st_hover_color',
         value: {
           'brand': t('Brand color'),
           'inherit': t('Inherit'),
           '': t('None'),
         },
       },
       {
         type: 'dropdown',
         heading: t('Border color'),
         param_name: 'st_theme_border_color',
         value: colors,
       },
       {
         type: 'colorpicker',
         heading: t('Border color'),
         param_name: 'st_border_color',
         dependency: {'element': 'st_theme_border_color', 'is_empty': {}},
       },
       {
         type: 'dropdown',
         heading: t('CSS3 Hover Effects'),
         description: t('Setting a CSS3 Hover effect will automatically make the icon a circle-style icon.'),
         param_name: 'st_css3_hover_effects',
         value: {
           '': t('None'),
           'disc': t('Disc'),
           'pulse': t('Pulse'),
         },
       },
      ],
      show_settings_on_create: true,
      showed: function($, p, fp) {
        this.baseclass.prototype.showed.apply(this, arguments);
        this.add_css('css/social.css', 'socialLink' in $.fn, function() {});
        this.add_css('css/icon-helpers.css', 'IconHelpers' in $.fn, function() {});
        this.add_css('vendor/font-awesome/css/font-awesome.min.css', 'fontAwesome' in $.fn, function() {});
        var element = this;
      },
      render: function($, p, fp) {
       this.dom_element = $('<ul class="az-element st-social stbe-social-links ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"></ul>');
       if (this.attrs['st_theme_bgcolor'] == 'brand')
         $(this.dom_element).addClass('stbe-social-links--bgcolor-brand');
       if (this.attrs['st_hover_bgcolor'] == 'brand')
         $(this.dom_element).addClass('stbe-social-links--hover-bgcolor-brand');
       if (this.attrs['st_theme_color'] == 'brand')
         $(this.dom_element).addClass('stbe-social-links--color-brand');
       if (this.attrs['st_hover_color'] == 'brand')
         $(this.dom_element).addClass('stbe-social-links--hover-color-brand');
       if (this.attrs['st_type'] == 'stacked')
         $(this.dom_element).addClass('stbe-social-links-stacked');

       var icon_style = '';
       // Foreground color
       if (this.attrs['st_bgcolor'] && (this.attrs['st_theme_color'] == '')) {
         icon_style = icon_style + 'color: ' + this.attrs['st_color'] + ';';
       }
       // Background color
       if (this.attrs['st_bgcolor'] && (this.attrs['st_theme_bgcolor'] == '')) {
         icon_style = icon_style + 'background-color: ' + this.attrs['st_bgcolor'] + ';';
       }
       // Border color
       if (this.attrs['st_border_color'] && (this.attrs['st_theme_border_color'] == '')) {
         icon_style = icon_style + 'border-color: ' + this.attrs['st_border_color'] + ';';
       }


       var links = this.attrs['st_social_links'].split("\n");
       for (var i in links) {
         if (links[i] != '') {
           var link = links[i].split("=");
           var name = link[0].replace(/['"]+/g, '').toLowerCase();
           var url = link[1].replace(/['"]+/g, '');

           var icon_classes = ['fa'];
           icon_classes.push('fa-' + this.attrs['st_size']);
           icon_classes.push('fa-' + name);
           icon_classes.push('stbe-util-icon-' + this.attrs['st_style']);
           // if (this.attrs['st_css3_hover_effects'])
           //   icon_classes.push('stbe-util-icon-fx');
           //   icon_classes.push('stbe-util-fx-' + this.attrs['st_css3_hover_effects']);
           // if (this.attrs['st_border_color'] != '' || this.attrs['st_theme_border_color'] != '')
           //   icon_classes.push('stbe-util-icon-border');

           $(this.dom_element).append('<li class="stbe-social-links__item"><a href="' + url + '"><i class="' + icon_classes.join(' ') + '" style="' + icon_style + '" data-toggle="tooltip" data-placement="top" title="' + name + '"></i></a></li>')
         }
      }
       this.baseclass.prototype.render.apply(this, arguments);
      },
    },
  ];
  if ('carbide_elements' in window) {
    window.carbide_elements = window.carbide_elements.concat(carbide_elements);
  } else {
    window.carbide_elements = carbide_elements;
  }

  var carbide_form_elements = [
    {
      base: 'az_dropdown',
      name: t('Dropdown'),
      icon: 'fa fa-level-down',
      // description: t('Form dropdown field'),
      params: [
        {
          type: 'rawtext',
          heading: t('Options'),
          param_name: 'options',
          description: t('Separated by new line.'),
        },
      ],
      hidden: true,
      show_settings_on_create: true,
      render: function($, p, fp) {


        var required = (this.attrs['required'] == 'yes') ? 'required' : '';
        var select = '<select name="' + this.attrs['name'] + '" class="' + p + 'form-control" ' + required + '>';
        select += '<option value="">' + t('Select') + '</option>';
        var options = this.attrs['options'].split("\n");
        for (var i = 0; i < options.length; i++) {
          select += '<option value="' + options[i] + '">' + options[i] + '</option>';
        }
        select += '/<select>';
        this.dom_element = $('<div class="az-element az-dropdown ' + p + 'form-group' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '" ><label>' + this.attrs['title'] + '</label><div>' + select + '</div></div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_checkbox',
      name: t('Checkbox'),
      icon: 'fa fa-check-square-o',
      // description: t('Form Checkbox field'),
      params: [
        {
          type: 'rawtext',
          heading: t('Options'),
          param_name: 'options',
          description: t('Name|Title separated by new line.'),
        },
      ],
      hidden: true,
      show_settings_on_create: true,
      render: function($, p, fp) {


        var inputs = '';
        var options = this.attrs['options'].split("\n");
        for (var i = 0; i < options.length; i++) {
          inputs += '<div class="' + p + 'checkbox"><label><input name="' + options[i].split("|")[0] + '" type="checkbox" value="' + options[i].split("|")[0] + '">' + options[i].split("|")[1] + '</label></div>';
        }
        this.dom_element = $('<div class="az-element az-checkbox ' + p + 'form-group' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"><label>' + this.attrs['title'] + '</label><div>' + inputs + '</div></div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_textfield',
      name: t('Textfield'),
      icon: 'fa fa-pencil-square-o',
      // description: t('Form text field'),
      params: [
      ],
      hidden: true,
      show_settings_on_create: true,
      render: function($, p, fp) {


        var required = (this.attrs['required'] == 'yes') ? 'required' : '';
        this.dom_element = $('<div class="az-element az-textfield ' + p + 'form-group' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"><div><input class="' + p + 'form-control" name="' + this.attrs['name'] + '" type="text" placeholder="' + this.attrs['title'] + '" ' + required + '></div></div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_hidden',
      name: t('Hidden data'),
      icon: 'fa fa-ticket',
      // description: t('Hidden text field'),
      params: [
        {
          type: 'textfield',
          heading: t('Data'),
          param_name: 'data',
        },
      ],
      hidden: true,
      show_settings_on_create: true,
      render: function($, p, fp) {
        this.dom_element = $('<input name="' + this.attrs['name'] + '" type="hidden" value="' + this.attrs['data'] + '" >');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_number',
      name: t('Number'),
      icon: 'fa fa-arrows-h',
      // description: t('Form number field with slider'),
      params: [
        {
          type: 'textfield',
          heading: t('Minimum'),
          param_name: 'minimum',
          description: t('Required.'),
          required: true,
          value: '0',
        },
        {
          type: 'textfield',
          heading: t('Maximum'),
          param_name: 'maximum',
          description: t('Required.'),
          required: true,
          value: '100',
        },
        {
          type: 'textfield',
          heading: t('Step'),
          param_name: 'step',
          description: t('Required.'),
          required: true,
          value: '1',
        },
      ],
      hidden: true,
      show_settings_on_create: true,
      showed: function($, p, fp) {
        var element = this;
        this.baseclass.prototype.showed.apply(this, arguments);
        function nouislider(slider, min, max, value, step, target) {
          element.add_css('vendor/noUiSlider/nouislider.min.css', function() {});
          element.add_js({
            path: 'vendor/noUiSlider/jquery.nouislider.min.js',
            callback: function() {
              $(slider).noUiSlider({
                start: [(value == '' || isNaN(parseFloat(value)) || value == 'NaN') ? min : parseFloat(value)],
                step: parseFloat(step),
                range: {
                  min: [parseFloat(min)],
                  max: [parseFloat(max)]
                },
              }).on('change', function() {
                $(target).val($(slider).val());
              });
            }
          });
        }
        nouislider($(this.dom_element).find('.slider'), this.attrs['minimum'], this.attrs['maximum'], '', this.attrs['step'], $(this.dom_element).find('input'));
      },
      render: function($, p, fp) {


        var required = (this.attrs['required'] == 'yes') ? 'required' : '';
        this.dom_element = $('<div class="az-element az-number ' + p + 'form-group' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"><div><input class="' + p + 'form-control" name="' + this.attrs['name'] + '" type="text" ' + required + ' placeholder="' + this.attrs['title'] + '"></div><div class="slider"></div></div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_date',
      name: t('Date'),
      icon: 'fa fa-calendar',
      // description: t('Form date field'),
      params: [
        {
          type: 'checkbox',
          heading: t('Time is enabled?'),
          param_name: 'time',
          value: {
            'yes': t("Yes"),
          },
        },
      ],
      hidden: true,
      show_settings_on_create: true,
      frontend_render: true,
      showed: function($, p, fp) {

        this.baseclass.prototype.showed.apply(this, arguments);
        var element = this;
        this.add_css('vendor/datetimepicker/jquery.datetimepicker.css', 'datetimepicker' in $.fn, function() {});
        this.add_js({
          path: 'vendor/datetimepicker/jquery.datetimepicker.js',
          callback: function() {
            function lang() {
              if ('carbide_lang' in window) {
                return window.carbide_lang;
              } else {
                return 'en';
              }
            }
            $(element.dom_element).find('input').datetimepicker({
              format: (element.attrs['time'] == 'yes') ? 'Y/m/d H:i' : 'Y/m/d',
              timepicker: (element.attrs['time'] == 'yes'),
              datepicker: true,
              inline: true,
              lang: lang()
            });
          }
        });
      },
      render: function($, p, fp) {

        var required = (this.attrs['required'] == 'yes') ? 'required' : '';
        this.dom_element = $('<div class="az-element az-date ' + p + 'form-group' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"><label>' + this.attrs['title'] + '</label><div><input class="' + p + 'form-control" name="' + this.attrs['name'] + '" type="text" ' + required + '></div></div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
    {
      base: 'az_textarea',
      name: t('Textarea'),
      icon: 'fa fa-file-text-o',
      // description: t('Form text area field'),
      params: [
      ],
      hidden: true,
      show_settings_on_create: true,
      render: function($, p, fp) {

        var required = (this.attrs['required'] == 'yes') ? 'required' : '';
        this.dom_element = $('<div class="az-element az-textarea ' + p + 'form-group' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"><div><textarea class="' + p + 'form-control" rows="10" cols="45" name="' + this.attrs['name'] + '" " placeholder="' + this.attrs['title'] + '" ' + required + '></textarea></div></div>');
        this.baseclass.prototype.render.apply(this, arguments);
      },
    },
  ];
  if ('carbide_form_elements' in window) {
    window.carbide_form_elements = window.carbide_form_elements.concat(carbide_form_elements);
  } else {
    window.carbide_form_elements = carbide_form_elements;
  }

})(window.jQuery);
