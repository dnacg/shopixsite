<?php
/**
 * @file
 * glazed_content_elements_examples_2.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function glazed_content_elements_examples_2_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_call-to-action:uuid/node/a13b4055-cfd9-4437-9127-35acd06cf9ea.
  $menu_links['main-menu_call-to-action:uuid/node/a13b4055-cfd9-4437-9127-35acd06cf9ea'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/a13b4055-cfd9-4437-9127-35acd06cf9ea',
    'router_path' => 'uuid',
    'link_title' => 'Call to Action',
    'options' => array(
      'identifier' => 'main-menu_call-to-action:uuid/node/a13b4055-cfd9-4437-9127-35acd06cf9ea',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -38,
    'customized' => 1,
    'uuid' => 'a13b4055-cfd9-4437-9127-35acd06cf9ea',
    'vuuid' => '3a5d03ad-0dd4-4ac9-9120-37b79537d9af',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_circle-counter:uuid/node/5c9b6810-6a1f-4109-9a8b-2560f5c65d0f.
  $menu_links['main-menu_circle-counter:uuid/node/5c9b6810-6a1f-4109-9a8b-2560f5c65d0f'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/5c9b6810-6a1f-4109-9a8b-2560f5c65d0f',
    'router_path' => 'uuid',
    'link_title' => 'Circle Counter',
    'options' => array(
      'identifier' => 'main-menu_circle-counter:uuid/node/5c9b6810-6a1f-4109-9a8b-2560f5c65d0f',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -32,
    'customized' => 1,
    'uuid' => '5c9b6810-6a1f-4109-9a8b-2560f5c65d0f',
    'vuuid' => 'c9f1c881-f929-445f-8e40-dd2ab6e404be',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_count-down-timer:uuid/node/9e718e9f-d297-46a8-a908-142b3853d829.
  $menu_links['main-menu_count-down-timer:uuid/node/9e718e9f-d297-46a8-a908-142b3853d829'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/9e718e9f-d297-46a8-a908-142b3853d829',
    'router_path' => 'uuid',
    'link_title' => 'Count Down Timer',
    'options' => array(
      'identifier' => 'main-menu_count-down-timer:uuid/node/9e718e9f-d297-46a8-a908-142b3853d829',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -31,
    'customized' => 1,
    'uuid' => '9e718e9f-d297-46a8-a908-142b3853d829',
    'vuuid' => 'e017c605-5d05-4354-b603-8b7f3eef0a51',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_counter:uuid/node/9c1a0b9b-a4c6-4c74-b134-975d2f0c0f33.
  $menu_links['main-menu_counter:uuid/node/9c1a0b9b-a4c6-4c74-b134-975d2f0c0f33'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/9c1a0b9b-a4c6-4c74-b134-975d2f0c0f33',
    'router_path' => 'uuid',
    'link_title' => 'Counter',
    'options' => array(
      'identifier' => 'main-menu_counter:uuid/node/9c1a0b9b-a4c6-4c74-b134-975d2f0c0f33',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -30,
    'customized' => 1,
    'uuid' => '9c1a0b9b-a4c6-4c74-b134-975d2f0c0f33',
    'vuuid' => 'a120713a-2aee-4d8f-90ce-5bf3ab1b6f92',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_drop-shadow:uuid/node/88d22be5-b9e3-4953-b98b-9c2deac33640.
  $menu_links['main-menu_drop-shadow:uuid/node/88d22be5-b9e3-4953-b98b-9c2deac33640'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/88d22be5-b9e3-4953-b98b-9c2deac33640',
    'router_path' => 'uuid',
    'link_title' => 'Drop Shadow',
    'options' => array(
      'identifier' => 'main-menu_drop-shadow:uuid/node/88d22be5-b9e3-4953-b98b-9c2deac33640',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '88d22be5-b9e3-4953-b98b-9c2deac33640',
    'vuuid' => '5245195f-d758-4925-b682-ef1a252f68b3',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_icon-box:uuid/node/8cee9fbf-2933-45e5-9c12-b6aab9075cfb.
  $menu_links['main-menu_icon-box:uuid/node/8cee9fbf-2933-45e5-9c12-b6aab9075cfb'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/8cee9fbf-2933-45e5-9c12-b6aab9075cfb',
    'router_path' => 'uuid',
    'link_title' => 'Icon Box',
    'options' => array(
      'identifier' => 'main-menu_icon-box:uuid/node/8cee9fbf-2933-45e5-9c12-b6aab9075cfb',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '8cee9fbf-2933-45e5-9c12-b6aab9075cfb',
    'vuuid' => '455899f7-68f8-4647-b6dd-e9da36dff9a1',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_icon-well:uuid/node/5ff84c19-e9e1-4760-9253-abbedce064cd.
  $menu_links['main-menu_icon-well:uuid/node/5ff84c19-e9e1-4760-9253-abbedce064cd'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/5ff84c19-e9e1-4760-9253-abbedce064cd',
    'router_path' => 'uuid',
    'link_title' => 'Icon Well',
    'options' => array(
      'identifier' => 'main-menu_icon-well:uuid/node/5ff84c19-e9e1-4760-9253-abbedce064cd',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '5ff84c19-e9e1-4760-9253-abbedce064cd',
    'vuuid' => 'd1b3b748-2146-4d3c-9a7b-a88b8a2101ff',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_motion-image-box:uuid/node/448c021f-1d62-43a3-aaa9-6111e2dca456.
  $menu_links['main-menu_motion-image-box:uuid/node/448c021f-1d62-43a3-aaa9-6111e2dca456'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/448c021f-1d62-43a3-aaa9-6111e2dca456',
    'router_path' => 'uuid',
    'link_title' => 'Motion Image Box',
    'options' => array(
      'identifier' => 'main-menu_motion-image-box:uuid/node/448c021f-1d62-43a3-aaa9-6111e2dca456',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '448c021f-1d62-43a3-aaa9-6111e2dca456',
    'vuuid' => 'd5d3f777-698e-4f7d-bf62-52cd23ce3aa5',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_pricing-table:uuid/node/e54ed492-aa67-4f17-84c2-a841dac84118.
  $menu_links['main-menu_pricing-table:uuid/node/e54ed492-aa67-4f17-84c2-a841dac84118'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/e54ed492-aa67-4f17-84c2-a841dac84118',
    'router_path' => 'uuid',
    'link_title' => 'Pricing Table',
    'options' => array(
      'identifier' => 'main-menu_pricing-table:uuid/node/e54ed492-aa67-4f17-84c2-a841dac84118',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -34,
    'customized' => 1,
    'uuid' => 'e54ed492-aa67-4f17-84c2-a841dac84118',
    'vuuid' => '87e48d61-71f7-4c9f-87e8-8d52408f342d',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_team-box:uuid/node/706f5a68-da75-4834-a050-9de1b4d58fd6.
  $menu_links['main-menu_team-box:uuid/node/706f5a68-da75-4834-a050-9de1b4d58fd6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/706f5a68-da75-4834-a050-9de1b4d58fd6',
    'router_path' => 'uuid',
    'link_title' => 'Team Box',
    'options' => array(
      'identifier' => 'main-menu_team-box:uuid/node/706f5a68-da75-4834-a050-9de1b4d58fd6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -37,
    'customized' => 1,
    'uuid' => '706f5a68-da75-4834-a050-9de1b4d58fd6',
    'vuuid' => '3a8fb281-630c-4b76-a6a8-81eb73e4acbb',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_time-line:uuid/node/b50ca955-ea53-4854-a408-6f39a7262093.
  $menu_links['main-menu_time-line:uuid/node/b50ca955-ea53-4854-a408-6f39a7262093'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/b50ca955-ea53-4854-a408-6f39a7262093',
    'router_path' => 'uuid',
    'link_title' => 'Time Line',
    'options' => array(
      'identifier' => 'main-menu_time-line:uuid/node/b50ca955-ea53-4854-a408-6f39a7262093',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -36,
    'customized' => 1,
    'uuid' => 'b50ca955-ea53-4854-a408-6f39a7262093',
    'vuuid' => 'faf55d30-0546-420a-be31-f76f9edc3ca3',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Call to Action');
  t('Circle Counter');
  t('Count Down Timer');
  t('Counter');
  t('Drop Shadow');
  t('Icon Box');
  t('Icon Well');
  t('Motion Image Box');
  t('Pricing Table');
  t('Team Box');
  t('Time Line');

  return $menu_links;
}
