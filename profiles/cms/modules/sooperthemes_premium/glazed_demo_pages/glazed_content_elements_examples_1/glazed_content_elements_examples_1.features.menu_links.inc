<?php
/**
 * @file
 * glazed_content_elements_examples_1.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function glazed_content_elements_examples_1_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_blockquote:uuid/node/229435b1-1c29-4eab-a591-9c8ceaaff8a0.
  $menu_links['main-menu_blockquote:uuid/node/229435b1-1c29-4eab-a591-9c8ceaaff8a0'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/229435b1-1c29-4eab-a591-9c8ceaaff8a0',
    'router_path' => 'uuid',
    'link_title' => 'Blockquote',
    'options' => array(
      'identifier' => 'main-menu_blockquote:uuid/node/229435b1-1c29-4eab-a591-9c8ceaaff8a0',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
    'customized' => 0,
    'uuid' => '229435b1-1c29-4eab-a591-9c8ceaaff8a0',
    'vuuid' => 'b982c6d1-9784-4777-8e96-297e51f96f62',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_buttons:uuid/node/34d13068-3849-4eaf-89d4-880954ac8bdb.
  $menu_links['main-menu_buttons:uuid/node/34d13068-3849-4eaf-89d4-880954ac8bdb'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/34d13068-3849-4eaf-89d4-880954ac8bdb',
    'router_path' => 'uuid',
    'link_title' => 'Buttons',
    'options' => array(
      'identifier' => 'main-menu_buttons:uuid/node/34d13068-3849-4eaf-89d4-880954ac8bdb',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -39,
    'customized' => 0,
    'uuid' => '34d13068-3849-4eaf-89d4-880954ac8bdb',
    'vuuid' => '488c536d-aae4-4329-acc6-50fa8d15a848',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_collapsibles:uuid/node/5b9b97a8-bb26-4288-a08e-5c4891a1fe99.
  $menu_links['main-menu_collapsibles:uuid/node/5b9b97a8-bb26-4288-a08e-5c4891a1fe99'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/5b9b97a8-bb26-4288-a08e-5c4891a1fe99',
    'router_path' => 'uuid',
    'link_title' => 'Collapsibles',
    'options' => array(
      'identifier' => 'main-menu_collapsibles:uuid/node/5b9b97a8-bb26-4288-a08e-5c4891a1fe99',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -42,
    'customized' => 0,
    'uuid' => '5b9b97a8-bb26-4288-a08e-5c4891a1fe99',
    'vuuid' => 'c094b3cc-de6f-49cf-b165-980a5c5442e6',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_columns:uuid/node/c3b6b1cf-1ae1-48ad-ab3a-0287775fe14a.
  $menu_links['main-menu_columns:uuid/node/c3b6b1cf-1ae1-48ad-ab3a-0287775fe14a'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/c3b6b1cf-1ae1-48ad-ab3a-0287775fe14a',
    'router_path' => 'uuid',
    'link_title' => 'Columns',
    'options' => array(
      'identifier' => 'main-menu_columns:uuid/node/c3b6b1cf-1ae1-48ad-ab3a-0287775fe14a',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 0,
    'uuid' => 'c3b6b1cf-1ae1-48ad-ab3a-0287775fe14a',
    'vuuid' => '828b11d9-b212-44a3-860b-0e6c8ad84a21',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_divider:uuid/node/0825174b-b312-41f9-b2f6-64d4c22edefd.
  $menu_links['main-menu_divider:uuid/node/0825174b-b312-41f9-b2f6-64d4c22edefd'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/0825174b-b312-41f9-b2f6-64d4c22edefd',
    'router_path' => 'uuid',
    'link_title' => 'Divider',
    'options' => array(
      'identifier' => 'main-menu_divider:uuid/node/0825174b-b312-41f9-b2f6-64d4c22edefd',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 0,
    'uuid' => '0825174b-b312-41f9-b2f6-64d4c22edefd',
    'vuuid' => '8e22aec5-02f5-451e-855e-94f762bccd3a',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4.
  $menu_links['main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
    'router_path' => 'uuid',
    'link_title' => 'Elements',
    'options' => array(
      'identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 0,
    'uuid' => '35015f1d-7a59-43e8-a066-a55c3474e0d4',
    'vuuid' => '771b8907-9453-45f9-8e97-4cddd582237e',
  );
  // Exported menu link: main-menu_full-width-section:uuid/node/969e7879-63f5-464c-a8af-6fb131a1aeed.
  $menu_links['main-menu_full-width-section:uuid/node/969e7879-63f5-464c-a8af-6fb131a1aeed'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/969e7879-63f5-464c-a8af-6fb131a1aeed',
    'router_path' => 'uuid',
    'link_title' => 'Full Width Section',
    'options' => array(
      'identifier' => 'main-menu_full-width-section:uuid/node/969e7879-63f5-464c-a8af-6fb131a1aeed',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 0,
    'uuid' => '969e7879-63f5-464c-a8af-6fb131a1aeed',
    'vuuid' => 'b65ae927-196f-46bd-a2e2-e87734e685ef',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_jumbotron:uuid/node/bfb6a4e5-3557-489b-b949-872f6dd0ebb2.
  $menu_links['main-menu_jumbotron:uuid/node/bfb6a4e5-3557-489b-b949-872f6dd0ebb2'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/bfb6a4e5-3557-489b-b949-872f6dd0ebb2',
    'router_path' => 'uuid',
    'link_title' => 'Jumbotron',
    'options' => array(
      'identifier' => 'main-menu_jumbotron:uuid/node/bfb6a4e5-3557-489b-b949-872f6dd0ebb2',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 0,
    'uuid' => 'bfb6a4e5-3557-489b-b949-872f6dd0ebb2',
    'vuuid' => 'd5d06e0a-2046-42b8-91e5-fca36a995a06',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_lead:uuid/node/9e0ff233-9546-4277-bac0-a2e6cb36c8a7.
  $menu_links['main-menu_lead:uuid/node/9e0ff233-9546-4277-bac0-a2e6cb36c8a7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/9e0ff233-9546-4277-bac0-a2e6cb36c8a7',
    'router_path' => 'uuid',
    'link_title' => 'Lead',
    'options' => array(
      'identifier' => 'main-menu_lead:uuid/node/9e0ff233-9546-4277-bac0-a2e6cb36c8a7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 0,
    'uuid' => '9e0ff233-9546-4277-bac0-a2e6cb36c8a7',
    'vuuid' => 'a1d07b0c-b88e-4008-936b-86deda105867',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_panel:uuid/node/2e6d0201-12ac-40ce-8c42-15b8251b7902.
  $menu_links['main-menu_panel:uuid/node/2e6d0201-12ac-40ce-8c42-15b8251b7902'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/2e6d0201-12ac-40ce-8c42-15b8251b7902',
    'router_path' => 'uuid',
    'link_title' => 'Panel',
    'options' => array(
      'identifier' => 'main-menu_panel:uuid/node/2e6d0201-12ac-40ce-8c42-15b8251b7902',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 0,
    'uuid' => '2e6d0201-12ac-40ce-8c42-15b8251b7902',
    'vuuid' => '6ddbb804-810e-4cef-9a41-24489c097636',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_quote-slider:uuid/node/d495cf25-fc13-4103-a876-c71b190c421c.
  $menu_links['main-menu_quote-slider:uuid/node/d495cf25-fc13-4103-a876-c71b190c421c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/d495cf25-fc13-4103-a876-c71b190c421c',
    'router_path' => 'uuid',
    'link_title' => 'Quote Slider',
    'options' => array(
      'identifier' => 'main-menu_quote-slider:uuid/node/d495cf25-fc13-4103-a876-c71b190c421c',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -41,
    'customized' => 0,
    'uuid' => 'd495cf25-fc13-4103-a876-c71b190c421c',
    'vuuid' => 'c04b138e-f3c7-4bee-8ab8-bae8cba0ed9a',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_tabs:uuid/node/92e308f2-1208-4b67-ba2b-04dbe2d5833a.
  $menu_links['main-menu_tabs:uuid/node/92e308f2-1208-4b67-ba2b-04dbe2d5833a'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/92e308f2-1208-4b67-ba2b-04dbe2d5833a',
    'router_path' => 'uuid',
    'link_title' => 'Tabs',
    'options' => array(
      'identifier' => 'main-menu_tabs:uuid/node/92e308f2-1208-4b67-ba2b-04dbe2d5833a',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 0,
    'uuid' => '92e308f2-1208-4b67-ba2b-04dbe2d5833a',
    'vuuid' => 'a1f30347-1c81-494a-97f4-62f74530a3bb',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );
  // Exported menu link: main-menu_well:uuid/node/ca2e52f4-ec41-4a6b-ae81-6aeef8149192.
  $menu_links['main-menu_well:uuid/node/ca2e52f4-ec41-4a6b-ae81-6aeef8149192'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/ca2e52f4-ec41-4a6b-ae81-6aeef8149192',
    'router_path' => 'uuid',
    'link_title' => 'Well',
    'options' => array(
      'identifier' => 'main-menu_well:uuid/node/ca2e52f4-ec41-4a6b-ae81-6aeef8149192',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 0,
    'uuid' => 'ca2e52f4-ec41-4a6b-ae81-6aeef8149192',
    'vuuid' => '3f161f89-81e3-4b65-82ec-0ada1f402950',
    'parent_identifier' => 'main-menu_elements:uuid/node/35015f1d-7a59-43e8-a066-a55c3474e0d4',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Blockquote');
  t('Buttons');
  t('Collapsibles');
  t('Columns');
  t('Divider');
  t('Elements');
  t('Full Width Section');
  t('Jumbotron');
  t('Lead');
  t('Panel');
  t('Quote Slider');
  t('Tabs');
  t('Well');

  return $menu_links;
}
