<?php
/**
 * @file
 * glazed_content_homepage_examples.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function glazed_content_homepage_examples_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_home-app-landing-page:uuid/node/a32afdd3-e3b0-4afb-9b9d-fca4ee53e917.
  $menu_links['main-menu_home-app-landing-page:uuid/node/a32afdd3-e3b0-4afb-9b9d-fca4ee53e917'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/a32afdd3-e3b0-4afb-9b9d-fca4ee53e917',
    'router_path' => 'uuid',
    'link_title' => 'Home App Landing Page',
    'options' => array(
      'identifier' => 'main-menu_home-app-landing-page:uuid/node/a32afdd3-e3b0-4afb-9b9d-fca4ee53e917',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => 'a32afdd3-e3b0-4afb-9b9d-fca4ee53e917',
    'vuuid' => '557c30e4-1561-4f10-87df-d8824fe48fbb',
    'parent_identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
  );
  // Exported menu link: main-menu_home-basic:uuid/node/fc35b67a-3cea-4e3b-bfc7-17018413f4d6.
  $menu_links['main-menu_home-basic:uuid/node/fc35b67a-3cea-4e3b-bfc7-17018413f4d6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/fc35b67a-3cea-4e3b-bfc7-17018413f4d6',
    'router_path' => 'uuid',
    'link_title' => 'Home Basic',
    'options' => array(
      'identifier' => 'main-menu_home-basic:uuid/node/fc35b67a-3cea-4e3b-bfc7-17018413f4d6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => 'fc35b67a-3cea-4e3b-bfc7-17018413f4d6',
    'vuuid' => '30d83a88-a4c2-4e46-b001-2b07c3cbe9be',
    'parent_identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
  );
  // Exported menu link: main-menu_home-blog-classic:uuid/node/703a60cd-2aa6-42ae-8ef9-cb207933bf35.
  $menu_links['main-menu_home-blog-classic:uuid/node/703a60cd-2aa6-42ae-8ef9-cb207933bf35'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/703a60cd-2aa6-42ae-8ef9-cb207933bf35',
    'router_path' => 'uuid',
    'link_title' => 'Home Blog Classic',
    'options' => array(
      'identifier' => 'main-menu_home-blog-classic:uuid/node/703a60cd-2aa6-42ae-8ef9-cb207933bf35',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '703a60cd-2aa6-42ae-8ef9-cb207933bf35',
    'vuuid' => 'f31835f0-e94c-438d-a5d0-944bc80fce4a',
    'parent_identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
  );
  // Exported menu link: main-menu_home-business-clean:uuid/node/760f2195-e3fd-4957-ac92-410530f39825.
  $menu_links['main-menu_home-business-clean:uuid/node/760f2195-e3fd-4957-ac92-410530f39825'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/760f2195-e3fd-4957-ac92-410530f39825',
    'router_path' => 'uuid',
    'link_title' => 'Home Business Clean',
    'options' => array(
      'identifier' => 'main-menu_home-business-clean:uuid/node/760f2195-e3fd-4957-ac92-410530f39825',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '760f2195-e3fd-4957-ac92-410530f39825',
    'vuuid' => '76ff02f5-7f43-4684-b811-50fa0dd3af00',
    'parent_identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
  );
  // Exported menu link: main-menu_home-canvas:uuid/node/2bc7e0bd-0597-455f-9730-a058acce07dd.
  $menu_links['main-menu_home-canvas:uuid/node/2bc7e0bd-0597-455f-9730-a058acce07dd'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/2bc7e0bd-0597-455f-9730-a058acce07dd',
    'router_path' => 'uuid',
    'link_title' => 'Home Canvas',
    'options' => array(
      'identifier' => 'main-menu_home-canvas:uuid/node/2bc7e0bd-0597-455f-9730-a058acce07dd',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '2bc7e0bd-0597-455f-9730-a058acce07dd',
    'vuuid' => '070a11c8-cb42-4b9a-a4e8-b87d12e9d036',
    'parent_identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
  );
  // Exported menu link: main-menu_home-main-demo:uuid/node/071120f5-9f83-42fd-88cc-eca14093c5ca.
  $menu_links['main-menu_home-main-demo:uuid/node/071120f5-9f83-42fd-88cc-eca14093c5ca'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/071120f5-9f83-42fd-88cc-eca14093c5ca',
    'router_path' => 'uuid',
    'link_title' => 'Home Main Demo',
    'options' => array(
      'identifier' => 'main-menu_home-main-demo:uuid/node/071120f5-9f83-42fd-88cc-eca14093c5ca',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => '071120f5-9f83-42fd-88cc-eca14093c5ca',
    'vuuid' => '70f21339-ec75-49cd-a0bb-60384a5b9386',
    'parent_identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
  );
  // Exported menu link: main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a.
  $menu_links['main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
    'router_path' => 'uuid',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 0,
    'uuid' => '5ba48b9e-63be-4e62-a816-aa3a95159c8a',
    'vuuid' => '4150a82b-b216-4648-bb3f-54e605611cd6',
  );
  // Exported menu link: main-menu_homepage-portfolio-1:uuid/node/d880e681-e7a1-494f-b182-f7da873cff88.
  $menu_links['main-menu_homepage-portfolio-1:uuid/node/d880e681-e7a1-494f-b182-f7da873cff88'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uuid/node/d880e681-e7a1-494f-b182-f7da873cff88',
    'router_path' => 'uuid',
    'link_title' => 'Homepage Portfolio 1',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_homepage-portfolio-1:uuid/node/d880e681-e7a1-494f-b182-f7da873cff88',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'uuid' => 'd880e681-e7a1-494f-b182-f7da873cff88',
    'vuuid' => '47f2ab1e-e716-45d5-ba06-b48e09854d5b',
    'parent_identifier' => 'main-menu_home:uuid/node/5ba48b9e-63be-4e62-a816-aa3a95159c8a',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Home');
  t('Home App Landing Page');
  t('Home Basic');
  t('Home Blog Classic');
  t('Home Business Clean');
  t('Home Canvas');
  t('Home Main Demo');
  t('Homepage Portfolio 1');

  return $menu_links;
}
