<?php
/**
 * @file
 * glazed_portfolio_content.features.uuid_file_entity.inc
 */

/**
 * Implements hook_uuid_features_default_file_entities().
 */
function glazed_portfolio_content_uuid_features_default_file_entities() {
  $files = array();

  $files[] = array(
    'filename' => 'maniac-typeface-square.jpg',
    'uri' => 'public://portfolio/maniac-typeface-square.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 75224,
    'status' => 1,
    'type' => 'image',
    'uuid' => '06bb1b4e-0720-4514-9cfa-38c5e06f447b',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1222,
      'width' => 1222,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1222,
    'width' => 1222,
    'uuid_features_packaged_file_path' => 'assets/maniac-typeface-square.jpg',
    'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
  );
  $files[] = array(
    'filename' => 'album-cover-square.jpg',
    'uri' => 'public://portfolio/album-cover-square.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 126420,
    'status' => 1,
    'type' => 'image',
    'uuid' => '26979d72-a7fb-41b4-bc57-3d669645eb68',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1222,
      'width' => 1222,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1222,
    'width' => 1222,
    'uuid_features_packaged_file_path' => 'assets/album-cover-square.jpg',
    'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
  );
  $files[] = array(
    'filename' => 'unsplash-verns.jpg',
    'uri' => 'public://portfolio/unsplash-verns.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 631974,
    'status' => 1,
    'type' => 'image',
    'uuid' => '2de1f73e-528e-4cae-b3ff-11534f48e014',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1282,
      'width' => 1920,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1282,
    'width' => 1920,
    'uuid_features_packaged_file_path' => 'assets/unsplash-verns.jpg',
    'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
  );
  $files[] = array(
    'filename' => 'Paper-Hot-Cup-Mock-Up-vol-2.jpg',
    'uri' => 'public://portfolio/Paper-Hot-Cup-Mock-Up-vol-2.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 86717,
    'status' => 1,
    'type' => 'image',
    'uuid' => '35713b94-8514-41af-83c0-cc3a0c3dc17e',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 873,
      'width' => 1400,
    ),
    'alt' => '',
    'title' => '',
    'height' => 873,
    'width' => 1400,
    'uuid_features_packaged_file_path' => 'assets/Paper-Hot-Cup-Mock-Up-vol-2.jpg',
    'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
  );
  return $files;
}
