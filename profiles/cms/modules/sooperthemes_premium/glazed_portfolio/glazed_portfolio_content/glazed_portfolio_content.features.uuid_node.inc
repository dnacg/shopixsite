<?php
/**
 * @file
 * glazed_portfolio_content.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function glazed_portfolio_content_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => 'Skillet Album Cover',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'portfolio',
  'language' => 'und',
  'created' => 1440664175,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '46776433-49a0-4da7-bf2a-35f97342e47c',
  'field_cms_portfolio_client' => array(
    'und' => array(
      0 => array(
        'value' => 'Wikipedia',
        'format' => NULL,
      ),
    ),
  ),
  'field_cms_portfolio_links' => array(),
  'field_cms_portoflio_custom' => array(
    'und' => array(
      0 => array(
        'value' => 'Pityful a rethoric',
        'format' => NULL,
      ),
    ),
  ),
  'field_portfolio_images' => array(
    'und' => array(
      0 => array(
        'file_uuid' => '26979d72-a7fb-41b4-bc57-3d669645eb68',
        'image_field_caption' => array(
          'value' => '<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen.</p><p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">She packed her seven versalia, put her initial into the belt and made herself on the way.</p>',
          'format' => 'wysiwyg_simple',
        ),
      ),
    ),
  ),
  'field_glazed_content_design' => array(),
  'field_mdp_categories' => array(
    'und' => array(
      0 => array(
        'tid' => '2020ba32-3910-41ba-ab55-f688cb8984ee',
        'uuid' => '2020ba32-3910-41ba-ab55-f688cb8984ee',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-08-27 08:29:35 +0000',
  'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
);
  $nodes[] = array(
  'title' => 'Unsplash Verns Isolated',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'portfolio',
  'language' => 'und',
  'created' => 1440663703,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '66705fa9-4a74-4b5d-ba68-5d4b2b441c42',
  'field_cms_portfolio_client' => array(
    'und' => array(
      0 => array(
        'value' => 'Wikipedia',
        'format' => NULL,
      ),
    ),
  ),
  'field_cms_portfolio_links' => array(),
  'field_cms_portoflio_custom' => array(
    'und' => array(
      0 => array(
        'value' => 'Pityful a rethoric',
        'format' => NULL,
      ),
    ),
  ),
  'field_portfolio_images' => array(
    'und' => array(
      0 => array(
        'file_uuid' => '2de1f73e-528e-4cae-b3ff-11534f48e014',
        'image_field_caption' => array(
          'value' => '<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">And if she hasn’t been rewritten, then they are still using her.</p><p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p><p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>',
          'format' => 'wysiwyg_simple',
        ),
      ),
    ),
  ),
  'field_glazed_content_design' => array(),
  'field_mdp_categories' => array(
    'und' => array(
      0 => array(
        'tid' => '667a0e7a-4952-4fbd-ad0d-f76c165afdb2',
        'uuid' => '667a0e7a-4952-4fbd-ad0d-f76c165afdb2',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-08-27 08:21:43 +0000',
  'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
);
  $nodes[] = array(
  'title' => 'Maniac Typeface',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'portfolio',
  'language' => 'und',
  'created' => 1440664225,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '6e95a721-dc10-4df8-9169-66eee9cdf916',
  'field_cms_portfolio_client' => array(
    'und' => array(
      0 => array(
        'value' => 'Wikipedia',
        'format' => NULL,
      ),
    ),
  ),
  'field_cms_portfolio_links' => array(),
  'field_cms_portoflio_custom' => array(
    'und' => array(
      0 => array(
        'value' => 'Pityful a rethoric',
        'format' => NULL,
      ),
    ),
  ),
  'field_portfolio_images' => array(
    'und' => array(
      0 => array(
        'file_uuid' => '06bb1b4e-0720-4514-9cfa-38c5e06f447b',
        'image_field_caption' => array(
          'value' => '<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">Pityful a rethoric question ran over her cheek, then she continued her way.</p><p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">On her way she met a copy.</p><p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country.</p>',
          'format' => 'wysiwyg_simple',
        ),
      ),
    ),
  ),
  'field_glazed_content_design' => array(),
  'field_mdp_categories' => array(
    'und' => array(
      0 => array(
        'tid' => '27db446d-edcd-47fe-a13e-7ee94963fd31',
        'uuid' => '27db446d-edcd-47fe-a13e-7ee94963fd31',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-08-27 08:30:25 +0000',
  'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
);
  $nodes[] = array(
  'title' => 'Mint Coffee Club Branding',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'portfolio',
  'language' => 'und',
  'created' => 1440665700,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'afa694d4-055b-4cc3-a4c6-e5235621761d',
  'field_cms_portfolio_client' => array(
    'und' => array(
      0 => array(
        'value' => 'Wikipedia',
        'format' => NULL,
      ),
    ),
  ),
  'field_cms_portfolio_links' => array(),
  'field_cms_portoflio_custom' => array(
    'und' => array(
      0 => array(
        'value' => 'Pityful a rethoric',
        'format' => NULL,
      ),
    ),
  ),
  'field_portfolio_images' => array(
    'und' => array(
      0 => array(
        'file_uuid' => '35713b94-8514-41af-83c0-cc3a0c3dc17e',
        'image_field_caption' => array(
          'value' => '<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">She packed her seven versalia, put her initial into the belt and made herself on the way.</p><p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-decoration: none; text-align: left; background-color: rgb(255, 255, 255);">When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>',
          'format' => 'wysiwyg_simple',
        ),
      ),
    ),
  ),
  'field_glazed_content_design' => array(),
  'field_mdp_categories' => array(
    'und' => array(
      0 => array(
        'tid' => 'cf638595-348f-4486-a34d-ecf7cb91b9c5',
        'uuid' => 'cf638595-348f-4486-a34d-ecf7cb91b9c5',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-08-27 08:55:00 +0000',
  'user_uuid' => 'c11ae397-1077-48bb-9e56-b43f3ace04af',
);
  return $nodes;
}
