<?php
/**
 * @file
 * glazed_main_theme_settings.features.uuid_bean.inc
 */

/**
 * Implements hook_uuid_features_default_beans().
 */
function glazed_main_theme_settings_uuid_features_default_beans() {
  $beans = array();

  $beans[] = array(
    'label' => 'SooperThemes Copyright Drupal TM',
    'description' => NULL,
    'title' => '',
    'type' => 'content_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'sooperthemes-copyright-drupal-tm',
    'view_mode' => 'default',
    'created' => 1432657587,
    'log' => '',
    'default_revision' => 1,
    'revisions' => array(),
    'vuuid' => 'b8e00d66-264a-4d18-b4fe-fd45b03b6908',
    'uuid' => '29f27e3b-7c6e-4c8d-bdcb-f09b48f9e3da',
    'field_block_content' => array(
      'und' => array(
        0 => array(
          'value' => '<p>Copyright 2015+ SooperThemes <a href="http://www.sooperthemes.com">Drupal Themes</a>. Powered by Drupal.</p>
',
          'format' => 'wysiwyg_full',
        ),
      ),
    ),
    'field_page_attachments' => array(),
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  $beans[] = array(
    'label' => 'Social Links',
    'description' => NULL,
    'title' => '',
    'type' => 'drag_drop_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'social-links-0',
    'view_mode' => 'default',
    'created' => 1441360358,
    'log' => '',
    'default_revision' => 1,
    'revisions' => array(),
    'vuuid' => '26673164-0711-40b7-8e76-eee372eea948',
    'uuid' => '304375ec-8656-45c5-9e6c-d759c7c099ea',
    'field_block_content' => array(
      'und' => array(
        0 => array(
          'value' => '<div data-azcnt="true" data-azat-device="sm" data-azb="az_row" data-az-id="b259" class="az-element az-row row" style=""><div data-azcnt="true" data-azb="az_column" data-az-id="b260" class="az-element az-ctnr az-column  col-sm-12" style=""><ul data-azat-el_class="pull-right" data-azat-st_theme_bgcolor="inherit" data-azat-st_theme_color="brand" data-azat-st_size="lg" data-azat-st_type="inline" data-azat-st_social_links="RmFjZWJvb2slM0QlMjJodHRwJTNBJTJGJTJGc29vcGVydGhlbWVzLmNvbSUyRnByaWNpbmclMjIlMEFZb3VUdWJlJTNEJTIyaHR0cCUzQSUyRiUyRnNvb3BlcnRoZW1lcy5jb20lMkZwcmljaW5nJTIyJTBBQW5kcm9pZCUzRCUyMmh0dHAlM0ElMkYlMkZzb29wZXJ0aGVtZXMuY29tJTJGcHJpY2luZyUyMiUwQVR3aXR0ZXIlM0QlMjJodHRwJTNBJTJGJTJGc29vcGVydGhlbWVzLmNvbSUyRnByaWNpbmclMjIlMEFMaW5rZWRJbiUzRCUyMmh0dHAlM0ElMkYlMkZzb29wZXJ0aGVtZXMuY29tJTJGcHJpY2luZyUyMiUwQURydXBhbCUzRCUyMmh0dHAlM0ElMkYlMkZzb29wZXJ0aGVtZXMuY29tJTJGcHJpY2luZyUyMiUwQVBpbnRlcmVzdCUzRCUyMmh0dHAlM0ElMkYlMkZzb29wZXJ0aGVtZXMuY29tJTJGcHJpY2luZyUyMg==" data-azb="st_social" data-az-id="b261" class="az-element st-social stbe-social-links pull-right stbe-social-links--color-brand inline" style=""><li class="stbe-social-links__item"><a href="http://sooperthemes.com/pricing"><i class="fa fa-lg fa-facebook stbe-util-icon- stbe-util-fx-" style="color: brand;background-color: inherit;border-color: ;" data-toggle="tooltip" data-placement="top" title="facebook"></i></a></li><li class="stbe-social-links__item"><a href="http://sooperthemes.com/pricing"><i class="fa fa-lg fa-youtube stbe-util-icon- stbe-util-fx-" style="color: brand;background-color: inherit;border-color: ;" data-toggle="tooltip" data-placement="top" title="youtube"></i></a></li><li class="stbe-social-links__item"><a href="http://sooperthemes.com/pricing"><i class="fa fa-lg fa-android stbe-util-icon- stbe-util-fx-" style="color: brand;background-color: inherit;border-color: ;" data-toggle="tooltip" data-placement="top" title="android"></i></a></li><li class="stbe-social-links__item"><a href="http://sooperthemes.com/pricing"><i class="fa fa-lg fa-twitter stbe-util-icon- stbe-util-fx-" style="color: brand;background-color: inherit;border-color: ;" data-toggle="tooltip" data-placement="top" title="twitter"></i></a></li><li class="stbe-social-links__item"><a href="http://sooperthemes.com/pricing"><i class="fa fa-lg fa-linkedin stbe-util-icon- stbe-util-fx-" style="color: brand;background-color: inherit;border-color: ;" data-toggle="tooltip" data-placement="top" title="linkedin"></i></a></li><li class="stbe-social-links__item"><a href="http://sooperthemes.com/pricing"><i class="fa fa-lg fa-drupal stbe-util-icon- stbe-util-fx-" style="color: brand;background-color: inherit;border-color: ;" data-toggle="tooltip" data-placement="top" title="drupal"></i></a></li><li class="stbe-social-links__item"><a href="http://sooperthemes.com/pricing"><i class="fa fa-lg fa-pinterest stbe-util-icon- stbe-util-fx-" style="color: brand;background-color: inherit;border-color: ;" data-toggle="tooltip" data-placement="top" title="pinterest"></i></a></li></ul></div></div>',
          'format' => 'wysiwyg_simple',
        ),
      ),
    ),
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  $beans[] = array(
    'label' => 'Contact Block',
    'description' => NULL,
    'title' => '',
    'type' => 'drag_drop_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'contact-block',
    'view_mode' => 'default',
    'created' => 1450730862,
    'log' => '',
    'default_revision' => 1,
    'revisions' => array(),
    'vuuid' => 'b890a719-a44f-4a77-ac32-1da6056f69e3',
    'uuid' => 'af442a54-e385-44e3-b24a-45b32fa5fe41',
    'field_block_content' => array(
      'und' => array(
        0 => array(
          'value' => '<div data-azb="az_section" data-az-id="b2" id="b2" class="az-element az-section  " style=""><div data-azcnt="true" class="az-ctnr container"><div data-azcnt="true" data-azat-device="sm" data-azb="az_row" data-az-id="b3" class="az-element az-row row" style=""><div data-azcnt="true" data-azat-width="1/2" data-azb="az_column" data-az-id="b4" class="az-element az-ctnr az-column  col-sm-6" style=""><div data-azcnt="true" data-azb="az_text" data-az-id="b23" class="az-element az-text" style=""><h2>We\'d Love to Hear From You.</h2>

<p class="lead">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
</div></div><div data-azcnt="true" data-azat-width=" 1/2" data-azb="az_column" data-az-id="b22" class="az-element az-ctnr az-column  col-sm-6" style=""><div data-azat-height="200" data-azat-address="Nachtegaalstraat 35, Utrecht" data-azb="az_map" data-az-id="b5" class="az-element az-map "><iframe style="width: 100%; height: 200px;" src="https://maps.google.com/maps?q=Nachtegaalstraat+35, Utrecht&amp;iwloc=near&amp;output=embed" frameborder="0"></<iframe></iframe></div></div></div></div></div>',
          'format' => 'wysiwyg_simple',
        ),
      ),
    ),
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  $beans[] = array(
    'label' => 'Social links',
    'description' => NULL,
    'title' => '',
    'type' => 'content_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'glazed-social-links',
    'view_mode' => 'default',
    'created' => 1441096007,
    'log' => '',
    'default_revision' => 1,
    'revisions' => array(),
    'vuuid' => '9b089052-ca6a-4aa8-b202-fe4d95e09a2b',
    'uuid' => 'cd3a90ac-e5ea-4165-b40f-e76b8173adc1',
    'field_block_content' => array(
      'und' => array(
        0 => array(
          'value' => '<p>[social style="square" color="inherit" hover_color="auto" hover="disc" Drupal="http://sooperthemes.com/pricing" Flickr="http://sooperthemes.com/pricing" Youtube="http://sooperthemes.com/pricing" Facebook="http://sooperthemes.com/pricing" Android="http://sooperthemes.com/pricing" Github="http://sooperthemes.com/pricing" SoundCloud="http://sooperthemes.com/pricing"][/social]</p>
',
          'format' => 'wysiwyg_full',
        ),
      ),
    ),
    'field_page_attachments' => array(),
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  $beans[] = array(
    'label' => 'Glazed Facebook Widget',
    'description' => NULL,
    'title' => '',
    'type' => 'content_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'glazed-facebook-widget',
    'view_mode' => 'default',
    'created' => 1441192822,
    'log' => '',
    'default_revision' => 1,
    'revisions' => array(),
    'vuuid' => 'bd783051-d178-4fbc-a44f-9add951d195e',
    'uuid' => 'df987535-1c43-4274-895d-fdd7ed82f6e4',
    'field_block_content' => array(
      'und' => array(
        0 => array(
          'value' => '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=568856383201152&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>
<div class="fb-like-box" data-href="https://www.facebook.com/sooperthemes" data-colorscheme="light" data-width="270" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>',
          'format' => 'unfiltered_html',
        ),
      ),
    ),
    'field_page_attachments' => array(),
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  return $beans;
}
