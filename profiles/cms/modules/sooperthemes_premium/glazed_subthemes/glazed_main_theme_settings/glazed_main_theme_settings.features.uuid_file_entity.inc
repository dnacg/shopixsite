<?php
/**
 * @file
 * glazed_main_theme_settings.features.uuid_file_entity.inc
 */

/**
 * Implements hook_uuid_features_default_file_entities().
 */
function glazed_main_theme_settings_uuid_features_default_file_entities() {
  $files = array();

  $files[] = array(
    'filename' => 'futuristic-architecture-design-m_0.jpg',
    'uri' => 'public://page-attachments/futuristic-architecture-design-m_0.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 124966,
    'status' => 1,
    'type' => 'image',
    'uuid' => '489e90bc-5d7f-477b-b0e7-2dec1e35ba48',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1414,
      'width' => 1414,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1414,
    'width' => 1414,
    'uuid_features_packaged_file_path' => 'assets/futuristic-architecture-design-m_0.jpg',
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  $files[] = array(
    'filename' => 'futuristic-curvy-architecture-banner-white_0.jpg',
    'uri' => 'public://page-attachments/futuristic-curvy-architecture-banner-white_0.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 45566,
    'status' => 1,
    'type' => 'image',
    'uuid' => 'bd30cc86-d44c-4429-97c7-ee8ebb2d93b8',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 350,
      'width' => 2560,
    ),
    'alt' => '',
    'title' => '',
    'height' => 350,
    'width' => 2560,
    'uuid_features_packaged_file_path' => 'assets/futuristic-curvy-architecture-banner-white_0.jpg',
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  $files[] = array(
    'filename' => 'photodune-6205560-abstract-construction-m_0.jpg',
    'uri' => 'public://page-attachments/photodune-6205560-abstract-construction-m_0.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 160299,
    'status' => 1,
    'type' => 'image',
    'uuid' => 'e24442b3-628f-4789-bb6d-0b6e12364db0',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1414,
      'width' => 1414,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1414,
    'width' => 1414,
    'uuid_features_packaged_file_path' => 'assets/photodune-6205560-abstract-construction-m_0.jpg',
    'user_uuid' => '69378e06-2a5f-4fc1-a6d1-b5cff201498b',
  );
  return $files;
}
