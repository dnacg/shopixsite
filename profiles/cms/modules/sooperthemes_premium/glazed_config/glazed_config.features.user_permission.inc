<?php
/**
 * @file
 * glazed_config.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function glazed_config_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view any drag_drop_block bean'.
  $permissions['view any drag_drop_block bean'] = array(
    'name' => 'view any drag_drop_block bean',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
