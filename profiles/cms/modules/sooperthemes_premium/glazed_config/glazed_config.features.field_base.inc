<?php
/**
 * @file
 * glazed_config.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function glazed_config_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_glazed_content_design'.
  $field_bases['field_glazed_content_design'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_glazed_content_design',
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
    ),
    'locked' => 0,
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'path' => '',
    ),
    'translatable' => 0,
    'type' => 'field_collection',
  );

  // Exported field_base: 'field_glazed_content_region'.
  $field_bases['field_glazed_content_region'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_glazed_content_region',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'glazed-util-full-width-content' => 'Full width content',
        'glazed-util-content-center-4-col' => 'Squish content column to 1/3',
        'glazed-util-content-center-6-col' => 'Squish content column to 1/2',
        'glazed-util-content-center-8-col' => 'Squish content column to 2/3',
        'glazed-util-content-center-10-col' => 'Squish content column to 5/6',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_glazed_header_image'.
  $field_bases['field_glazed_header_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_glazed_header_image',
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_glazed_hide_regions'.
  $field_bases['field_glazed_hide_regions'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_glazed_hide_regions',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'navigation' => 'Navigation',
        'page_title' => 'Page Title',
        'slider' => 'Hero Region',
        'header' => 'Header',
        'content_top' => 'Content Top',
        'content' => 'Content',
        'content_bottom' => 'Content Bottom',
        'sidebar_first' => 'Primary',
        'sidebar_second' => 'Secondary',
        'footer' => 'Footer',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
