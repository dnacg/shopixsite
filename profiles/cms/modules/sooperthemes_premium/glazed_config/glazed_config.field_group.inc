<?php
/**
 * @file
 * glazed_config.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function glazed_config_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_glazed|node|blog|form';
  $field_group->group_name = 'group_glazed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Glazed Content Design',
    'weight' => '8',
    'children' => array(
      0 => 'field_glazed_content_design',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Glazed Content Design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-glazed field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_glazed|node|blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_glazed|node|drag_drop_page|form';
  $field_group->group_name = 'group_glazed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'drag_drop_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Glazed Content Design',
    'weight' => '9',
    'children' => array(
      0 => 'field_glazed_content_design',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Glazed Content Design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-glazed field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_glazed|node|drag_drop_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_glazed|node|event|form';
  $field_group->group_name = 'group_glazed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Glazed Content Design',
    'weight' => '10',
    'children' => array(
      0 => 'field_glazed_content_design',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Glazed Content Design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_glazed|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_glazed|node|news|form';
  $field_group->group_name = 'group_glazed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Glazed Content Design',
    'weight' => '6',
    'children' => array(
      0 => 'field_glazed_content_design',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Glazed Content Design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-glazed field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_glazed|node|news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_glazed|node|page|form';
  $field_group->group_name = 'group_glazed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Glazed Content Design',
    'weight' => '46',
    'children' => array(
      0 => 'field_glazed_content_design',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Glazed Content Design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-glazed field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_glazed|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_glazed|node|portfolio|form';
  $field_group->group_name = 'group_glazed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'portfolio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Glazed Content Design',
    'weight' => '9',
    'children' => array(
      0 => 'field_glazed_content_design',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Glazed Content Design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-glazed field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_glazed|node|portfolio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_glazed|node|webform|form';
  $field_group->group_name = 'group_glazed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'webform';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Glazed Content Design',
    'weight' => '45',
    'children' => array(
      0 => 'field_glazed_content_design',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Glazed Content Design',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-glazed field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_glazed|node|webform|form'] = $field_group;

  return $export;
}
