<?php
/**
 * @file
 * glazed_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function glazed_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function glazed_config_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function glazed_config_node_info() {
  $items = array(
    'drag_drop_page' => array(
      'name' => t('Drag and Drop Page'),
      'base' => 'node_content',
      'description' => t('Advanced page with visual page building tools. Powered by carbide Composer and SooperThemes Elements.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
