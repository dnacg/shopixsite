<?php
/**
 * @file
 * cms_blog_content.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function cms_blog_content_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => 'The Evolution of Ipsum Lorem',
  'log' => '',
  'status' => 1,
  'comment' => 2,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'blog',
  'language' => 'und',
  'created' => 1430984458,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '35d04adb-1b41-4970-8be0-19269b1d6618',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>When she <em>reached</em> the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>

<p>Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a <strong>copy</strong>.</p>

<p>The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country.</p>

<p>But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their projects again and again.</p>

<p>And if she hasn’t been rewritten, then they are still using her.</p>

<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>

<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
',
        'summary' => '',
        'format' => 'wysiwyg_full',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_blog_category' => array(
    'und' => array(
      0 => array(
        'tid' => '60e20db5-3340-40b2-87e0-ac2b560b1355',
        'uuid' => '60e20db5-3340-40b2-87e0-ac2b560b1355',
      ),
    ),
  ),
  'field_blog_image' => array(
    'und' => array(
      0 => array(
        'file_uuid' => '4d781159-cdb5-48a9-9587-baa0e6a903c7',
        'image_field_caption' => array(
          'value' => '',
          'format' => 'wysiwyg_full',
        ),
      ),
    ),
  ),
  'field_blog_tags' => array(
    'und' => array(
      0 => array(
        'tid' => '3456efe3-c665-49de-abc8-0fd9f5075ee5',
        'uuid' => '3456efe3-c665-49de-abc8-0fd9f5075ee5',
      ),
      1 => array(
        'tid' => '9ee3da11-283d-45d3-85ed-a7e96547ad49',
        'uuid' => '9ee3da11-283d-45d3-85ed-a7e96547ad49',
      ),
      2 => array(
        'tid' => 'f870ff3e-acef-4e3e-8958-1e10ef0565ed',
        'uuid' => 'f870ff3e-acef-4e3e-8958-1e10ef0565ed',
      ),
      3 => array(
        'tid' => 'e32de019-e4d5-46b6-a10d-786c6cc70756',
        'uuid' => 'e32de019-e4d5-46b6-a10d-786c6cc70756',
      ),
    ),
  ),
  'field_page_attachments' => array(),
  'field_glazed_content_design' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-05-07 07:40:58 +0000',
  'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
);
  $nodes[] = array(
  'title' => '7 ways to increase your lorem ipsum',
  'log' => '',
  'status' => 1,
  'comment' => 2,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'blog',
  'language' => 'und',
  'created' => 1409556578,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'f386b493-4347-4570-bc22-382db8c2df88',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Diam distineo ex ille imputo tamen usitas. Eum gemino iusto melior quibus. Aliquip defui exputo feugiat iaceo nostrud singularis tego ulciscor. Camur et ex praemitto ratis ulciscor. Abico dignissim eum iriure laoreet persto praesent rusticus secundum velit. Ibidem occuro quis velit. Brevitas caecus ex melior vulpes. Brevitas dignissim probo quadrum sed ulciscor venio vicis ymo. Abbas nibh obruo probo qui. Ad bene dolor et gemino ideo iusto quidne saluto uxor. Defui exputo hendrerit paratus.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eleifend, mi sed porta dictum, tellus odio ultricies ligula, a accumsan diam tortor ut nibh. Proin non sapien aliquet, posuere est non, porttitor velit. Cras adipiscing dolor vitae vestibulum consequat. Sed vehicula bibendum lorem ut pellentesque. Pellentesque vitae tellus nec ante ultricies placerat et vel velit. In sit amet dui varius, gravida lacus nec, lobortis libero. Vestibulum porta, nulla quis tempus convallis, turpis diam volutpat enim, vitae pellentesque ligula tellus nec turpis. Pellentesque gravida sem metus, a commodo urna convallis condimentum. Duis dictum gravida enim, a sollicitudin ante fermentum a. Nam gravida arcu vitae sapien suscipit semper. Integer convallis nisl eu orci posuere rhoncus. Ut pharetra dui vel luctus lacinia.</p>

<p>Cras ultrices neque ligula, vitae euismod nibh euismod et. Praesent non porta nibh, ut suscipit odio. Mauris viverra id nunc eget facilisis. Proin fringilla rhoncus neque, vitae eleifend libero elementum id. Suspendisse potenti. Nam sed nulla eu ligula porttitor tincidunt. Cras in luctus libero.</p>

<p>Integer orci neque, consequat sed lacus et, porttitor dapibus purus. Vestibulum ac nisl et nulla facilisis dapibus vel semper nunc. Nullam eleifend orci vel libero interdum, vitae vestibulum eros dictum. Cras enim augue, blandit ut justo non, porta aliquam felis. Nullam sagittis lacinia nibh, ut iaculis nisl iaculis nec. Vivamus ut sapien sed dolor dignissim commodo vitae eu purus. Pellentesque luctus dolor augue, id pulvinar augue eleifend id.</p>

<p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut a nisi condimentum, facilisis dui et, porta leo. Proin semper cursus eros, nec vulputate lacus consectetur non. Suspendisse mattis quam sit amet sem pellentesque ultricies. Morbi ut libero suscipit, mollis nisl nec, dapibus purus. Integer eu interdum arcu, sit amet consectetur arcu. Duis sed nisi non lacus adipiscing mattis. Nam aliquam egestas quam at interdum.</p>

<p>Nulla ante arcu, porta a viverra sed, rhoncus sed justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut vulputate tristique tincidunt. Fusce eu tellus diam. Vivamus ultricies cursus mattis. Nunc tempus, est et viverra ullamcorper, urna elit suscipit turpis, nec viverra tellus lorem a nibh. Ut id egestas felis, et ullamcorper tellus. Ut quis dapibus urna. Aliquam elit justo, gravida et adipiscing ut, eleifend id lacus. Nunc et ullamcorper massa. Donec in sapien nec justo bibendum venenatis.</p>
',
        'summary' => '',
        'format' => 'wysiwyg_full',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_blog_category' => array(
    'und' => array(
      0 => array(
        'tid' => '3456efe3-c665-49de-abc8-0fd9f5075ee5',
        'uuid' => '3456efe3-c665-49de-abc8-0fd9f5075ee5',
      ),
    ),
  ),
  'field_blog_image' => array(
    'und' => array(
      0 => array(
        'file_uuid' => '592618fb-18f5-4816-b915-3c58f971b248',
        'image_field_caption' => array(
          'value' => '',
          'format' => 'wysiwyg_simple',
        ),
      ),
    ),
  ),
  'field_blog_tags' => array(
    'und' => array(
      0 => array(
        'tid' => 'e32de019-e4d5-46b6-a10d-786c6cc70756',
        'uuid' => 'e32de019-e4d5-46b6-a10d-786c6cc70756',
      ),
      1 => array(
        'tid' => 'c8507d08-9e00-4f1a-938a-5b97d82f8813',
        'uuid' => 'c8507d08-9e00-4f1a-938a-5b97d82f8813',
      ),
      2 => array(
        'tid' => '9ee3da11-283d-45d3-85ed-a7e96547ad49',
        'uuid' => '9ee3da11-283d-45d3-85ed-a7e96547ad49',
      ),
      3 => array(
        'tid' => '3f81c575-06ff-43cb-8339-d4790fa56ede',
        'uuid' => '3f81c575-06ff-43cb-8339-d4790fa56ede',
      ),
    ),
  ),
  'field_page_attachments' => array(),
  'field_glazed_content_design' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'comment_count' => 0,
  'pathauto_perform_alias' => FALSE,
  'date' => '2014-09-01 07:29:38 +0000',
  'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
);
  return $nodes;
}
