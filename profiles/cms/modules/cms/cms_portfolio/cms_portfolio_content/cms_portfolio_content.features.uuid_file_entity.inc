<?php
/**
 * @file
 * cms_portfolio_content.features.uuid_file_entity.inc
 */

/**
 * Implements hook_uuid_features_default_file_entities().
 */
function cms_portfolio_content_uuid_features_default_file_entities() {
  $files = array();

  $files[] = array(
    'filename' => 'unsplash-mountain-bridge.jpg',
    'uri' => 'public://portfolio/unsplash-mountain-bridge.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 261430,
    'status' => 1,
    'type' => 'image',
    'uuid' => '7d53fe6d-4ef8-4114-b016-c125538c5de2',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1920,
      'width' => 1439,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1920,
    'width' => 1439,
    'uuid_features_packaged_file_path' => 'assets/unsplash-mountain-bridge.jpg',
    'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
  );
  $files[] = array(
    'filename' => 'unsplash-desert-girl.jpg',
    'uri' => 'public://portfolio/unsplash-desert-girl.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 255210,
    'status' => 1,
    'type' => 'image',
    'uuid' => 'e032678c-5076-4490-aa91-b26614f93a83',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1280,
      'width' => 1920,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1280,
    'width' => 1920,
    'uuid_features_packaged_file_path' => 'assets/unsplash-desert-girl.jpg',
    'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
  );
  $files[] = array(
    'filename' => 'unsplash-incredible-view.jpg',
    'uri' => 'public://portfolio/unsplash-incredible-view.jpg',
    'filemime' => 'image/jpeg',
    'filesize' => 406124,
    'status' => 1,
    'type' => 'image',
    'uuid' => 'f02cd32a-6bbf-4f12-b0dc-6ffed3959130',
    'field_file_image_alt_text' => array(),
    'field_file_image_title_text' => array(),
    'rdf_mapping' => array(),
    'metadata' => array(
      'height' => 1280,
      'width' => 1920,
    ),
    'alt' => '',
    'title' => '',
    'height' => 1280,
    'width' => 1920,
    'uuid_features_packaged_file_path' => 'assets/unsplash-incredible-view.jpg',
    'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
  );
  return $files;
}
