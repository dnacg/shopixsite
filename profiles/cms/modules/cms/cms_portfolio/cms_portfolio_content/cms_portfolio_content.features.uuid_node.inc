<?php
/**
 * @file
 * cms_portfolio_content.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function cms_portfolio_content_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => 'Unsplash Incredible Nature',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'portfolio',
  'language' => 'und',
  'created' => 1440752996,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '810ccb6a-f32d-452e-b8e3-fa8cf2396e9a',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>

<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen.</p>
',
        'summary' => '',
        'format' => 'wysiwyg_full',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_cms_portfolio_client' => array(
    'und' => array(
      0 => array(
        'value' => 'Wikipedia',
        'format' => NULL,
      ),
    ),
  ),
  'field_cms_portfolio_links' => array(),
  'field_cms_portoflio_custom' => array(
    'und' => array(
      0 => array(
        'value' => 'Pityful a rethoric',
        'format' => NULL,
      ),
    ),
  ),
  'field_page_attachments' => array(),
  'field_portfolio_images' => array(
    'und' => array(
      0 => array(
        'file_uuid' => 'f02cd32a-6bbf-4f12-b0dc-6ffed3959130',
        'image_field_caption' => array(
          'value' => '<p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>

<p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>

<p>Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy.</p>
',
          'format' => 'wysiwyg_full',
        ),
      ),
      1 => array(
        'file_uuid' => 'e032678c-5076-4490-aa91-b26614f93a83',
        'image_field_caption' => array(
          'value' => '<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>

<p>It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
',
          'format' => 'wysiwyg_full',
        ),
      ),
      2 => array(
        'file_uuid' => '7d53fe6d-4ef8-4114-b016-c125538c5de2',
        'image_field_caption' => array(
          'value' => '<p>The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their projects again and again.</p>

<p>And if she hasn’t been rewritten, then they are still using her. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
',
          'format' => 'wysiwyg_full',
        ),
      ),
    ),
  ),
  'field_glazed_content_design' => array(),
  'field_mdp_categories' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-08-28 09:09:56 +0000',
  'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
);
  $nodes[] = array(
  'title' => 'Unsplash Misty Bridge',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'portfolio',
  'language' => 'und',
  'created' => 1440750666,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '91ea6105-d5a4-4202-a30c-e4afd630d3f9',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>

<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
',
        'summary' => '',
        'format' => 'wysiwyg_full',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_cms_portfolio_client' => array(
    'und' => array(
      0 => array(
        'value' => 'Wikipedia',
        'format' => NULL,
      ),
    ),
  ),
  'field_cms_portfolio_links' => array(),
  'field_cms_portoflio_custom' => array(
    'und' => array(
      0 => array(
        'value' => 'Pityful a rethoric question',
        'format' => NULL,
      ),
    ),
  ),
  'field_page_attachments' => array(),
  'field_portfolio_images' => array(
    'und' => array(
      0 => array(
        'file_uuid' => '7d53fe6d-4ef8-4114-b016-c125538c5de2',
        'image_field_caption' => array(
          'value' => '<p>The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country.</p>

<p>But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their projects again and again.</p>
',
          'format' => 'wysiwyg_full',
        ),
      ),
    ),
  ),
  'field_glazed_content_design' => array(),
  'field_mdp_categories' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-08-28 08:31:06 +0000',
  'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
);
  $nodes[] = array(
  'title' => 'Unsplash Lost in Desert',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'type' => 'portfolio',
  'language' => 'und',
  'created' => 1440749347,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'f92ce0fd-a55f-4a71-9203-624270628344',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>

<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
',
        'summary' => '',
        'format' => 'wysiwyg_full',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_cms_portfolio_client' => array(
    'und' => array(
      0 => array(
        'value' => 'Wikipedia',
        'format' => NULL,
      ),
    ),
  ),
  'field_cms_portfolio_links' => array(),
  'field_cms_portoflio_custom' => array(
    'und' => array(
      0 => array(
        'value' => 'Pityful a rethoric',
        'format' => NULL,
      ),
    ),
  ),
  'field_page_attachments' => array(),
  'field_portfolio_images' => array(
    'und' => array(
      0 => array(
        'file_uuid' => 'e032678c-5076-4490-aa91-b26614f93a83',
        'image_field_caption' => array(
          'value' => '<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>

<p>It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
',
          'format' => 'wysiwyg_full',
        ),
      ),
    ),
  ),
  'field_glazed_content_design' => array(),
  'field_mdp_categories' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'comment_count' => 0,
  'date' => '2015-08-28 08:09:07 +0000',
  'user_uuid' => 'c155b92b-6053-4816-8abf-401a4fcd0619',
);
  return $nodes;
}
